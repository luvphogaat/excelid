<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('card_sizes') ) {
            Schema::create('card_sizes', function (Blueprint $table) {
                $table->increments('id');
                $table->string('card_size');
                $table->decimal('price', $precision = 8, $scale = 2);
                $table->string('category_id');
                $table->string('product_id');
                $table->softDeletes('deleted_at', 0);
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_sizes');
    }
}
