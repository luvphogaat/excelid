<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrintingTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('printing_type') ) {
            Schema::create('printing_type', function (Blueprint $table) {
                $table->increments('id');
                $table->string('printing_name');
                $table->decimal('price', $precision = 8, $scale = 2);
                $table->bigInteger('product_id');
                $table->bigInteger('qty_id');
                $table->softDeletes('deleted_at', 0);
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('printing_type');
    }
}
