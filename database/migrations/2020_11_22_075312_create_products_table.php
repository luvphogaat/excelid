<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $casts = [
        'quantity' => 'array'
    ];


    public function up()
    {
        if ( !Schema::hasTable('products') ) {
            Schema::create('products', function (Blueprint $table) {
                $table->increments('id');
                $table->foreignId('category_id');
                $table->text('product_desc')->nullable();
                $table->text('product_name');
                $table->text('product_keyword')->nullable();
                $table->string('status')->default('ACTIVE');
                $table->text('slug');
                $table->longText('addonstables');
                $table->softDeletes('deleted_at', 0);
                $table->timestamps();
            });
        }
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
