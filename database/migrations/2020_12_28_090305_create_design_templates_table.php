<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('design_templates') ) {
            Schema::create('design_templates', function (Blueprint $table) {
                $table->increments('id');
                $table->string('category_id');
                $table->longText('data');
                $table->string('fill_template1');
                $table->string('fill_template2')->nullable();
                $table->string('fontName');
                $table->string('fontUrl');
                $table->softDeletes('deleted_at', 0);
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('design_templates');
    }
}
