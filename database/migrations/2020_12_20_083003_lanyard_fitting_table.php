<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LanyardFittingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if ( !Schema::hasTable('lanyard_fitting') ) {
            Schema::create('lanyard_fitting', function (Blueprint $table) {
                $table->increments('id');
                $table->string('lanyard_id');
                $table->string('lanyard_fitting_size');
                $table->decimal('price', $precision = 8, $scale = 2);
                $table->string('category');
                $table->softDeletes('deleted_at', 0);
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
