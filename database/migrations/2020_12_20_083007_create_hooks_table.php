<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('hooks') ) {
            Schema::create('hooks', function (Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('lanyard_id');
                $table->bigInteger('qty_id');
                $table->string('hooks_name');
                $table->string('hook_image');
                $table->decimal('price', $precision = 8, $scale = 2);
                $table->softDeletes('deleted_at', 0);
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hooks');
    }
}
