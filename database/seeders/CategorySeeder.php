<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('category')->insert([[
            'category_title' => 'Plastic Cards & Accessories',
            'category_href' => 'plastic_cards_and_accessories',
            'total_products' => 8,
        ], [
            'category_title' => 'Customized Lanyard',
            'category_href' => 'customized_lanyard',
            'total_products' => 6,
        ],[
            'category_title' => 'Badges',
            'category_href' => 'badges',
            'total_products' => 4,
        ],[
            'category_title' => 'Mugs',
            'category_href' => 'mugs',
            'total_products' => 5,
        ],[
            'category_title' => 'T-Shirt',
            'category_href' => 't_shirt',
            'total_products' => 2,
        ],[
            'category_title' => 'Caps',
            'category_href' => 'caps',
            'total_products' => 1,
        ],[
            'category_title' => 'Mouse Pad',
            'category_href' => 'mouse_pad',
            'total_products' => 1,
        ],[
            'category_title' => 'Pen',
            'category_href' => 'pen',
            'total_products' => 1,
        ],[
            'category_title' => 'Sipper Bottles',
            'category_href' => 'sipper_bottles',
            'total_products' => 1,
        ]]
    );
    }
}
