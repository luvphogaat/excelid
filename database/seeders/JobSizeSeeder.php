<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('job_size')->insert([
             //  Student Id Cards Single
            [
                'id' => NULL,
                'product_id' => 1,
                'size_name' => '86x54 MM',
            ],
            //  Student Id Cards Double
            [
                'id' => NULL,
                'product_id' => 2,
                'size_name' => '86x54 MM',
            ],
            // Employee Single
            [
                'id' => NULL,
                'product_id' => 3,
                'size_name' => '86x54 MM',
            ],
            // Employee Double
            [
                'id' => NULL,
                'product_id' => 4,
                'size_name' => '86x54 MM',
            ],
            //  visiting Single
            [
                'id' => NULL,
                'product_id' => 5,
                'size_name' => '86x54 MM',
            ],
            
            // Memebershio ID Single
            [
                'id' => NULL,
                'product_id' => 6,
                'size_name' => '86x54 MM',
            ],
            // Memebershio ID Double
            [
                'id' => NULL,
                'product_id' => 7,
                'size_name' => '86x54 MM',
            ],
            //  waranty Card Single
            [
                'id' => NULL,
                'product_id' => 8,
                'size_name' => '86x54 MM',
            ],
            // Loyalty Card Single
            [
                'id' => NULL,
                'product_id' => 9,
                'size_name' => '86x54 MM',
            ],
            // Previlege Card Single
            [
                'id' => NULL,
                'product_id' => 10,
                'size_name' => '86x54 MM',
            ],
            // Flat Lanyard
            [
                'id' => NULL,
                'product_id' => 11,
                'size_name' => '12 MM',
            ],
            // Tube Lanyard
            [
                'id' => NULL,
                'product_id' => 12,
                'size_name' => '12 MM',
            ],
            // whitemug
            [
                'id' => NULL,
                'product_id' => 21,
                'size_name' => '86x54 MM',
            ],
            
            // inner color Mug
            [
                'id' => NULL,
                'product_id' => 22,
                'size_name' => '86x54 MM',
            ],
            
            //  Sattain Lanyard 12 MM
            [
                'id' => NULL,
                'product_id' => 13,
                'size_name' => '12 MM',
            ],
            
            //  Sattain Lanyard 20 MM
            [
                'id' => NULL,
                'product_id' => 13,
                'size_name' => '20 MM',
            ],
            
        ]);
    }
}
