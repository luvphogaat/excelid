<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrinttypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('printing_type')->insert([
            [
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.00,
                'product_id' => '1',
                'qty_id' => '1',
            ],[
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.00,
                'product_id' => '1',
                'qty_id' => '2',
            ],[
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.00,
                'product_id' => '1',
                'qty_id' => '140',
            ],[
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.00,
                'product_id' => '1',
                'qty_id' => '141',
            ],[
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.00,
                'product_id' => '1',
                'qty_id' => '142',
            ],[
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.00,
                'product_id' => '1',
                'qty_id' => '143',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '32',
                'qty_id' => '149',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '32',
                'qty_id' => '150',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '32',
                'qty_id' => '151',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '32',
                'qty_id' => '152',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '32',
                'qty_id' => '153',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '32',
                'qty_id' => '154',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '32',
                'qty_id' => '155',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 400.0,
                'product_id' => '32',
                'qty_id' => '149',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 800.0,
                'product_id' => '32',
                'qty_id' => '150',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 1200.0,
                'product_id' => '32',
                'qty_id' => '151',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 1600.0,
                'product_id' => '32',
                'qty_id' => '152',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 2500.0,
                'product_id' => '32',
                'qty_id' => '153',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 5000.0,
                'product_id' => '32',
                'qty_id' => '154',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 8000.0,
                'product_id' => '32',
                'qty_id' => '155',
            ], [
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '33',
                'qty_id' => '156',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '33',
                'qty_id' => '157',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '33',
                'qty_id' => '158',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '33',
                'qty_id' => '159',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '33',
                'qty_id' => '160',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '33',
                'qty_id' => '161',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '33',
                'qty_id' => '162',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 400.0,
                'product_id' => '33',
                'qty_id' => '156',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 800.0,
                'product_id' => '33',
                'qty_id' => '157',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 1200.0,
                'product_id' => '33',
                'qty_id' => '158',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 1600.0,
                'product_id' => '33',
                'qty_id' => '159',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 2500.0,
                'product_id' => '33',
                'qty_id' => '160',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 5000.0,
                'product_id' => '33',
                'qty_id' => '161',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 8000.0,
                'product_id' => '33',
                'qty_id' => '162',
            ], [
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '163',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '164',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '165',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '166',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '167',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '168',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '169',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 400.0,
                'product_id' => '34',
                'qty_id' => '163',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 800.0,
                'product_id' => '34',
                'qty_id' => '164',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 1200.0,
                'product_id' => '34',
                'qty_id' => '165',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 1600.0,
                'product_id' => '34',
                'qty_id' => '166',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 2500.0,
                'product_id' => '34',
                'qty_id' => '167',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 5000.0,
                'product_id' => '34',
                'qty_id' => '168',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 8000.0,
                'product_id' => '34',
                'qty_id' => '169',
            ], [
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '38',
                'qty_id' => '163',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '164',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '165',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '166',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '167',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '168',
            ],[
                'id' => NULL,
                'printing_type' => 'PLAIN',
                'price' => 0.0,
                'product_id' => '34',
                'qty_id' => '169',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 400.0,
                'product_id' => '34',
                'qty_id' => '163',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 800.0,
                'product_id' => '34',
                'qty_id' => '164',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 1200.0,
                'product_id' => '34',
                'qty_id' => '165',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 1600.0,
                'product_id' => '34',
                'qty_id' => '166',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 2500.0,
                'product_id' => '34',
                'qty_id' => '167',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 5000.0,
                'product_id' => '34',
                'qty_id' => '168',
            ],[
                'id' => NULL,
                'printing_type' => 'PVC',
                'price' => 8000.0,
                'product_id' => '34',
                'qty_id' => '169',
            ],
            // Student Id Double Sided
            [
                'id' => NULL,
                'printing_type' => 'DOUBLE SIDED',
                'price' => 0.0,
                'product_id' => '2',
                'qty_id' => '3',
            ],
            [
                'id' => NULL,
                'printing_type' => 'DOUBLE SIDED',
                'price' => 0.0,
                'product_id' => '2',
                'qty_id' => '144',
            ],[
                'id' => NULL,
                'printing_type' => 'DOUBLE SIDED',
                'price' => 0.0,
                'product_id' => '2',
                'qty_id' => '145',
            ],[
                'id' => NULL,
                'printing_type' => 'DOUBLE SIDED',
                'price' => 0.0,
                'product_id' => '2',
                'qty_id' => '146',
            ],[
                'id' => NULL,
                'printing_type' => 'DOUBLE SIDED',
                'price' => 0.0,
                'product_id' => '2',
                'qty_id' => '147',
            ],[
                'id' => NULL,
                'printing_type' => 'DOUBLE SIDED',
                'price' => 0.0,
                'product_id' => '2',
                'qty_id' => '148',
            ],
            //EMPLOYEE ID CARD (SINGLE SIDED)
            [
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.0,
                'product_id' => '3',
                'qty_id' => '4',
            ],
            // EMPLOYEE ID CARD (DOUBLE SIDED)
            [
                'id' => NULL,
                'printing_type' => 'DOUBLE SIDED',
                'price' => 0.0,
                'product_id' => '4',
                'qty_id' => '5',
            ],
            // VISITING CARD (SINGLE SIDED)
            [
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.0,
                'product_id' => '5',
                'qty_id' => '6',
            ],[
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.0,
                'product_id' => '5',
                'qty_id' => '7',
            ],[
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.0,
                'product_id' => '5',
                'qty_id' => '8',
            ],[
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.0,
                'product_id' => '5',
                'qty_id' => '9',
            ],
            // MEMBERSHIP ID CARD (SINGLE SIDED)
            [
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.0,
                'product_id' => '6',
                'qty_id' => '14',
            ],
            // MEMBERSHIP ID CARD (DOUBLE SIDED)
            [
                'id' => NULL,
                'printing_type' => 'DOUBLE SIDED',
                'price' => 0.0,
                'product_id' => '7',
                'qty_id' => '15',
            ],
            // WARRANTY CARD (SINGLE SIDED)
            [
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.0,
                'product_id' => '8',
                'qty_id' => '16',
            ],
            // LOYALITY CARD (SINGLE SIDED)
            [
                'id' => NULL,
                'printing_type' => 'SINGLE SIDED',
                'price' => 0.0,
                'product_id' => '9',
                'qty_id' => '17',
            ],
            // // FLAT LANYARD
            // [
            //     'id' => NULL,
            //     'printing_type' => 'SINGLE SIDED',
            //     'price' => 0.0,
            //     'product_id' => '9',
            //     'qty_id' => '17',
            // ],
        ]);
    }
}
