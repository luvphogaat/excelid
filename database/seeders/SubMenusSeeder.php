<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SubMenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sub_menu_items')->insert([[
                'id' => null,
                'title' => 'STUDENT ID CARD (SINGLE SIDED)',
                'link' => Str::slug('STUDENT ID CARD (SINGLE SIDED)'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],
            [
                'id' => null,
                'title' => 'STUDENT ID CARD (DOUBLE SIDED)',
                'link' => Str::slug('STUDENT ID CARD (DOUBLE SIDED)'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],
            [
                'id' => null,
                'title' => 'EMPLOYEE ID CARD (SINGLE SIDED)',
                'link' => Str::slug('EMPLOYEE ID CARD (SINGLE SIDED)'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],
            [
                'id' => null,
                'title' => 'EMPLOYEE ID CARD (DOUBLE SIDED)',
                'link' => Str::slug('EMPLOYEE ID CARD (DOUBLE SIDED)'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],
            [
                'id' => null,
                'title' => 'VISITING CARD',
                'link' => Str::slug('VISITING CARD'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],
            [
                'id' => null,
                'title' => 'MEMBERSHIP ID CARD (SINGLE SIDED)',
                'link' => Str::slug('MEMBERSHIP ID CARD (SINGLE SIDED)'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],
            [
                'id' => null,
                'title' => 'MEMBERSHIP ID CARD (DOUBLE SIDED)',
                'link' => Str::slug('MEMBERSHIP ID CARD (DOUBLE SIDED)'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],
            [
                'id' => null,
                'title' => 'WARRANTY CARD',
                'link' => Str::slug('WARRANTY CARD'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],
            [
                'id' => null,
                'title' => 'LOYALITY CARD',
                'link' => Str::slug('LOYALITY CARD'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],
            [
                'id' => null,
                'title' => 'PRIVILEGE CARD',
                'link' => Str::slug('PRIVILEGE CARD'),
                'status' => 'ENABLED',
                'parent_id' => 1,
            ],

// Lanyard
            [
                'id' => null,
                'title' => 'FLAT LANYARD',
                'link' => Str::slug('FLAT LANYARD'),
                'status' => 'ENABLED',
                'parent_id' => 2,
            ],
            [
                'id' => null,
                'title' => 'TUBE LANYARD',
                'link' => Str::slug('TUBE LANYARD'),
                'status' => 'ENABLED',
                'parent_id' => 2,
            ],
            [
                'id' => null,
                'title' => 'SATTAIN LANYARD',
                'link' => Str::slug('SATTAIN LANYARD'),
                'status' => 'ENABLED',
                'parent_id' => 2,
            ],
            [
                'id' => null,
                'title' => 'MULTI COLOR LANYARD',
                'link' => Str::slug('MULTI COLOR LANYARD'),
                'status' => 'ENABLED',
                'parent_id' => 2,
            ],
            [
                'id' => null,
                'title' => '25 MM DIGITAL',
                'link' => Str::slug('MM DIGITAL'),
                'status' => 'ENABLED',
                'parent_id' => 2,
            ],
            [
                'id' => null,
                'title' => 'SASH',
                'link' => Str::slug('SASH'),
                'status' => 'ENABLED',
                'parent_id' => 2,
            ],
// Badges
            [
                'id' => null,
                'title' => 'SCHOOL BADGE',
                'link' => Str::slug('SCHOOL BADGE'),
                'status' => 'ENABLED',
                'parent_id' => 3,
            ],
            [
                'id' => null,
                'title' => 'PROMOTIONAL BADGE',
                'link' => Str::slug('PROMOTIONAL BADGE'),
                'status' => 'ENABLED',
                'parent_id' => 3,
            ],
            [
                'id' => null,
                'title' => 'SPORTS BADGE',
                'link' => Str::slug('SPORTS BADGE'),
                'status' => 'ENABLED',
                'parent_id' => 3,
            ],
            [
                'id' => null,
                'title' => 'ELECTION BADGE',
                'link' => Str::slug('ELECTION BADGE'),
                'status' => 'ENABLED',
                'parent_id' => 3,
            ],

// Mugs
            [
                'id' => null,
                'title' => 'WHITE MUG',
                'link' => Str::slug('WHITE MUG'),
                'status' => 'ENABLED',
                'parent_id' => 4,
            ],
            [
                'id' => null,
                'title' => 'INNER COLOR MUG',
                'link' => Str::slug('INNER COLOR MUG'),
                'status' => 'ENABLED',
                'parent_id' => 4,
            ],
            [
                'id' => null,
                'title' => 'PATCH MUG',
                'link' => Str::slug('PATCH MUG'),
                'status' => 'ENABLED',
                'parent_id' => 4,
            ],
            [
                'id' => null,
                'title' => 'MAGIC MUG',
                'link' => Str::slug('MAGIC MUG'),
                'status' => 'ENABLED',
                'parent_id' => 4,
            ],
            [
                'id' => null,
                'title' => 'ANIMAL MUG',
                'link' => Str::slug('ANIMAL MUG'),
                'status' => 'ENABLED',
                'parent_id' => 4,
            ],

// TShirt

            [
                'id' => null,
                'title' => 'PRINTED DRY COLLAR NECK FIT T- SHIRT',
                'link' => Str::slug('PRINTED DRY COLLAR NECK FIT T- SHIRT'),
                'status' => 'ENABLED',
                'parent_id' => 5,
            ],
            [
                'id' => null,
                'title' => 'PRINTED DRY ROUND NECK FIT T- SHIRT',
                'link' => Str::slug('PRINTED DRY ROUND NECK FIT T- SHIRT'),
                'status' => 'ENABLED',
                'parent_id' => 5,
            ],

// More
            [
                'id' => null,
                'title' => 'KEYCHAIN NO. 23',
                'link' => Str::slug('KEYCHAIN NO. 23'),
                'status' => 'ENABLED',
                'parent_id' => 6,
            ],
            [
                'id' => null,
                'title' => 'KEYCHAIN NO. 24',
                'link' => Str::slug('KEYCHAIN NO. 24'),
                'status' => 'ENABLED',
                'parent_id' => 6,
            ],
            [
                'id' => null,
                'title' => 'KEYCHAIN NO. 25',
                'link' => Str::slug('KEYCHAIN NO. 25'),
                'status' => 'ENABLED',
                'parent_id' => 6,
            ],
            [
                'id' => null,
                'title' => 'Caps',
                'link' => Str::slug('Caps'),
                'status' => 'ENABLED',
                'parent_id' => 6,
            ],
            [
                'id' => null,
                'title' => 'Mouse Pad',
                'link' => Str::slug('Mouse Pad'),
                'status' => 'ENABLED',
                'parent_id' => 6,
            ],
            [
                'id' => null,
                'title' => 'Pen',
                'link' => Str::slug('Pen'),
                'status' => 'ENABLED',
                'parent_id' => 6,
            ],
            [
                'id' => null,
                'title' => 'Sipper Bottles',
                'link' => Str::slug('Sipper Bottles'),
                'status' => 'ENABLED',
                'parent_id' => 6,
            ],

        ]);
    }
}
