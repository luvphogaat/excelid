<?php

namespace Database\Seeders;
use App\Models\ProductReview;

use Illuminate\Database\Seeder;

class ProductReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $faker = Faker\Factory::create();

        foreach(range(1, 100) as $index) {
            ProductReview::create([
                'customer_id' => rand(50, 100),
                'product_id' => rand(1, 50),
                'message' => 'Hello world',
                'rating' => rand(1, 5),
                'status' => $this->getRandomStatus()
            ]);
        }
    }

    public function getRandomStatus() {
        $statuses = array('visible', 'hidden');
        return $statuses[array_rand($statuses)];
    }
}
