<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DesignTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('design_templates')->insert([
            [
                'id' => null,
                'category_id' => 1,
                'data' => '{
                "type": "Portrait",
                "area": {
                  "width": 325,
                  "height": 204
                },
                "borderRound": false,
                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/1.jpg",
                "images": [{
                    "type": "photo",
                    "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                    "xAxis": 440,
                    "yAxis": 67,
                    "width": 133,
                    "height": 146.5
                  }
                ],
                "text": [{
                  "text": "CAMBRIDGE SCHOOL",
                  "xAxis": 40,
                  "yAxis": 80,
                  "fontSize": "38px",
                  "color": "#5d6d65"
                },  {
                  "text": "Student Name",
                  "xAxis": 450,
                  "yAxis": 240,
                  "fontSize": "20px",
                  "color": "#000"
                },{
                  "text": "4426",
                  "xAxis": 130,
                  "yAxis": 140,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "10th July 2005",
                  "xAxis": 130,
                  "yAxis": 190,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "Karol Bagh, Delhi",
                  "xAxis": 130,
                  "yAxis": 240,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "9998765647",
                  "xAxis": 130,
                  "yAxis": 290,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "B+ve",
                  "xAxis": 130,
                  "yAxis": 340,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "5th (B)",
                  "xAxis": 500,
                  "yAxis": 300,
                  "fontSize": "18px",
                  "color": "#000"
                }]
            }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/1.jpg',
                'fill_template_one' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/1_1.jpg',
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ], [
                'id' => null,
                'category_id' => 1,
                'data' => '{

                "type": "Portrait",
                "area": {
                  "width": 325,
                  "height": 204
                },
                "borderRound": false,
                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/3.jpg",
                "images": [{
                    "type": "photo",
                    "src": "assets/img/userImageCircle.png",
                    "xAxis": 61,
                    "yAxis": 54,
                    "width": 156,
                    "height": 147.5
                  }
                ],
                "text": [{
                  "text": "CAMBRIDGE SCHOOL",
                  "xAxis": 270,
                  "yAxis": 50,
                  "fontSize": "40px",
                  "color": "#fff"
                },  {
                  "text": "Student Name",
                  "xAxis": 80,
                  "yAxis": 230,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "4426",
                  "xAxis": 380,
                  "yAxis": 175,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "10th July 2005",
                  "xAxis": 380,
                  "yAxis": 218,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "Karol Bagh, Delhi",
                  "xAxis": 380,
                  "yAxis": 265,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "9998765647",
                  "xAxis": 380,
                  "yAxis": 310,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "B+ve",
                  "xAxis": 380,
                  "yAxis": 345,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "5th (B)",
                  "xAxis": 135,
                  "yAxis": 268,
                  "font": "18px Arial",
                  "color": "#fff"
                }]
              }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/2.jpg',
                'fill_template_one' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/2_1.jpg',
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ], [
                'id' => null,
                'category_id' => 1,
                'data' => '{

                "type": "Portrait",
                "area": {
                  "width": 325,
                  "height": 204
                },
                "borderRound": false,
                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/4.jpg",
                "images": [{
                    "type": "photo",
                    "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                    "xAxis": 68,
                    "yAxis": 65,
                    "width": 132.5,
                    "height": 146.5
                  },
                  {
                    "type": "logo",
                    "src": "assets/img/logo.jpg",
                    "xAxis": 210,
                    "yAxis": 57,
                    "width": 60,
                    "height": 25
                  }
                ],
                "text": [{
                  "text": "CAMBRIDGE SCHOOL",
                  "xAxis": 270,
                  "yAxis": 50,
                  "fontSize": "40px",
                  "color": "#000"
                },  {
                  "text": "Student Name",
                  "xAxis": 80,
                  "yAxis": 235,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "4426",
                  "xAxis": 360,
                  "yAxis": 149,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "10th July 2005",
                  "xAxis": 360,
                  "yAxis": 197,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "Karol Bagh, Delhi",
                  "xAxis": 360,
                  "yAxis": 242,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "9998765647",
                  "xAxis": 360,
                  "yAxis": 290,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "B+ve",
                  "xAxis": 360,
                  "yAxis": 338,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "5th (B)",
                  "xAxis": 140,
                  "yAxis": 288,
                  "font": "18px Arial",
                  "color": "#000"
                }]
              }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/3.jpg',
                'fill_template_one' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/3_1.jpg',
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ], [
                'id' => null,
                'category_id' => 1,
                'data' => '{

                "type": "Portrait",
                "area": {
                  "width": 325,
                  "height": 204
                },
                "borderRound": false,
                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/5.jpg",
                "images": [{
                    "type": "photo",
                    "src": "assets/img/userImageCircle.png",
                    "xAxis": 385,
                    "yAxis": 90,
                    "width": 156,
                    "height": 147.5
                  },
                  {
                    "type": "logo",
                    "src": "assets/img/logo.jpg",
                    "xAxis": 210,
                    "yAxis": 57,
                    "width": 60,
                    "height": 25
                  }
                ],
                "text": [{
                  "text": "CAMBRIDGE SCHOOL",
                  "xAxis": 270,
                  "yAxis": 50,
                  "fontSize": "40px",
                  "color": "#000"
                },  {
                  "text": "Student Name",
                  "xAxis": 405,
                  "yAxis": 260,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "4426",
                  "xAxis": 120,
                  "yAxis": 135,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "10th July 2005",
                  "xAxis": 120,
                  "yAxis": 185,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "Karol Bagh, Delhi",
                  "xAxis": 120,
                  "yAxis": 235,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "9998765647",
                  "xAxis": 120,
                  "yAxis": 285,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "B+ve",
                  "xAxis": 120,
                  "yAxis": 330,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "5th (B)",
                  "xAxis": 460,
                  "yAxis": 302,
                  "font": "18px Arial",
                  "color": "#000"
                }]
              }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/4.jpg',
                'fill_template_one' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/4_1.jpg',
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ], [
                'id' => null,
                'category_id' => 1,
                'data' => '{

                "type": "Portrait",
                "area": {
                  "width": 325,
                  "height": 204
                },
                "borderRound": false,
                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/6.jpg",
                "images": [{
                    "type": "photo",
                    "src": "assets/img/userImageCircle.png",
                    "xAxis": 72,
                    "yAxis": 120,
                    "width": 156,
                    "height": 147.5
                  },
                  {
                    "type": "logo",
                    "src": "assets/img/logo.jpg",
                    "xAxis": 210,
                    "yAxis": 57,
                    "width": 60,
                    "height": 25
                  }, {

                ],
                "text": [{
                  "text": "CAMBRIDGE SCHOOL",
                  "xAxis": 270,
                  "yAxis": 50,
                  "fontSize": "40px",
                  "color": "#0598d6"
                },  {
                  "text": "Student Name",
                  "xAxis": 90,
                  "yAxis": 290,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "4426",
                  "xAxis": 410,
                  "yAxis": 78,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "10th July 2005",
                  "xAxis": 410,
                  "yAxis": 118,
                  "fontSize": "20px",
                  "color": "#fffs"
                }, {
                  "text": "Karol Bagh, Delhi",
                  "xAxis": 410,
                  "yAxis": 160,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "9998765647",
                  "xAxis": 410,
                  "yAxis": 203,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "B+ve",
                  "xAxis": 410,
                  "yAxis": 243,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "5th (B)",
                  "xAxis": 130,
                  "yAxis": 333,
                  "font": "18px Arial",
                  "color": "#000"
                }]
              }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/5.jpg',
                'fill_template_one' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/5_1.jpg',
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ], [
                'id' => null,
                'category_id' => 1,
                'data' => '{

                "type": "Portrait",
                "area": {
                  "width": 325,
                  "height": 204
                },
                "borderRound": false,
                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/7.jpg",
                "images": [{
                    "type": "photo",
                    "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3419.jpeg",
                    "xAxis": 438,
                    "yAxis": 100,
                    "width": 132.5,
                    "height": 146.5
                  }
                ],
                "text": [{
                  "text": "CAMBRIDGE SCHOOL",
                  "xAxis": 70,
                  "yAxis": 80,
                  "fontSize": "40px",
                  "color": "#0598d6"
                },  {
                  "text": "Student Name",
                  "xAxis": 448,
                  "yAxis": 265,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "4426",
                  "xAxis": 115,
                  "yAxis": 147,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "10th July 2005",
                  "xAxis": 115,
                  "yAxis": 195,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "Karol Bagh, Delhi",
                  "xAxis": 115,
                  "yAxis": 240,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "9998765647",
                  "xAxis": 115,
                  "yAxis": 290,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "B+ve",
                  "xAxis": 115,
                  "yAxis": 335,
                  "fontSize": "20px",
                  "color": "#000"
                }, {
                  "text": "5th (B)",
                  "xAxis": 530,
                  "yAxis": 345,
                  "fontSize": "16px",
                  "color": "#000"
                }]
              }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/6.jpg',
                'fill_template_one' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/6_1.jpg',
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ], [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                "type": "Portrait",
                "area": {
                  "width": 325,
                  "height": 204
                },
                "borderRound": false,
                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/8.jpg",
                "images": [{
                    "type": "photo",
                    "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                    "xAxis": 439,
                    "yAxis": 100,
                    "width": 132.5,
                    "height": 146.5
                  },
                  {
                    "type": "logo",
                    "src": "assets/img/logo.jpg",
                    "xAxis": 110,
                    "yAxis": 57,
                    "width": 60,
                    "height": 25
                  }
                ],
                "text": [{
                  "text": "CAMBRIDGE SCHOOL",
                  "xAxis": 175,
                  "yAxis": 80,
                  "fontSize": "40px",
                  "color": "#fff"
                },  {
                  "text": "Student Name",
                  "xAxis": 448,
                  "yAxis": 270,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "4426",
                  "xAxis": 105,
                  "yAxis": 205,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "10th July 2005",
                  "xAxis": 105,
                  "yAxis": 240,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "Karol Bagh, Delhi",
                  "xAxis": 105,
                  "yAxis": 283,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "9998765647",
                  "xAxis": 105,
                  "yAxis": 323,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "B+ve",
                  "xAxis": 105,
                  "yAxis": 357,
                  "fontSize": "20px",
                  "color": "#fff"
                }, {
                  "text": "5th (B)",
                  "xAxis": 510,
                  "yAxis": 318,
                  "font": "18px Arial",
                  "color": "#fff"
                }]
              }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/7.jpg',
                'fill_template_one' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/7_1.jpg',
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ], [
                'id' => null,
                'category_id' => 1,

                'data' => '{

              "type": "Portrait",
              "area": {
              "width": 325,
              "height": 204
              },
              "borderRound": false,
              "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
              "images": [{
              "type": "photo",
              "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
              "xAxis": 439,
              "yAxis": 100,
              "width": 132.5,
              "height": 146.5
              },
              {
              "type": "logo",
              "src": "assets/img/logo.jpg",
              "xAxis": 110,
              "yAxis": 57,
              "width": 60,
              "height": 25
              }, {
              ],
              "text": [{
              "text": "CAMBRIDGE SCHOOL",
              "xAxis": 175,
              "yAxis": 80,
              "fontSize": "40px",
              "color": "#fff"
              }, {
              "text": "Student Name",
              "xAxis": 448,
              "yAxis": 270,
              "fontSize": "20px",
              "color": "#fff"
              }, {
              "text": "4426",
              "xAxis": 105,
              "yAxis": 205,
              "fontSize": "20px",
              "color": "#fff"
              }, {
              "text": "10th July 2005",
              "xAxis": 105,
              "yAxis": 240,
              "fontSize": "20px",
              "color": "#fff"
              }, {
              "text": "Karol Bagh, Delhi",
              "xAxis": 105,
              "yAxis": 283,
              "fontSize": "20px",
              "color": "#fff"
              }, {
              "text": "9998765647",
              "xAxis": 105,
              "yAxis": 323,
              "fontSize": "20px",
              "color": "#fff"
              }, {
              "text": "B+ve",
              "xAxis": 105,
              "yAxis": 357,
              "fontSize": "20px",
              "color": "#fff"
              }, {
              "text": "5th (B)",
              "xAxis": 510,
              "yAxis": 318,
              "font": "18px Arial",
              "color": "#fff"
              }]
              }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                "type": "Portrait",
                "area": {
                "width": 325,
                "height": 204
                },
                "borderRound": false,
                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                "images": [{
                "type": "photo",
                "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                "xAxis": 439,
                "yAxis": 100,
                "width": 132.5,
                "height": 146.5
                },
                {
                "type": "logo",
                "src": "assets/img/logo.jpg",
                "xAxis": 110,
                "yAxis": 57,
                "width": 60,
                "height": 25
                }, {
                ],
                "text": [{
                "text": "CAMBRIDGE SCHOOL",
                "xAxis": 175,
                "yAxis": 80,
                "fontSize": "40px",
                "color": "#fff"
                }, {
                "text": "Student Name",
                "xAxis": 448,
                "yAxis": 270,
                "fontSize": "20px",
                "color": "#fff"
                }, {
                "text": "4426",
                "xAxis": 105,
                "yAxis": 205,
                "fontSize": "20px",
                "color": "#fff"
                }, {
                "text": "10th July 2005",
                "xAxis": 105,
                "yAxis": 240,
                "fontSize": "20px",
                "color": "#fff"
                }, {
                "text": "Karol Bagh, Delhi",
                "xAxis": 105,
                "yAxis": 283,
                "fontSize": "20px",
                "color": "#fff"
                }, {
                "text": "9998765647",
                "xAxis": 105,
                "yAxis": 323,
                "fontSize": "20px",
                "color": "#fff"
                }, {
                "text": "B+ve",
                "xAxis": 105,
                "yAxis": 357,
                "fontSize": "20px",
                "color": "#fff"
                }, {
                "text": "5th (B)",
                "xAxis": 510,
                "yAxis": 318,
                "font": "18px Arial",
                "color": "#fff"
                }]
                }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                  "type": "Portrait",
                  "area": {
                  "width": 325,
                  "height": 204
                  },
                  "borderRound": false,
                  "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                  "images": [{
                  "type": "photo",
                  "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                  "xAxis": 439,
                  "yAxis": 100,
                  "width": 132.5,
                  "height": 146.5
                  },
                  {
                  "type": "logo",
                  "src": "assets/img/logo.jpg",
                  "xAxis": 110,
                  "yAxis": 57,
                  "width": 60,
                  "height": 25
                  }, {
                  ],
                  "text": [{
                  "text": "CAMBRIDGE SCHOOL",
                  "xAxis": 175,
                  "yAxis": 80,
                  "fontSize": "40px",
                  "color": "#fff"
                  }, {
                  "text": "Student Name",
                  "xAxis": 448,
                  "yAxis": 270,
                  "fontSize": "20px",
                  "color": "#fff"
                  }, {
                  "text": "4426",
                  "xAxis": 105,
                  "yAxis": 205,
                  "fontSize": "20px",
                  "color": "#fff"
                  }, {
                  "text": "10th July 2005",
                  "xAxis": 105,
                  "yAxis": 240,
                  "fontSize": "20px",
                  "color": "#fff"
                  }, {
                  "text": "Karol Bagh, Delhi",
                  "xAxis": 105,
                  "yAxis": 283,
                  "fontSize": "20px",
                  "color": "#fff"
                  }, {
                  "text": "9998765647",
                  "xAxis": 105,
                  "yAxis": 323,
                  "fontSize": "20px",
                  "color": "#fff"
                  }, {
                  "text": "B+ve",
                  "xAxis": 105,
                  "yAxis": 357,
                  "fontSize": "20px",
                  "color": "#fff"
                  }, {
                  "text": "5th (B)",
                  "xAxis": 510,
                  "yAxis": 318,
                  "font": "18px Arial",
                  "color": "#fff"
                  }]
                  }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                    "type": "Portrait",
                    "area": {
                    "width": 325,
                    "height": 204
                    },
                    "borderRound": false,
                    "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                    "images": [{
                    "type": "photo",
                    "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                    "xAxis": 439,
                    "yAxis": 100,
                    "width": 132.5,
                    "height": 146.5
                    },
                    {
                    "type": "logo",
                    "src": "assets/img/logo.jpg",
                    "xAxis": 110,
                    "yAxis": 57,
                    "width": 60,
                    "height": 25
                    }, {
                    ],
                    "text": [{
                    "text": "CAMBRIDGE SCHOOL",
                    "xAxis": 175,
                    "yAxis": 80,
                    "fontSize": "40px",
                    "color": "#fff"
                    }, {
                    "text": "Student Name",
                    "xAxis": 448,
                    "yAxis": 270,
                    "fontSize": "20px",
                    "color": "#fff"
                    }, {
                    "text": "4426",
                    "xAxis": 105,
                    "yAxis": 205,
                    "fontSize": "20px",
                    "color": "#fff"
                    }, {
                    "text": "10th July 2005",
                    "xAxis": 105,
                    "yAxis": 240,
                    "fontSize": "20px",
                    "color": "#fff"
                    }, {
                    "text": "Karol Bagh, Delhi",
                    "xAxis": 105,
                    "yAxis": 283,
                    "fontSize": "20px",
                    "color": "#fff"
                    }, {
                    "text": "9998765647",
                    "xAxis": 105,
                    "yAxis": 323,
                    "fontSize": "20px",
                    "color": "#fff"
                    }, {
                    "text": "B+ve",
                    "xAxis": 105,
                    "yAxis": 357,
                    "fontSize": "20px",
                    "color": "#fff"
                    }, {
                    "text": "5th (B)",
                    "xAxis": 510,
                    "yAxis": 318,
                    "font": "18px Arial",
                    "color": "#fff"
                    }]
                    }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                      "type": "Portrait",
                      "area": {
                      "width": 325,
                      "height": 204
                      },
                      "borderRound": false,
                      "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                      "images": [{
                      "type": "photo",
                      "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                      "xAxis": 439,
                      "yAxis": 100,
                      "width": 132.5,
                      "height": 146.5
                      },
                      {
                      "type": "logo",
                      "src": "assets/img/logo.jpg",
                      "xAxis": 110,
                      "yAxis": 57,
                      "width": 60,
                      "height": 25
                      }, {
                      ],
                      "text": [{
                      "text": "CAMBRIDGE SCHOOL",
                      "xAxis": 175,
                      "yAxis": 80,
                      "fontSize": "40px",
                      "color": "#fff"
                      }, {
                      "text": "Student Name",
                      "xAxis": 448,
                      "yAxis": 270,
                      "fontSize": "20px",
                      "color": "#fff"
                      }, {
                      "text": "4426",
                      "xAxis": 105,
                      "yAxis": 205,
                      "fontSize": "20px",
                      "color": "#fff"
                      }, {
                      "text": "10th July 2005",
                      "xAxis": 105,
                      "yAxis": 240,
                      "fontSize": "20px",
                      "color": "#fff"
                      }, {
                      "text": "Karol Bagh, Delhi",
                      "xAxis": 105,
                      "yAxis": 283,
                      "fontSize": "20px",
                      "color": "#fff"
                      }, {
                      "text": "9998765647",
                      "xAxis": 105,
                      "yAxis": 323,
                      "fontSize": "20px",
                      "color": "#fff"
                      }, {
                      "text": "B+ve",
                      "xAxis": 105,
                      "yAxis": 357,
                      "fontSize": "20px",
                      "color": "#fff"
                      }, {
                      "text": "5th (B)",
                      "xAxis": 510,
                      "yAxis": 318,
                      "font": "18px Arial",
                      "color": "#fff"
                      }]
                      }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                        "type": "Portrait",
                        "area": {
                        "width": 325,
                        "height": 204
                        },
                        "borderRound": false,
                        "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                        "images": [{
                        "type": "photo",
                        "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                        "xAxis": 439,
                        "yAxis": 100,
                        "width": 132.5,
                        "height": 146.5
                        },
                        {
                        "type": "logo",
                        "src": "assets/img/logo.jpg",
                        "xAxis": 110,
                        "yAxis": 57,
                        "width": 60,
                        "height": 25
                        }, {
                        ],
                        "text": [{
                        "text": "CAMBRIDGE SCHOOL",
                        "xAxis": 175,
                        "yAxis": 80,
                        "fontSize": "40px",
                        "color": "#fff"
                        }, {
                        "text": "Student Name",
                        "xAxis": 448,
                        "yAxis": 270,
                        "fontSize": "20px",
                        "color": "#fff"
                        }, {
                        "text": "4426",
                        "xAxis": 105,
                        "yAxis": 205,
                        "fontSize": "20px",
                        "color": "#fff"
                        }, {
                        "text": "10th July 2005",
                        "xAxis": 105,
                        "yAxis": 240,
                        "fontSize": "20px",
                        "color": "#fff"
                        }, {
                        "text": "Karol Bagh, Delhi",
                        "xAxis": 105,
                        "yAxis": 283,
                        "fontSize": "20px",
                        "color": "#fff"
                        }, {
                        "text": "9998765647",
                        "xAxis": 105,
                        "yAxis": 323,
                        "fontSize": "20px",
                        "color": "#fff"
                        }, {
                        "text": "B+ve",
                        "xAxis": 105,
                        "yAxis": 357,
                        "fontSize": "20px",
                        "color": "#fff"
                        }, {
                        "text": "5th (B)",
                        "xAxis": 510,
                        "yAxis": 318,
                        "font": "18px Arial",
                        "color": "#fff"
                        }]
                        }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                          "type": "Portrait",
                          "area": {
                          "width": 325,
                          "height": 204
                          },
                          "borderRound": false,
                          "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                          "images": [{
                          "type": "photo",
                          "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                          "xAxis": 439,
                          "yAxis": 100,
                          "width": 132.5,
                          "height": 146.5
                          },
                          {
                          "type": "logo",
                          "src": "assets/img/logo.jpg",
                          "xAxis": 110,
                          "yAxis": 57,
                          "width": 60,
                          "height": 25
                          }, {
                          ],
                          "text": [{
                          "text": "CAMBRIDGE SCHOOL",
                          "xAxis": 175,
                          "yAxis": 80,
                          "fontSize": "40px",
                          "color": "#fff"
                          }, {
                          "text": "Student Name",
                          "xAxis": 448,
                          "yAxis": 270,
                          "fontSize": "20px",
                          "color": "#fff"
                          }, {
                          "text": "4426",
                          "xAxis": 105,
                          "yAxis": 205,
                          "fontSize": "20px",
                          "color": "#fff"
                          }, {
                          "text": "10th July 2005",
                          "xAxis": 105,
                          "yAxis": 240,
                          "fontSize": "20px",
                          "color": "#fff"
                          }, {
                          "text": "Karol Bagh, Delhi",
                          "xAxis": 105,
                          "yAxis": 283,
                          "fontSize": "20px",
                          "color": "#fff"
                          }, {
                          "text": "9998765647",
                          "xAxis": 105,
                          "yAxis": 323,
                          "fontSize": "20px",
                          "color": "#fff"
                          }, {
                          "text": "B+ve",
                          "xAxis": 105,
                          "yAxis": 357,
                          "fontSize": "20px",
                          "color": "#fff"
                          }, {
                          "text": "5th (B)",
                          "xAxis": 510,
                          "yAxis": 318,
                          "font": "18px Arial",
                          "color": "#fff"
                          }]
                          }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                            "type": "Portrait",
                            "area": {
                            "width": 325,
                            "height": 204
                            },
                            "borderRound": false,
                            "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                            "images": [{
                            "type": "photo",
                            "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                            "xAxis": 439,
                            "yAxis": 100,
                            "width": 132.5,
                            "height": 146.5
                            },
                            {
                            "type": "logo",
                            "src": "assets/img/logo.jpg",
                            "xAxis": 110,
                            "yAxis": 57,
                            "width": 60,
                            "height": 25
                            }, {
                            ],
                            "text": [{
                            "text": "CAMBRIDGE SCHOOL",
                            "xAxis": 175,
                            "yAxis": 80,
                            "fontSize": "40px",
                            "color": "#fff"
                            }, {
                            "text": "Student Name",
                            "xAxis": 448,
                            "yAxis": 270,
                            "fontSize": "20px",
                            "color": "#fff"
                            }, {
                            "text": "4426",
                            "xAxis": 105,
                            "yAxis": 205,
                            "fontSize": "20px",
                            "color": "#fff"
                            }, {
                            "text": "10th July 2005",
                            "xAxis": 105,
                            "yAxis": 240,
                            "fontSize": "20px",
                            "color": "#fff"
                            }, {
                            "text": "Karol Bagh, Delhi",
                            "xAxis": 105,
                            "yAxis": 283,
                            "fontSize": "20px",
                            "color": "#fff"
                            }, {
                            "text": "9998765647",
                            "xAxis": 105,
                            "yAxis": 323,
                            "fontSize": "20px",
                            "color": "#fff"
                            }, {
                            "text": "B+ve",
                            "xAxis": 105,
                            "yAxis": 357,
                            "fontSize": "20px",
                            "color": "#fff"
                            }, {
                            "text": "5th (B)",
                            "xAxis": 510,
                            "yAxis": 318,
                            "font": "18px Arial",
                            "color": "#fff"
                            }]
                            }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                              "type": "Portrait",
                              "area": {
                              "width": 325,
                              "height": 204
                              },
                              "borderRound": false,
                              "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                              "images": [{
                              "type": "photo",
                              "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                              "xAxis": 439,
                              "yAxis": 100,
                              "width": 132.5,
                              "height": 146.5
                              },
                              {
                              "type": "logo",
                              "src": "assets/img/logo.jpg",
                              "xAxis": 110,
                              "yAxis": 57,
                              "width": 60,
                              "height": 25
                              }, {
                              ],
                              "text": [{
                              "text": "CAMBRIDGE SCHOOL",
                              "xAxis": 175,
                              "yAxis": 80,
                              "fontSize": "40px",
                              "color": "#fff"
                              }, {
                              "text": "Student Name",
                              "xAxis": 448,
                              "yAxis": 270,
                              "fontSize": "20px",
                              "color": "#fff"
                              }, {
                              "text": "4426",
                              "xAxis": 105,
                              "yAxis": 205,
                              "fontSize": "20px",
                              "color": "#fff"
                              }, {
                              "text": "10th July 2005",
                              "xAxis": 105,
                              "yAxis": 240,
                              "fontSize": "20px",
                              "color": "#fff"
                              }, {
                              "text": "Karol Bagh, Delhi",
                              "xAxis": 105,
                              "yAxis": 283,
                              "fontSize": "20px",
                              "color": "#fff"
                              }, {
                              "text": "9998765647",
                              "xAxis": 105,
                              "yAxis": 323,
                              "fontSize": "20px",
                              "color": "#fff"
                              }, {
                              "text": "B+ve",
                              "xAxis": 105,
                              "yAxis": 357,
                              "fontSize": "20px",
                              "color": "#fff"
                              }, {
                              "text": "5th (B)",
                              "xAxis": 510,
                              "yAxis": 318,
                              "font": "18px Arial",
                              "color": "#fff"
                              }]
                              }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                "type": "Portrait",
                                "area": {
                                "width": 325,
                                "height": 204
                                },
                                "borderRound": false,
                                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                "images": [{
                                "type": "photo",
                                "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                "xAxis": 439,
                                "yAxis": 100,
                                "width": 132.5,
                                "height": 146.5
                                },
                                {
                                "type": "logo",
                                "src": "assets/img/logo.jpg",
                                "xAxis": 110,
                                "yAxis": 57,
                                "width": 60,
                                "height": 25
                                }, {
                                ],
                                "text": [{
                                "text": "CAMBRIDGE SCHOOL",
                                "xAxis": 175,
                                "yAxis": 80,
                                "fontSize": "40px",
                                "color": "#fff"
                                }, {
                                "text": "Student Name",
                                "xAxis": 448,
                                "yAxis": 270,
                                "fontSize": "20px",
                                "color": "#fff"
                                }, {
                                "text": "4426",
                                "xAxis": 105,
                                "yAxis": 205,
                                "fontSize": "20px",
                                "color": "#fff"
                                }, {
                                "text": "10th July 2005",
                                "xAxis": 105,
                                "yAxis": 240,
                                "fontSize": "20px",
                                "color": "#fff"
                                }, {
                                "text": "Karol Bagh, Delhi",
                                "xAxis": 105,
                                "yAxis": 283,
                                "fontSize": "20px",
                                "color": "#fff"
                                }, {
                                "text": "9998765647",
                                "xAxis": 105,
                                "yAxis": 323,
                                "fontSize": "20px",
                                "color": "#fff"
                                }, {
                                "text": "B+ve",
                                "xAxis": 105,
                                "yAxis": 357,
                                "fontSize": "20px",
                                "color": "#fff"
                                }, {
                                "text": "5th (B)",
                                "xAxis": 510,
                                "yAxis": 318,
                                "font": "18px Arial",
                                "color": "#fff"
                                }]
                                }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                  "type": "Portrait",
                                  "area": {
                                  "width": 325,
                                  "height": 204
                                  },
                                  "borderRound": false,
                                  "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                  "images": [{
                                  "type": "photo",
                                  "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                  "xAxis": 439,
                                  "yAxis": 100,
                                  "width": 132.5,
                                  "height": 146.5
                                  },
                                  {
                                  "type": "logo",
                                  "src": "assets/img/logo.jpg",
                                  "xAxis": 110,
                                  "yAxis": 57,
                                  "width": 60,
                                  "height": 25
                                  }, {
                                  ],
                                  "text": [{
                                  "text": "CAMBRIDGE SCHOOL",
                                  "xAxis": 175,
                                  "yAxis": 80,
                                  "fontSize": "40px",
                                  "color": "#fff"
                                  }, {
                                  "text": "Student Name",
                                  "xAxis": 448,
                                  "yAxis": 270,
                                  "fontSize": "20px",
                                  "color": "#fff"
                                  }, {
                                  "text": "4426",
                                  "xAxis": 105,
                                  "yAxis": 205,
                                  "fontSize": "20px",
                                  "color": "#fff"
                                  }, {
                                  "text": "10th July 2005",
                                  "xAxis": 105,
                                  "yAxis": 240,
                                  "fontSize": "20px",
                                  "color": "#fff"
                                  }, {
                                  "text": "Karol Bagh, Delhi",
                                  "xAxis": 105,
                                  "yAxis": 283,
                                  "fontSize": "20px",
                                  "color": "#fff"
                                  }, {
                                  "text": "9998765647",
                                  "xAxis": 105,
                                  "yAxis": 323,
                                  "fontSize": "20px",
                                  "color": "#fff"
                                  }, {
                                  "text": "B+ve",
                                  "xAxis": 105,
                                  "yAxis": 357,
                                  "fontSize": "20px",
                                  "color": "#fff"
                                  }, {
                                  "text": "5th (B)",
                                  "xAxis": 510,
                                  "yAxis": 318,
                                  "font": "18px Arial",
                                  "color": "#fff"
                                  }]
                                  }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                    "type": "Portrait",
                                    "area": {
                                    "width": 325,
                                    "height": 204
                                    },
                                    "borderRound": false,
                                    "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                    "images": [{
                                    "type": "photo",
                                    "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                    "xAxis": 439,
                                    "yAxis": 100,
                                    "width": 132.5,
                                    "height": 146.5
                                    },
                                    {
                                    "type": "logo",
                                    "src": "assets/img/logo.jpg",
                                    "xAxis": 110,
                                    "yAxis": 57,
                                    "width": 60,
                                    "height": 25
                                    }, {
                                    ],
                                    "text": [{
                                    "text": "CAMBRIDGE SCHOOL",
                                    "xAxis": 175,
                                    "yAxis": 80,
                                    "fontSize": "40px",
                                    "color": "#fff"
                                    }, {
                                    "text": "Student Name",
                                    "xAxis": 448,
                                    "yAxis": 270,
                                    "fontSize": "20px",
                                    "color": "#fff"
                                    }, {
                                    "text": "4426",
                                    "xAxis": 105,
                                    "yAxis": 205,
                                    "fontSize": "20px",
                                    "color": "#fff"
                                    }, {
                                    "text": "10th July 2005",
                                    "xAxis": 105,
                                    "yAxis": 240,
                                    "fontSize": "20px",
                                    "color": "#fff"
                                    }, {
                                    "text": "Karol Bagh, Delhi",
                                    "xAxis": 105,
                                    "yAxis": 283,
                                    "fontSize": "20px",
                                    "color": "#fff"
                                    }, {
                                    "text": "9998765647",
                                    "xAxis": 105,
                                    "yAxis": 323,
                                    "fontSize": "20px",
                                    "color": "#fff"
                                    }, {
                                    "text": "B+ve",
                                    "xAxis": 105,
                                    "yAxis": 357,
                                    "fontSize": "20px",
                                    "color": "#fff"
                                    }, {
                                    "text": "5th (B)",
                                    "xAxis": 510,
                                    "yAxis": 318,
                                    "font": "18px Arial",
                                    "color": "#fff"
                                    }]
                                    }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                      "type": "Portrait",
                                      "area": {
                                      "width": 325,
                                      "height": 204
                                      },
                                      "borderRound": false,
                                      "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                      "images": [{
                                      "type": "photo",
                                      "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                      "xAxis": 439,
                                      "yAxis": 100,
                                      "width": 132.5,
                                      "height": 146.5
                                      },
                                      {
                                      "type": "logo",
                                      "src": "assets/img/logo.jpg",
                                      "xAxis": 110,
                                      "yAxis": 57,
                                      "width": 60,
                                      "height": 25
                                      }, {
                                      ],
                                      "text": [{
                                      "text": "CAMBRIDGE SCHOOL",
                                      "xAxis": 175,
                                      "yAxis": 80,
                                      "fontSize": "40px",
                                      "color": "#fff"
                                      }, {
                                      "text": "Student Name",
                                      "xAxis": 448,
                                      "yAxis": 270,
                                      "fontSize": "20px",
                                      "color": "#fff"
                                      }, {
                                      "text": "4426",
                                      "xAxis": 105,
                                      "yAxis": 205,
                                      "fontSize": "20px",
                                      "color": "#fff"
                                      }, {
                                      "text": "10th July 2005",
                                      "xAxis": 105,
                                      "yAxis": 240,
                                      "fontSize": "20px",
                                      "color": "#fff"
                                      }, {
                                      "text": "Karol Bagh, Delhi",
                                      "xAxis": 105,
                                      "yAxis": 283,
                                      "fontSize": "20px",
                                      "color": "#fff"
                                      }, {
                                      "text": "9998765647",
                                      "xAxis": 105,
                                      "yAxis": 323,
                                      "fontSize": "20px",
                                      "color": "#fff"
                                      }, {
                                      "text": "B+ve",
                                      "xAxis": 105,
                                      "yAxis": 357,
                                      "fontSize": "20px",
                                      "color": "#fff"
                                      }, {
                                      "text": "5th (B)",
                                      "xAxis": 510,
                                      "yAxis": 318,
                                      "font": "18px Arial",
                                      "color": "#fff"
                                      }]
                                      }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                        "type": "Portrait",
                                        "area": {
                                        "width": 325,
                                        "height": 204
                                        },
                                        "borderRound": false,
                                        "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                        "images": [{
                                        "type": "photo",
                                        "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                        "xAxis": 439,
                                        "yAxis": 100,
                                        "width": 132.5,
                                        "height": 146.5
                                        },
                                        {
                                        "type": "logo",
                                        "src": "assets/img/logo.jpg",
                                        "xAxis": 110,
                                        "yAxis": 57,
                                        "width": 60,
                                        "height": 25
                                        }, {
                                        ],
                                        "text": [{
                                        "text": "CAMBRIDGE SCHOOL",
                                        "xAxis": 175,
                                        "yAxis": 80,
                                        "fontSize": "40px",
                                        "color": "#fff"
                                        }, {
                                        "text": "Student Name",
                                        "xAxis": 448,
                                        "yAxis": 270,
                                        "fontSize": "20px",
                                        "color": "#fff"
                                        }, {
                                        "text": "4426",
                                        "xAxis": 105,
                                        "yAxis": 205,
                                        "fontSize": "20px",
                                        "color": "#fff"
                                        }, {
                                        "text": "10th July 2005",
                                        "xAxis": 105,
                                        "yAxis": 240,
                                        "fontSize": "20px",
                                        "color": "#fff"
                                        }, {
                                        "text": "Karol Bagh, Delhi",
                                        "xAxis": 105,
                                        "yAxis": 283,
                                        "fontSize": "20px",
                                        "color": "#fff"
                                        }, {
                                        "text": "9998765647",
                                        "xAxis": 105,
                                        "yAxis": 323,
                                        "fontSize": "20px",
                                        "color": "#fff"
                                        }, {
                                        "text": "B+ve",
                                        "xAxis": 105,
                                        "yAxis": 357,
                                        "fontSize": "20px",
                                        "color": "#fff"
                                        }, {
                                        "text": "5th (B)",
                                        "xAxis": 510,
                                        "yAxis": 318,
                                        "font": "18px Arial",
                                        "color": "#fff"
                                        }]
                                        }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                          "type": "Portrait",
                                          "area": {
                                          "width": 325,
                                          "height": 204
                                          },
                                          "borderRound": false,
                                          "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                          "images": [{
                                          "type": "photo",
                                          "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                          "xAxis": 439,
                                          "yAxis": 100,
                                          "width": 132.5,
                                          "height": 146.5
                                          },
                                          {
                                          "type": "logo",
                                          "src": "assets/img/logo.jpg",
                                          "xAxis": 110,
                                          "yAxis": 57,
                                          "width": 60,
                                          "height": 25
                                          }, {
                                          ],
                                          "text": [{
                                          "text": "CAMBRIDGE SCHOOL",
                                          "xAxis": 175,
                                          "yAxis": 80,
                                          "fontSize": "40px",
                                          "color": "#fff"
                                          }, {
                                          "text": "Student Name",
                                          "xAxis": 448,
                                          "yAxis": 270,
                                          "fontSize": "20px",
                                          "color": "#fff"
                                          }, {
                                          "text": "4426",
                                          "xAxis": 105,
                                          "yAxis": 205,
                                          "fontSize": "20px",
                                          "color": "#fff"
                                          }, {
                                          "text": "10th July 2005",
                                          "xAxis": 105,
                                          "yAxis": 240,
                                          "fontSize": "20px",
                                          "color": "#fff"
                                          }, {
                                          "text": "Karol Bagh, Delhi",
                                          "xAxis": 105,
                                          "yAxis": 283,
                                          "fontSize": "20px",
                                          "color": "#fff"
                                          }, {
                                          "text": "9998765647",
                                          "xAxis": 105,
                                          "yAxis": 323,
                                          "fontSize": "20px",
                                          "color": "#fff"
                                          }, {
                                          "text": "B+ve",
                                          "xAxis": 105,
                                          "yAxis": 357,
                                          "fontSize": "20px",
                                          "color": "#fff"
                                          }, {
                                          "text": "5th (B)",
                                          "xAxis": 510,
                                          "yAxis": 318,
                                          "font": "18px Arial",
                                          "color": "#fff"
                                          }]
                                          }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                            "type": "Portrait",
                                            "area": {
                                            "width": 325,
                                            "height": 204
                                            },
                                            "borderRound": false,
                                            "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                            "images": [{
                                            "type": "photo",
                                            "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                            "xAxis": 439,
                                            "yAxis": 100,
                                            "width": 132.5,
                                            "height": 146.5
                                            },
                                            {
                                            "type": "logo",
                                            "src": "assets/img/logo.jpg",
                                            "xAxis": 110,
                                            "yAxis": 57,
                                            "width": 60,
                                            "height": 25
                                            }, {
                                            ],
                                            "text": [{
                                            "text": "CAMBRIDGE SCHOOL",
                                            "xAxis": 175,
                                            "yAxis": 80,
                                            "fontSize": "40px",
                                            "color": "#fff"
                                            }, {
                                            "text": "Student Name",
                                            "xAxis": 448,
                                            "yAxis": 270,
                                            "fontSize": "20px",
                                            "color": "#fff"
                                            }, {
                                            "text": "4426",
                                            "xAxis": 105,
                                            "yAxis": 205,
                                            "fontSize": "20px",
                                            "color": "#fff"
                                            }, {
                                            "text": "10th July 2005",
                                            "xAxis": 105,
                                            "yAxis": 240,
                                            "fontSize": "20px",
                                            "color": "#fff"
                                            }, {
                                            "text": "Karol Bagh, Delhi",
                                            "xAxis": 105,
                                            "yAxis": 283,
                                            "fontSize": "20px",
                                            "color": "#fff"
                                            }, {
                                            "text": "9998765647",
                                            "xAxis": 105,
                                            "yAxis": 323,
                                            "fontSize": "20px",
                                            "color": "#fff"
                                            }, {
                                            "text": "B+ve",
                                            "xAxis": 105,
                                            "yAxis": 357,
                                            "fontSize": "20px",
                                            "color": "#fff"
                                            }, {
                                            "text": "5th (B)",
                                            "xAxis": 510,
                                            "yAxis": 318,
                                            "font": "18px Arial",
                                            "color": "#fff"
                                            }]
                                            }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                              "type": "Portrait",
                                              "area": {
                                              "width": 325,
                                              "height": 204
                                              },
                                              "borderRound": false,
                                              "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                              "images": [{
                                              "type": "photo",
                                              "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                              "xAxis": 439,
                                              "yAxis": 100,
                                              "width": 132.5,
                                              "height": 146.5
                                              },
                                              {
                                              "type": "logo",
                                              "src": "assets/img/logo.jpg",
                                              "xAxis": 110,
                                              "yAxis": 57,
                                              "width": 60,
                                              "height": 25
                                              }, {
                                              ],
                                              "text": [{
                                              "text": "CAMBRIDGE SCHOOL",
                                              "xAxis": 175,
                                              "yAxis": 80,
                                              "fontSize": "40px",
                                              "color": "#fff"
                                              }, {
                                              "text": "Student Name",
                                              "xAxis": 448,
                                              "yAxis": 270,
                                              "fontSize": "20px",
                                              "color": "#fff"
                                              }, {
                                              "text": "4426",
                                              "xAxis": 105,
                                              "yAxis": 205,
                                              "fontSize": "20px",
                                              "color": "#fff"
                                              }, {
                                              "text": "10th July 2005",
                                              "xAxis": 105,
                                              "yAxis": 240,
                                              "fontSize": "20px",
                                              "color": "#fff"
                                              }, {
                                              "text": "Karol Bagh, Delhi",
                                              "xAxis": 105,
                                              "yAxis": 283,
                                              "fontSize": "20px",
                                              "color": "#fff"
                                              }, {
                                              "text": "9998765647",
                                              "xAxis": 105,
                                              "yAxis": 323,
                                              "fontSize": "20px",
                                              "color": "#fff"
                                              }, {
                                              "text": "B+ve",
                                              "xAxis": 105,
                                              "yAxis": 357,
                                              "fontSize": "20px",
                                              "color": "#fff"
                                              }, {
                                              "text": "5th (B)",
                                              "xAxis": 510,
                                              "yAxis": 318,
                                              "font": "18px Arial",
                                              "color": "#fff"
                                              }]
                                              }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                                "type": "Portrait",
                                                "area": {
                                                "width": 325,
                                                "height": 204
                                                },
                                                "borderRound": false,
                                                "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                                "images": [{
                                                "type": "photo",
                                                "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                                "xAxis": 439,
                                                "yAxis": 100,
                                                "width": 132.5,
                                                "height": 146.5
                                                },
                                                {
                                                "type": "logo",
                                                "src": "assets/img/logo.jpg",
                                                "xAxis": 110,
                                                "yAxis": 57,
                                                "width": 60,
                                                "height": 25
                                                }, {
                                                ],
                                                "text": [{
                                                "text": "CAMBRIDGE SCHOOL",
                                                "xAxis": 175,
                                                "yAxis": 80,
                                                "fontSize": "40px",
                                                "color": "#fff"
                                                }, {
                                                "text": "Student Name",
                                                "xAxis": 448,
                                                "yAxis": 270,
                                                "fontSize": "20px",
                                                "color": "#fff"
                                                }, {
                                                "text": "4426",
                                                "xAxis": 105,
                                                "yAxis": 205,
                                                "fontSize": "20px",
                                                "color": "#fff"
                                                }, {
                                                "text": "10th July 2005",
                                                "xAxis": 105,
                                                "yAxis": 240,
                                                "fontSize": "20px",
                                                "color": "#fff"
                                                }, {
                                                "text": "Karol Bagh, Delhi",
                                                "xAxis": 105,
                                                "yAxis": 283,
                                                "fontSize": "20px",
                                                "color": "#fff"
                                                }, {
                                                "text": "9998765647",
                                                "xAxis": 105,
                                                "yAxis": 323,
                                                "fontSize": "20px",
                                                "color": "#fff"
                                                }, {
                                                "text": "B+ve",
                                                "xAxis": 105,
                                                "yAxis": 357,
                                                "fontSize": "20px",
                                                "color": "#fff"
                                                }, {
                                                "text": "5th (B)",
                                                "xAxis": 510,
                                                "yAxis": 318,
                                                "font": "18px Arial",
                                                "color": "#fff"
                                                }]
                                                }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ]
            , [
                'id' => null,
                'category_id' => 1,

                'data' => '{

                                                  "type": "Portrait",
                                                  "area": {
                                                  "width": 325,
                                                  "height": 204
                                                  },
                                                  "borderRound": false,
                                                  "templateImg": "https://resources.excelidcardsolutions.com/images/templates/20.jpg",
                                                  "images": [{
                                                  "type": "photo",
                                                  "src": "https://resources.excelidcardsolutions.com/images/props/sample-1610830061.3414.jpeg",
                                                  "xAxis": 439,
                                                  "yAxis": 100,
                                                  "width": 132.5,
                                                  "height": 146.5
                                                  },
                                                  {
                                                  "type": "logo",
                                                  "src": "assets/img/logo.jpg",
                                                  "xAxis": 110,
                                                  "yAxis": 57,
                                                  "width": 60,
                                                  "height": 25
                                                  }, {
                                                  ],
                                                  "text": [{
                                                  "text": "CAMBRIDGE SCHOOL",
                                                  "xAxis": 175,
                                                  "yAxis": 80,
                                                  "fontSize": "40px",
                                                  "color": "#fff"
                                                  }, {
                                                  "text": "Student Name",
                                                  "xAxis": 448,
                                                  "yAxis": 270,
                                                  "fontSize": "20px",
                                                  "color": "#fff"
                                                  }, {
                                                  "text": "4426",
                                                  "xAxis": 105,
                                                  "yAxis": 205,
                                                  "fontSize": "20px",
                                                  "color": "#fff"
                                                  }, {
                                                  "text": "10th July 2005",
                                                  "xAxis": 105,
                                                  "yAxis": 240,
                                                  "fontSize": "20px",
                                                  "color": "#fff"
                                                  }, {
                                                  "text": "Karol Bagh, Delhi",
                                                  "xAxis": 105,
                                                  "yAxis": 283,
                                                  "fontSize": "20px",
                                                  "color": "#fff"
                                                  }, {
                                                  "text": "9998765647",
                                                  "xAxis": 105,
                                                  "yAxis": 323,
                                                  "fontSize": "20px",
                                                  "color": "#fff"
                                                  }, {
                                                  "text": "B+ve",
                                                  "xAxis": 105,
                                                  "yAxis": 357,
                                                  "fontSize": "20px",
                                                  "color": "#fff"
                                                  }, {
                                                  "text": "5th (B)",
                                                  "xAxis": 510,
                                                  "yAxis": 318,
                                                  "font": "18px Arial",
                                                  "color": "#fff"
                                                  }]
                                                  }',
                'fill_template' => 'https://resources.excelidcardsolutions.com/images/fillTemplates/20.jpg',
                'fill_template_one' => null,
                'fontName' => 'Franklin Gothic Medium Regular',
                'fontUrl' => 'https://resources.excelidcardsolutions.com/fonts/Franklin Gothic Medium Regular.ttf',
            ],

        ]);
    }
}
