<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobSize2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('job_size')->insert([
            //  Student Id Cards Single
           [
               'id' => NULL,
               'product_id' => 1,
               'size_name' => '86x54 MM',
           ],
        ]);
    }
}
