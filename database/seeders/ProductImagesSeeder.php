<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('product_images')->insert([[
            'product_id' => 5,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/3.jpg'
        ], [
            'product_id' => 5,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/3.jpg'
        ], [
            'product_id' => 6,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/4.jpg'
        ], [
            'product_id' => 7,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/4.jpg'
        ], [
            'product_id' => 9,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/6.jpg'
        ], [
            'product_id' => 10,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/7.jpg'
        ], [
            'product_id' => 15,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/12.png'
        ], [
            'product_id' => 18,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/15.jpg'
        ], [
            'product_id' => 23,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/20.png'
        ], [
            'product_id' => 24,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/21.jpg'
        ], [
            'product_id' => 25,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/22.jpg'
        ], [
            'product_id' => 29,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/24.png'
        ], [
            'product_id' => 30,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/25.jpg'
        ], [
            'product_id' => 31,
            'product_image' => 'http://resources.excelidcardsolutions.com/images/products/26.jpg'
        ]]);
    }
}
