<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('menus')->insert([[
            'id' => NULL,
            'title' => 'PLASTIC CARDS & ACCESSORIES',
            'link' => 'products/plastic_cards_and_accessories',
            'status' => 'ENABLED',
            'parent_id' => 0
        ],
        [
            'id' => NULL,
            'title' => 'CUSTOMIZED LANYARD',
            'link' => 'products/customized_lanyard',
            'status' => 'ENABLED',
            'parent_id' => 0
        ],
        [
            'id' => NULL,
            'title' => 'BUTTON BADGES',
            'link' => 'products/badges',
            'status' => 'ENABLED',
            'parent_id' => 0
        ],
        [
            'id' => NULL,
            'title' => 'MUGS',
            'link' => 'products/mugs',
            'status' => 'ENABLED',
            'parent_id' => 0
        ],
        [
            'id' => NULL,
            'title' => 'T-SHIRTS',
            'link' => 'products/t_shirt',
            'status' => 'ENABLED',
            'parent_id' => 0
        ],
        [
            'id' => NULL,
            'title' => 'More',
            'link' => '',
            'status' => 'ENABLED',
            'parent_id' => 0
        ]]);
    }
}
