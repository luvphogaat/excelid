<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\ProductQtyPrices;

class ProductQtySizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
            ProductQtyPrices::create( [
            'id'=> NULL,
            'product_id'=>1,
            'quantity'=>50,
            'size_id'=>1,
            'price'=>25.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:00:43'
            ] );

            ProductQtyPrices::create( [
            'id'=> NULL,
            'product_id'=>1,
            'quantity'=>100,
            'size_id'=>1,
            'price'=>20.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:00:49'
            ] );

            ProductQtyPrices::create( [
            'id'=>3,
            'product_id'=>2,
            'quantity'=>50,
            'size_id'=>2,
            'price'=>30.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:01:31'
            ] );

            ProductQtyPrices::create( [
            'id'=>4,
            'product_id'=>3,
            'quantity'=>10,
            'size_id'=>3,
            'price'=>22.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:42:26'
            ] );

            ProductQtyPrices::create( [
            'id'=>5,
            'product_id'=>4,
            'quantity'=>10,
            'size_id'=>4,
            'price'=>28.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:42:29'
            ] );

            ProductQtyPrices::create( [
            'id'=>6,
            'product_id'=>5,
            'quantity'=>100,
            'size_id'=>5,
            'price'=>300.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:42:32'
            ] );

            ProductQtyPrices::create( [
            'id'=>7,
            'product_id'=>5,
            'quantity'=>200,
            'size_id'=>5,
            'price'=>500.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:42:34'
            ] );

            ProductQtyPrices::create( [
            'id'=>8,
            'product_id'=>5,
            'quantity'=>500,
            'size_id'=>5,
            'price'=>1000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:42:37'
            ] );

            ProductQtyPrices::create( [
            'id'=>9,
            'product_id'=>5,
            'quantity'=>1000,
            'size_id'=>5,
            'price'=>1900.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:42:39'
            ] );

            ProductQtyPrices::create( [
            'id'=>10,
            'product_id'=>39,
            'quantity'=>100,
            'size_id'=>18,
            'price'=>400.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 17:49:51'
            ] );

            ProductQtyPrices::create( [
            'id'=>11,
            'product_id'=>39,
            'quantity'=>200,
            'size_id'=>18,
            'price'=>600.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 17:49:54'
            ] );

            ProductQtyPrices::create( [
            'id'=>12,
            'product_id'=>39,
            'quantity'=>500,
            'size_id'=>18,
            'price'=>1250.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 17:49:56'
            ] );

            ProductQtyPrices::create( [
            'id'=>13,
            'product_id'=>39,
            'quantity'=>1000,
            'size_id'=>18,
            'price'=>2400.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 17:50:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>14,
            'product_id'=>6,
            'quantity'=>10,
            'size_id'=>6,
            'price'=>28.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:43:26'
            ] );

            ProductQtyPrices::create( [
            'id'=>15,
            'product_id'=>7,
            'quantity'=>10,
            'size_id'=>7,
            'price'=>22.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:43:28'
            ] );

            ProductQtyPrices::create( [
            'id'=>16,
            'product_id'=>8,
            'quantity'=>10,
            'size_id'=>8,
            'price'=>28.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:43:31'
            ] );

            ProductQtyPrices::create( [
            'id'=>17,
            'product_id'=>9,
            'quantity'=>10,
            'size_id'=>9,
            'price'=>22.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:43:33'
            ] );

            ProductQtyPrices::create( [
            'id'=>76,
            'product_id'=>17,
            'quantity'=>100,
            'size_id'=>19,
            'price'=>1200.00,
            'created_at'=>'2021-06-20 12:51:08',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>19,
            'product_id'=>11,
            'quantity'=>100,
            'size_id'=>11,
            'price'=>900.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:51:54'
            ] );

            ProductQtyPrices::create( [
            'id'=>20,
            'product_id'=>11,
            'quantity'=>200,
            'size_id'=>11,
            'price'=>1800.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:52:06'
            ] );

            ProductQtyPrices::create( [
            'id'=>21,
            'product_id'=>11,
            'quantity'=>300,
            'size_id'=>11,
            'price'=>2550.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:52:13'
            ] );

            ProductQtyPrices::create( [
            'id'=>22,
            'product_id'=>11,
            'quantity'=>400,
            'size_id'=>11,
            'price'=>3400.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:52:20'
            ] );

            ProductQtyPrices::create( [
            'id'=>23,
            'product_id'=>11,
            'quantity'=>500,
            'size_id'=>11,
            'price'=>3500.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:52:25'
            ] );

            ProductQtyPrices::create( [
            'id'=>24,
            'product_id'=>11,
            'quantity'=>1000,
            'size_id'=>11,
            'price'=>6000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:52:30'
            ] );

            ProductQtyPrices::create( [
            'id'=>25,
            'product_id'=>11,
            'quantity'=>2000,
            'size_id'=>11,
            'price'=>10000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-07-04 17:52:35'
            ] );

            ProductQtyPrices::create( [
            'id'=>26,
            'product_id'=>12,
            'quantity'=>100,
            'size_id'=>12,
            'price'=>1400.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:43:51'
            ] );

            ProductQtyPrices::create( [
            'id'=>27,
            'product_id'=>12,
            'quantity'=>200,
            'size_id'=>12,
            'price'=>2600.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:43:53'
            ] );

            ProductQtyPrices::create( [
            'id'=>28,
            'product_id'=>12,
            'quantity'=>300,
            'size_id'=>12,
            'price'=>3750.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:43:56'
            ] );

            ProductQtyPrices::create( [
            'id'=>29,
            'product_id'=>12,
            'quantity'=>400,
            'size_id'=>12,
            'price'=>5000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:43:58'
            ] );

            ProductQtyPrices::create( [
            'id'=>30,
            'product_id'=>12,
            'quantity'=>500,
            'size_id'=>12,
            'price'=>6000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:44:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>31,
            'product_id'=>12,
            'quantity'=>1000,
            'size_id'=>12,
            'price'=>10000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:44:03'
            ] );

            ProductQtyPrices::create( [
            'id'=>32,
            'product_id'=>12,
            'quantity'=>2000,
            'size_id'=>12,
            'price'=>18000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:44:05'
            ] );

            ProductQtyPrices::create( [
            'id'=>33,
            'product_id'=>21,
            'quantity'=>1,
            'size_id'=>13,
            'price'=>220.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:19'
            ] );

            ProductQtyPrices::create( [
            'id'=>34,
            'product_id'=>21,
            'quantity'=>2,
            'size_id'=>13,
            'price'=>440.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:21'
            ] );

            ProductQtyPrices::create( [
            'id'=>35,
            'product_id'=>21,
            'quantity'=>3,
            'size_id'=>13,
            'price'=>660.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:23'
            ] );

            ProductQtyPrices::create( [
            'id'=>36,
            'product_id'=>21,
            'quantity'=>4,
            'size_id'=>13,
            'price'=>880.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:25'
            ] );

            ProductQtyPrices::create( [
            'id'=>37,
            'product_id'=>21,
            'quantity'=>5,
            'size_id'=>13,
            'price'=>1000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:26'
            ] );

            ProductQtyPrices::create( [
            'id'=>38,
            'product_id'=>21,
            'quantity'=>10,
            'size_id'=>13,
            'price'=>1850.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:28'
            ] );

            ProductQtyPrices::create( [
            'id'=>39,
            'product_id'=>21,
            'quantity'=>20,
            'size_id'=>13,
            'price'=>3500.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:30'
            ] );

            ProductQtyPrices::create( [
            'id'=>40,
            'product_id'=>21,
            'quantity'=>50,
            'size_id'=>13,
            'price'=>7500.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:32'
            ] );

            ProductQtyPrices::create( [
            'id'=>41,
            'product_id'=>21,
            'quantity'=>100,
            'size_id'=>13,
            'price'=>13000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:36'
            ] );

            ProductQtyPrices::create( [
            'id'=>42,
            'product_id'=>21,
            'quantity'=>200,
            'size_id'=>13,
            'price'=>24000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:38'
            ] );

            ProductQtyPrices::create( [
            'id'=>43,
            'product_id'=>21,
            'quantity'=>500,
            'size_id'=>13,
            'price'=>45000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:40'
            ] );

            ProductQtyPrices::create( [
            'id'=>44,
            'product_id'=>22,
            'quantity'=>1,
            'size_id'=>14,
            'price'=>320.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:50'
            ] );

            ProductQtyPrices::create( [
            'id'=>45,
            'product_id'=>22,
            'quantity'=>2,
            'size_id'=>14,
            'price'=>640.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:52'
            ] );

            ProductQtyPrices::create( [
            'id'=>46,
            'product_id'=>22,
            'quantity'=>3,
            'size_id'=>14,
            'price'=>960.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:55'
            ] );

            ProductQtyPrices::create( [
            'id'=>47,
            'product_id'=>22,
            'quantity'=>4,
            'size_id'=>14,
            'price'=>1280.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:56'
            ] );

            ProductQtyPrices::create( [
            'id'=>48,
            'product_id'=>22,
            'quantity'=>5,
            'size_id'=>14,
            'price'=>1500.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:45:58'
            ] );

            ProductQtyPrices::create( [
            'id'=>49,
            'product_id'=>22,
            'quantity'=>10,
            'size_id'=>14,
            'price'=>2850.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:46:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>50,
            'product_id'=>22,
            'quantity'=>20,
            'size_id'=>14,
            'price'=>5500.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:46:02'
            ] );

            ProductQtyPrices::create( [
            'id'=>51,
            'product_id'=>22,
            'quantity'=>50,
            'size_id'=>14,
            'price'=>12500.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:46:03'
            ] );

            ProductQtyPrices::create( [
            'id'=>52,
            'product_id'=>22,
            'quantity'=>100,
            'size_id'=>14,
            'price'=>23000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:46:06'
            ] );

            ProductQtyPrices::create( [
            'id'=>53,
            'product_id'=>22,
            'quantity'=>200,
            'size_id'=>14,
            'price'=>44000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:46:08'
            ] );

            ProductQtyPrices::create( [
            'id'=>54,
            'product_id'=>22,
            'quantity'=>500,
            'size_id'=>14,
            'price'=>95000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:46:10'
            ] );

            ProductQtyPrices::create( [
            'id'=>55,
            'product_id'=>13,
            'quantity'=>100,
            'size_id'=>15,
            'price'=>1500.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:46:59'
            ] );

            ProductQtyPrices::create( [
            'id'=>56,
            'product_id'=>13,
            'quantity'=>200,
            'size_id'=>15,
            'price'=>3000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:47:02'
            ] );

            ProductQtyPrices::create( [
            'id'=>57,
            'product_id'=>13,
            'quantity'=>300,
            'size_id'=>15,
            'price'=>4200.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:47:04'
            ] );

            ProductQtyPrices::create( [
            'id'=>58,
            'product_id'=>13,
            'quantity'=>400,
            'size_id'=>15,
            'price'=>5400.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:47:06'
            ] );

            ProductQtyPrices::create( [
            'id'=>59,
            'product_id'=>13,
            'quantity'=>500,
            'size_id'=>15,
            'price'=>6000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:47:15'
            ] );

            ProductQtyPrices::create( [
            'id'=>60,
            'product_id'=>13,
            'quantity'=>1000,
            'size_id'=>15,
            'price'=>11000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:47:17'
            ] );

            ProductQtyPrices::create( [
            'id'=>61,
            'product_id'=>13,
            'quantity'=>2000,
            'size_id'=>15,
            'price'=>20000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:47:20'
            ] );

            ProductQtyPrices::create( [
            'id'=>62,
            'product_id'=>13,
            'quantity'=>100,
            'size_id'=>16,
            'price'=>1700.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:15'
            ] );

            ProductQtyPrices::create( [
            'id'=>63,
            'product_id'=>13,
            'quantity'=>200,
            'size_id'=>16,
            'price'=>3400.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:17'
            ] );

            ProductQtyPrices::create( [
            'id'=>64,
            'product_id'=>13,
            'quantity'=>300,
            'size_id'=>16,
            'price'=>4800.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:19'
            ] );

            ProductQtyPrices::create( [
            'id'=>65,
            'product_id'=>13,
            'quantity'=>400,
            'size_id'=>16,
            'price'=>6200.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:21'
            ] );

            ProductQtyPrices::create( [
            'id'=>66,
            'product_id'=>13,
            'quantity'=>500,
            'size_id'=>16,
            'price'=>7000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:24'
            ] );

            ProductQtyPrices::create( [
            'id'=>67,
            'product_id'=>13,
            'quantity'=>1000,
            'size_id'=>16,
            'price'=>13000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:27'
            ] );

            ProductQtyPrices::create( [
            'id'=>68,
            'product_id'=>13,
            'quantity'=>2000,
            'size_id'=>16,
            'price'=>24000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:30'
            ] );

            ProductQtyPrices::create( [
            'id'=>69,
            'product_id'=>13,
            'quantity'=>100,
            'size_id'=>17,
            'price'=>1900.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:34'
            ] );

            ProductQtyPrices::create( [
            'id'=>70,
            'product_id'=>13,
            'quantity'=>200,
            'size_id'=>17,
            'price'=>3800.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:37'
            ] );

            ProductQtyPrices::create( [
            'id'=>71,
            'product_id'=>13,
            'quantity'=>300,
            'size_id'=>17,
            'price'=>5400.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:39'
            ] );

            ProductQtyPrices::create( [
            'id'=>72,
            'product_id'=>13,
            'quantity'=>400,
            'size_id'=>17,
            'price'=>7000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:41'
            ] );

            ProductQtyPrices::create( [
            'id'=>73,
            'product_id'=>13,
            'quantity'=>500,
            'size_id'=>17,
            'price'=>8000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:44'
            ] );

            ProductQtyPrices::create( [
            'id'=>74,
            'product_id'=>13,
            'quantity'=>1000,
            'size_id'=>17,
            'price'=>15000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:46'
            ] );

            ProductQtyPrices::create( [
            'id'=>75,
            'product_id'=>13,
            'quantity'=>2000,
            'size_id'=>17,
            'price'=>28000.00,
            'created_at'=>'2021-06-10 07:35:59',
            'updated_at'=>'2021-06-10 10:48:48'
            ] );

            ProductQtyPrices::create( [
            'id'=>77,
            'product_id'=>17,
            'quantity'=>100,
            'size_id'=>20,
            'price'=>1500.00,
            'created_at'=>'2021-06-20 12:51:08',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>78,
            'product_id'=>17,
            'quantity'=>200,
            'size_id'=>19,
            'price'=>2400.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>79,
            'product_id'=>17,
            'quantity'=>200,
            'size_id'=>20,
            'price'=>3000.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>80,
            'product_id'=>17,
            'quantity'=>300,
            'size_id'=>19,
            'price'=>3600.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>81,
            'product_id'=>17,
            'quantity'=>300,
            'size_id'=>20,
            'price'=>4500.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>82,
            'product_id'=>17,
            'quantity'=>400,
            'size_id'=>19,
            'price'=>4800.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>83,
            'product_id'=>17,
            'quantity'=>400,
            'size_id'=>20,
            'price'=>6000.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>84,
            'product_id'=>17,
            'quantity'=>500,
            'size_id'=>19,
            'price'=>5500.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>85,
            'product_id'=>17,
            'quantity'=>500,
            'size_id'=>20,
            'price'=>7000.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>86,
            'product_id'=>17,
            'quantity'=>1000,
            'size_id'=>19,
            'price'=>10000.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>87,
            'product_id'=>17,
            'quantity'=>1000,
            'size_id'=>20,
            'price'=>13000.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>88,
            'product_id'=>17,
            'quantity'=>1500,
            'size_id'=>19,
            'price'=>13500.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>89,
            'product_id'=>17,
            'quantity'=>1500,
            'size_id'=>20,
            'price'=>18000.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>90,
            'product_id'=>17,
            'quantity'=>2000,
            'size_id'=>19,
            'price'=>16000.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>91,
            'product_id'=>17,
            'quantity'=>2000,
            'size_id'=>20,
            'price'=>22000.00,
            'created_at'=>'2021-06-20 12:54:43',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>92,
            'product_id'=>18,
            'quantity'=>100,
            'size_id'=>21,
            'price'=>1200.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>93,
            'product_id'=>18,
            'quantity'=>100,
            'size_id'=>22,
            'price'=>1500.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>94,
            'product_id'=>18,
            'quantity'=>200,
            'size_id'=>21,
            'price'=>2400.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>95,
            'product_id'=>18,
            'quantity'=>200,
            'size_id'=>22,
            'price'=>3000.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>96,
            'product_id'=>18,
            'quantity'=>300,
            'size_id'=>21,
            'price'=>3600.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>97,
            'product_id'=>18,
            'quantity'=>300,
            'size_id'=>22,
            'price'=>4500.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>98,
            'product_id'=>18,
            'quantity'=>400,
            'size_id'=>21,
            'price'=>4800.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>99,
            'product_id'=>18,
            'quantity'=>400,
            'size_id'=>22,
            'price'=>6000.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>100,
            'product_id'=>18,
            'quantity'=>500,
            'size_id'=>21,
            'price'=>5500.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>101,
            'product_id'=>18,
            'quantity'=>500,
            'size_id'=>22,
            'price'=>7000.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>102,
            'product_id'=>18,
            'quantity'=>1000,
            'size_id'=>21,
            'price'=>10000.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>103,
            'product_id'=>18,
            'quantity'=>1000,
            'size_id'=>22,
            'price'=>13000.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>104,
            'product_id'=>18,
            'quantity'=>1500,
            'size_id'=>21,
            'price'=>13500.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>105,
            'product_id'=>18,
            'quantity'=>1500,
            'size_id'=>22,
            'price'=>18000.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>106,
            'product_id'=>18,
            'quantity'=>2000,
            'size_id'=>21,
            'price'=>16000.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>107,
            'product_id'=>18,
            'quantity'=>2000,
            'size_id'=>22,
            'price'=>22000.00,
            'created_at'=>'2021-06-20 12:57:07',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>108,
            'product_id'=>19,
            'quantity'=>100,
            'size_id'=>23,
            'price'=>1200.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>109,
            'product_id'=>19,
            'quantity'=>100,
            'size_id'=>24,
            'price'=>1500.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>110,
            'product_id'=>19,
            'quantity'=>200,
            'size_id'=>23,
            'price'=>2400.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>111,
            'product_id'=>19,
            'quantity'=>200,
            'size_id'=>24,
            'price'=>3000.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>112,
            'product_id'=>19,
            'quantity'=>300,
            'size_id'=>23,
            'price'=>3600.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>113,
            'product_id'=>19,
            'quantity'=>300,
            'size_id'=>24,
            'price'=>4500.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>114,
            'product_id'=>19,
            'quantity'=>400,
            'size_id'=>23,
            'price'=>4800.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>115,
            'product_id'=>19,
            'quantity'=>400,
            'size_id'=>24,
            'price'=>6000.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>116,
            'product_id'=>19,
            'quantity'=>500,
            'size_id'=>23,
            'price'=>5500.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>117,
            'product_id'=>19,
            'quantity'=>500,
            'size_id'=>24,
            'price'=>7000.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>118,
            'product_id'=>19,
            'quantity'=>1000,
            'size_id'=>23,
            'price'=>10000.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>119,
            'product_id'=>19,
            'quantity'=>1000,
            'size_id'=>24,
            'price'=>13000.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>120,
            'product_id'=>19,
            'quantity'=>1500,
            'size_id'=>23,
            'price'=>13500.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>121,
            'product_id'=>19,
            'quantity'=>1500,
            'size_id'=>24,
            'price'=>18000.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>122,
            'product_id'=>19,
            'quantity'=>2000,
            'size_id'=>23,
            'price'=>16000.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>123,
            'product_id'=>19,
            'quantity'=>2000,
            'size_id'=>24,
            'price'=>22000.00,
            'created_at'=>'2021-06-20 12:58:33',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>124,
            'product_id'=>20,
            'quantity'=>100,
            'size_id'=>25,
            'price'=>1200.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>125,
            'product_id'=>20,
            'quantity'=>100,
            'size_id'=>26,
            'price'=>1500.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>126,
            'product_id'=>20,
            'quantity'=>200,
            'size_id'=>25,
            'price'=>2400.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>127,
            'product_id'=>20,
            'quantity'=>200,
            'size_id'=>26,
            'price'=>3000.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>128,
            'product_id'=>20,
            'quantity'=>300,
            'size_id'=>25,
            'price'=>3600.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>129,
            'product_id'=>20,
            'quantity'=>300,
            'size_id'=>26,
            'price'=>4500.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>130,
            'product_id'=>20,
            'quantity'=>400,
            'size_id'=>25,
            'price'=>4800.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>131,
            'product_id'=>20,
            'quantity'=>400,
            'size_id'=>26,
            'price'=>6000.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>132,
            'product_id'=>20,
            'quantity'=>500,
            'size_id'=>25,
            'price'=>5500.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>133,
            'product_id'=>20,
            'quantity'=>500,
            'size_id'=>26,
            'price'=>7000.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>134,
            'product_id'=>20,
            'quantity'=>1000,
            'size_id'=>25,
            'price'=>10000.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>135,
            'product_id'=>20,
            'quantity'=>1000,
            'size_id'=>26,
            'price'=>13000.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>136,
            'product_id'=>20,
            'quantity'=>1500,
            'size_id'=>25,
            'price'=>13500.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>137,
            'product_id'=>20,
            'quantity'=>1500,
            'size_id'=>26,
            'price'=>18000.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>138,
            'product_id'=>20,
            'quantity'=>2000,
            'size_id'=>25,
            'price'=>16000.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>139,
            'product_id'=>20,
            'quantity'=>2000,
            'size_id'=>26,
            'price'=>22000.00,
            'created_at'=>'2021-06-20 13:01:15',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>140,
            'product_id'=>1,
            'quantity'=>200,
            'size_id'=>1,
            'price'=>19.00,
            'created_at'=>'2021-07-04 17:02:45',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>141,
            'product_id'=>1,
            'quantity'=>500,
            'size_id'=>1,
            'price'=>15.00,
            'created_at'=>'2021-07-04 17:08:12',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>142,
            'product_id'=>1,
            'quantity'=>1000,
            'size_id'=>1,
            'price'=>14.00,
            'created_at'=>'2021-07-04 17:08:12',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>143,
            'product_id'=>1,
            'quantity'=>2000,
            'size_id'=>1,
            'price'=>12.00,
            'created_at'=>'2021-07-04 17:08:12',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>144,
            'product_id'=>2,
            'quantity'=>100,
            'size_id'=>2,
            'price'=>25.00,
            'created_at'=>'2021-07-04 17:08:12',
            'updated_at'=>'2021-09-13 02:09:34'
            ] );

            ProductQtyPrices::create( [
            'id'=>145,
            'product_id'=>2,
            'quantity'=>200,
            'size_id'=>2,
            'price'=>23.00,
            'created_at'=>'2021-07-04 17:08:12',
            'updated_at'=>'2021-09-13 02:09:37'
            ] );

            ProductQtyPrices::create( [
            'id'=>146,
            'product_id'=>2,
            'quantity'=>500,
            'size_id'=>2,
            'price'=>18.00,
            'created_at'=>'2021-07-04 17:08:12',
            'updated_at'=>'2021-09-13 02:09:39'
            ] );

            ProductQtyPrices::create( [
            'id'=>147,
            'product_id'=>2,
            'quantity'=>1000,
            'size_id'=>2,
            'price'=>16.00,
            'created_at'=>'2021-07-04 17:08:12',
            'updated_at'=>'2021-09-13 02:09:41'
            ] );

            ProductQtyPrices::create( [
            'id'=>148,
            'product_id'=>2,
            'quantity'=>2000,
            'size_id'=>2,
            'price'=>15.00,
            'created_at'=>'2021-07-04 17:08:12',
            'updated_at'=>'2021-09-13 02:09:43'
            ] );

            ProductQtyPrices::create( [
            'id'=>149,
            'product_id'=>32,
            'quantity'=>100,
            'size_id'=>27,
            'price'=>1200.00,
            'created_at'=>'2021-07-10 16:09:28',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>150,
            'product_id'=>32,
            'quantity'=>200,
            'size_id'=>27,
            'price'=>2400.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>151,
            'product_id'=>32,
            'quantity'=>300,
            'size_id'=>27,
            'price'=>3600.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>152,
            'product_id'=>32,
            'quantity'=>400,
            'size_id'=>27,
            'price'=>4800.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>153,
            'product_id'=>32,
            'quantity'=>500,
            'size_id'=>27,
            'price'=>5000.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>154,
            'product_id'=>32,
            'quantity'=>1000,
            'size_id'=>27,
            'price'=>9000.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>155,
            'product_id'=>32,
            'quantity'=>2000,
            'size_id'=>27,
            'price'=>16000.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'2021-07-10 16:17:23'
            ] );

            ProductQtyPrices::create( [
            'id'=>156,
            'product_id'=>33,
            'quantity'=>100,
            'size_id'=>28,
            'price'=>1200.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>157,
            'product_id'=>33,
            'quantity'=>200,
            'size_id'=>28,
            'price'=>2400.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>158,
            'product_id'=>33,
            'quantity'=>300,
            'size_id'=>28,
            'price'=>3600.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>159,
            'product_id'=>33,
            'quantity'=>400,
            'size_id'=>28,
            'price'=>4800.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>160,
            'product_id'=>33,
            'quantity'=>500,
            'size_id'=>28,
            'price'=>5000.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>161,
            'product_id'=>33,
            'quantity'=>1000,
            'size_id'=>28,
            'price'=>9000.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>162,
            'product_id'=>33,
            'quantity'=>2000,
            'size_id'=>28,
            'price'=>16000.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'2021-07-10 16:17:38'
            ] );

            ProductQtyPrices::create( [
            'id'=>163,
            'product_id'=>34,
            'quantity'=>100,
            'size_id'=>29,
            'price'=>1200.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>164,
            'product_id'=>34,
            'quantity'=>200,
            'size_id'=>29,
            'price'=>2400.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>165,
            'product_id'=>34,
            'quantity'=>300,
            'size_id'=>29,
            'price'=>3600.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>166,
            'product_id'=>34,
            'quantity'=>400,
            'size_id'=>29,
            'price'=>4800.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>167,
            'product_id'=>34,
            'quantity'=>500,
            'size_id'=>29,
            'price'=>5000.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>168,
            'product_id'=>34,
            'quantity'=>1000,
            'size_id'=>29,
            'price'=>9000.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>169,
            'product_id'=>34,
            'quantity'=>2000,
            'size_id'=>29,
            'price'=>16000.00,
            'created_at'=>'2021-07-10 16:14:09',
            'updated_at'=>'2021-07-10 16:17:34'
            ] );

            ProductQtyPrices::create( [
            'id'=>170,
            'product_id'=>26,
            'quantity'=>1,
            'size_id'=>30,
            'price'=>350.00,
            'created_at'=>'2021-08-15 14:26:17',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>171,
            'product_id'=>26,
            'quantity'=>2,
            'size_id'=>30,
            'price'=>600.00,
            'created_at'=>'2021-08-15 14:26:17',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>172,
            'product_id'=>26,
            'quantity'=>5,
            'size_id'=>30,
            'price'=>1500.00,
            'created_at'=>'2021-08-15 14:26:58',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>173,
            'product_id'=>26,
            'quantity'=>10,
            'size_id'=>30,
            'price'=>2800.00,
            'created_at'=>'2021-08-15 14:26:58',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>174,
            'product_id'=>26,
            'quantity'=>15,
            'size_id'=>30,
            'price'=>4050.00,
            'created_at'=>'2021-08-15 14:26:58',
            'updated_at'=>'2021-08-15 14:28:19'
            ] );

            ProductQtyPrices::create( [
            'id'=>175,
            'product_id'=>26,
            'quantity'=>20,
            'size_id'=>30,
            'price'=>5400.00,
            'created_at'=>'2021-08-15 14:26:58',
            'updated_at'=>'2021-08-15 14:28:10'
            ] );

            ProductQtyPrices::create( [
            'id'=>176,
            'product_id'=>26,
            'quantity'=>50,
            'size_id'=>30,
            'price'=>12500.00,
            'created_at'=>'2021-08-15 14:26:58',
            'updated_at'=>'2021-08-15 14:28:03'
            ] );

            ProductQtyPrices::create( [
            'id'=>177,
            'product_id'=>26,
            'quantity'=>100,
            'size_id'=>30,
            'price'=>23000.00,
            'created_at'=>'2021-08-15 14:26:58',
            'updated_at'=>'2021-08-15 14:27:56'
            ] );

            ProductQtyPrices::create( [
            'id'=>178,
            'product_id'=>26,
            'quantity'=>200,
            'size_id'=>30,
            'price'=>40000.00,
            'created_at'=>'2021-08-15 14:28:58',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>179,
            'product_id'=>26,
            'quantity'=>500,
            'size_id'=>30,
            'price'=>80000.00,
            'created_at'=>'2021-08-15 14:28:58',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>180,
            'product_id'=>27,
            'quantity'=>1,
            'size_id'=>31,
            'price'=>300.00,
            'created_at'=>'2021-08-15 14:32:47',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>181,
            'product_id'=>27,
            'quantity'=>2,
            'size_id'=>31,
            'price'=>550.00,
            'created_at'=>'2021-08-15 14:32:47',
            'updated_at'=>'0000-00-00 00:00:00'
            ] );

            ProductQtyPrices::create( [
            'id'=>182,
            'product_id'=>27,
            'quantity'=>5,
            'size_id'=>31,
            'price'=>1375.00,
            'created_at'=>'2021-08-15 14:33:08',
            'updated_at'=>'2021-08-15 14:34:53'
            ] );

            ProductQtyPrices::create( [
            'id'=>183,
            'product_id'=>27,
            'quantity'=>10,
            'size_id'=>31,
            'price'=>2500.00,
            'created_at'=>'2021-08-15 14:33:08',
            'updated_at'=>'2021-08-15 14:34:42'
            ] );

            ProductQtyPrices::create( [
            'id'=>184,
            'product_id'=>27,
            'quantity'=>15,
            'size_id'=>31,
            'price'=>3000.00,
            'created_at'=>'2021-08-15 14:33:08',
            'updated_at'=>'2021-08-15 14:34:35'
            ] );

            ProductQtyPrices::create( [
            'id'=>185,
            'product_id'=>27,
            'quantity'=>20,
            'size_id'=>31,
            'price'=>4000.00,
            'created_at'=>'2021-08-15 14:33:08',
            'updated_at'=>'2021-08-15 14:34:30'
            ] );

            ProductQtyPrices::create( [
            'id'=>186,
            'product_id'=>27,
            'quantity'=>50,
            'size_id'=>31,
            'price'=>8750.00,
            'created_at'=>'2021-08-15 14:33:08',
            'updated_at'=>'2021-08-15 14:34:23'
            ] );

            ProductQtyPrices::create( [
            'id'=>187,
            'product_id'=>27,
            'quantity'=>100,
            'size_id'=>31,
            'price'=>16000.00,
            'created_at'=>'2021-08-15 14:33:08',
            'updated_at'=>'2021-08-15 14:34:16'
            ] );

            ProductQtyPrices::create( [
            'id'=>188,
            'product_id'=>27,
            'quantity'=>200,
            'size_id'=>31,
            'price'=>30000.00,
            'created_at'=>'2021-08-15 14:33:08',
            'updated_at'=>'2021-08-15 14:34:09'
            ] );

            ProductQtyPrices::create( [
            'id'=>189,
            'product_id'=>27,
            'quantity'=>500,
            'size_id'=>31,
            'price'=>60000.00,
            'created_at'=>'2021-08-15 14:33:08',
            'updated_at'=>'2021-08-15 14:34:03'
            ] );
        // DB::table('product_qty_size_prices')->insert([
        //     //  Student Id Cards Single
        //     [
        //         'id' => NULL,
        //         'product_id' => 1,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => 22
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 1,
        //         'quantity' => '50',
        //         'size_id' => '1',
        //         'price' => 20
        //     ],
        //     //  Student Id Cards Double
        //     [
        //         'id' => NULL,
        //         'product_id' => 2,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => 28
        //     ],
        //     // Employee Single
        //     [
        //         'id' => NULL,
        //         'product_id' => 3,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => 22
        //     ],
        //     // Employee Double
        //     [
        //         'id' => NULL,
        //         'product_id' => 4,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => 28
        //     ],
        //     //  visiting Single
        //     [
        //         'id' => NULL,
        //         'product_id' => 5,
        //         'quantity' => '100',
        //         'size_id' => '1',
        //         'price' => 300
        //     ],
        //     //  visiting Single
        //     [
        //         'id' => NULL,
        //         'product_id' => 5,
        //         'quantity' => 200,
        //         'size_id' => '1',
        //         'price' => 500
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 5,
        //         'quantity' => 500,
        //         'size_id' => '1',
        //         'price' => 1000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 5,
        //         'quantity' => 1000,
        //         'size_id' => '1',
        //         'price' => 1900
        //     ],
        //     //  visiting Double
        //     [
        //         'id' => NULL,
        //         'product_id' => 39,
        //         'quantity' => '100',
        //         'size_id' => '1',
        //         'price' => 400
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 39,
        //         'quantity' => 200,
        //         'size_id' => '1',
        //         'price' => 600
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 39,
        //         'quantity' => 500,
        //         'size_id' => '1',
        //         'price' => 1250
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 39,
        //         'quantity' => 1000,
        //         'size_id' => '1',
        //         'price' => 2400
        //     ],
        //     // Memebershio ID Single
        //     [
        //         'id' => NULL,
        //         'product_id' => 6,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => 28
        //     ],
        //     // Memebershio ID Double
        //     [
        //         'id' => NULL,
        //         'product_id' => 7,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => '22'
        //     ],
        //     //  waranty Card Single
        //     [
        //         'id' => NULL,
        //         'product_id' => 8,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => 28
        //     ],
        //     // Loyalty Card Single
        //     [
        //         'id' => NULL,
        //         'product_id' => 9,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => '22'
        //     ],
        //     // Previlege Card Single
        //     [
        //         'id' => NULL,
        //         'product_id' => 10,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => 28
        //     ],
        //     // Flat Lanyard
        //     [
        //         'id' => NULL,
        //         'product_id' => 11,
        //         'quantity' => '100',
        //         'size_id' => '2',
        //         'price' => 1100
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 11,
        //         'quantity' => '200',
        //         'size_id' => '2',
        //         'price' => 2200
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 11,
        //         'quantity' => '300',
        //         'size_id' => '2',
        //         'price' => 3150
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 11,
        //         'quantity' => '400',
        //         'size_id' => '2',
        //         'price' => 4200
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 11,
        //         'quantity' => '500',
        //         'size_id' => '2',
        //         'price' => 4500
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 11,
        //         'quantity' => '1000',
        //         'size_id' => '2',
        //         'price' => 8000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 11,
        //         'quantity' => '2000',
        //         'size_id' => '2',
        //         'price' => 14000
        //     ],
        //     // Tube Lanyard
        //     [
        //         'id' => NULL,
        //         'product_id' => 12,
        //         'quantity' => '100',
        //         'size_id' => '2',
        //         'price' => 1400
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 12,
        //         'quantity' => '200',
        //         'size_id' => '2',
        //         'price' => 2600
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 12,
        //         'quantity' => '300',
        //         'size_id' => '2',
        //         'price' => 3750
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 12,
        //         'quantity' => '400',
        //         'size_id' => '2',
        //         'price' => 5000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 12,
        //         'quantity' => '500',
        //         'size_id' => '2',
        //         'price' => 6000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 12,
        //         'quantity' => '1000',
        //         'size_id' => '2',
        //         'price' => 10000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 12,
        //         'quantity' => '2000',
        //         'size_id' => '2',
        //         'price' => 18000
        //     ],
        //     // whitemug
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '1',
        //         'size_id' => '1',
        //         'price' => 220
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '2',
        //         'size_id' => '1',
        //         'price' => 440
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '3',
        //         'size_id' => '1',
        //         'price' => 660
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '4',
        //         'size_id' => '1',
        //         'price' => 880
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '5',
        //         'size_id' => '1',
        //         'price' => 1000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => 1850
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '20',
        //         'size_id' => '1',
        //         'price' => 3500
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '50',
        //         'size_id' => '1',
        //         'price' => 7500
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '100',
        //         'size_id' => '1',
        //         'price' => 13000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '200',
        //         'size_id' => '1',
        //         'price' => 24000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 21,
        //         'quantity' => '500',
        //         'size_id' => '1',
        //         'price' => 45000
        //     ],
        //     // inner color Mug
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '1',
        //         'size_id' => '1',
        //         'price' => 320
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '2',
        //         'size_id' => '1',
        //         'price' => 640
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '3',
        //         'size_id' => '1',
        //         'price' => 960
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '4',
        //         'size_id' => '1',
        //         'price' => 1280
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '5',
        //         'size_id' => '1',
        //         'price' => 1500
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '10',
        //         'size_id' => '1',
        //         'price' => 2850
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '20',
        //         'size_id' => '1',
        //         'price' => 5500
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '50',
        //         'size_id' => '1',
        //         'price' => 12500
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '100',
        //         'size_id' => '1',
        //         'price' => 23000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '200',
        //         'size_id' => '1',
        //         'price' => 44000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 22,
        //         'quantity' => '500',
        //         'size_id' => '1',
        //         'price' => 95000
        //     ],
        //     //  Sattain Lanyard 12 MM
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '100',
        //         'size_id' => '2',
        //         'price' => 1500
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '200',
        //         'size_id' => '2',
        //         'price' => 3000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '300',
        //         'size_id' => '2',
        //         'price' => 4200
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '400',
        //         'size_id' => '2',
        //         'price' => 5400
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '500',
        //         'size_id' => '2',
        //         'price' => 6000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '1000',
        //         'size_id' => '2',
        //         'price' => 11000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '2000',
        //         'size_id' => '2',
        //         'price' => 20000
        //     ],
        //     //  Sattain Lanyard 16 MM
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '100',
        //         'size_id' => '3',
        //         'price' => 1700
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '200',
        //         'size_id' => '3',
        //         'price' => 3400
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '300',
        //         'size_id' => '3',
        //         'price' => 4800
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '400',
        //         'size_id' => '3',
        //         'price' => 6200
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '500',
        //         'size_id' => '3',
        //         'price' => 7000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '1000',
        //         'size_id' => '3',
        //         'price' => 13000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '2000',
        //         'size_id' => '3',
        //         'price' => 24000
        //     ],
        //     //  Sattain Lanyard 20 MM
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '100',
        //         'size_id' => '4',
        //         'price' => 1900
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '200',
        //         'size_id' => '4',
        //         'price' => 3800
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '300',
        //         'size_id' => '4',
        //         'price' => 5400
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '400',
        //         'size_id' => '4',
        //         'price' => 7000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '500',
        //         'size_id' => '4',
        //         'price' => 8000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '1000',
        //         'size_id' => '4',
        //         'price' => 15000
        //     ],
        //     [
        //         'id' => NULL,
        //         'product_id' => 13,
        //         'quantity' => '2000',
        //         'size_id' => '4',
        //         'price' => 28000
        //     ],
        // ]);
    }
}
