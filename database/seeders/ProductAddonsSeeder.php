<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductAddonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('product_addons')->insert([
            [
                'id' => NULL,
                'product_id' => '1',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '1',
                'addon_name' => 'lanyard'
            ], [
                'id' => NULL,
                'product_id' => '1',
                'addon_name' => 'barcode'
            ],[
                'id' => NULL,
                'product_id' => '2',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '2',
                'addon_name' => 'lanyard'
            ], [
                'id' => NULL,
                'product_id' => '2',
                'addon_name' => 'barcode'
            ], [
                'id' => NULL,
                'product_id' => '3',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_name' => '3',
                'addon_name' => 'lanyard'
            ], [
                'id' => NULL,
                'product_id' => '3',
                'addon_name' => 'barcode'
            ], [
                'id' => NULL,
                'product_id' => '4',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '4',
                'addon_name' => 'lanyard'
            ],  [
                'id' => NULL,
                'product_id' => '4',
                'addon_name' => 'barcode'
            ], [
                'id' => NULL,
                'product_id' => '11',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '11',
                'addon_name' => 'hooks'
            ], [
                'id' => NULL,
                'product_id' => '12',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '12',
                'addon_name' => 'hooks'
            ], [
                'id' => NULL,
                'product_id' => '13',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '13',
                'addon_name' => 'hooks'
            ], [
                'id' => NULL,
                'product_id' => '14',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '14',
                'addon_name' => 'hooks'
            ], [
                'id' => NULL,
                'product_id' => '15',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '15',
                'addon_name' => 'hooks'
            ], [
                'id' => NULL,
                'product_id' => '16',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '16',
                'addon_name' => 'hooks'
            ], [
                'id' => NULL,
                'product_id' => '32',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '33',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '34',
                'addon_name' => 'printingType'
            ], [
                'id' => NULL,
                'product_id' => '38',
                'addon_name' => 'printingType'
            ],

        ]);
    }
}
