<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([[
            'category_id' => '1',
            'product_desc' => 'STUDENT ID CARD (SINGLE SIDED)',
            'product_name' => 'STUDENT ID CARD (SINGLE SIDED)',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('STUDENT ID CARD (SINGLE SIDED)'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '1',
            'product_desc' => 'STUDENT ID CARD (DOUBLE SIDED)',
            'product_name' => 'STUDENT ID CARD (DOUBLE SIDED)',
            'product_keyword' => 'STUDENT ID CARD (DOUBLE SIDED)',
            'status' => 'ACTIVE',
            'slug' => Str::slug('STUDENT ID CARD (DOUBLE SIDED)'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '1',
            'product_desc' => 'EMPLOYEE ID CARD (SINGLE SIDED)',
            'product_name' => 'EMPLOYEE ID CARD (SINGLE SIDED)',
            'product_keyword' => 'EMPLOYEE ID CARD (SINGLE SIDED)',
            'status' => 'ACTIVE',
            'slug' => Str::slug('EMPLOYEE ID CARD (SINGLE SIDED)'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '1',
            'product_desc' => 'EMPLOYEE ID CARD (DOUBLE SIDED)',
            'product_name' => 'EMPLOYEE ID CARD (DOUBLE SIDED)',
            'product_keyword' => 'EMPLOYEE ID CARD (DOUBLE SIDED)',
            'status' => 'ACTIVE',
            'slug' => Str::slug('EMPLOYEE ID CARD (DOUBLE SIDED)'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '1',
            'product_desc' => 'VISITING CARD',
            'product_name' => 'VISITING CARD',
            'product_keyword' => 'VISITING CARD',
            'status' => 'ACTIVE',
            'slug' => Str::slug('VISITING CARD'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '1',
            'product_desc' => 'MEMBERSHIP ID CARD (SINGLE SIDED)',
            'product_name' => 'MEMBERSHIP ID CARD (SINGLE SIDED)',
            'product_keyword' => 'MEMBERSHIP ID CARD (SINGLE SIDED)',
            'status' => 'ACTIVE',
            'slug' => Str::slug('MEMBERSHIP ID CARD (SINGLE SIDED)'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ],[
            'category_id' => '1',
            'product_desc' => 'MEMBERSHIP ID CARD (DOUBLE SIDED)',
            'product_name' => 'MEMBERSHIP ID CARD (DOUBLE SIDED)',
            'product_keyword' => 'MEMBERSHIP ID CARD (DOUBLE SIDED)',
            'status' => 'ACTIVE',
            'slug' => Str::slug('MEMBERSHIP ID CARD (DOUBLE SIDED)'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '1',
            'product_desc' => 'WARRANTY CARD',
            'product_name' => 'WARRANTY CARD',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('WARRANTY CARD'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '1',
            'product_desc' => 'LOYALITY CARD',
            'product_name' => 'LOYALITY CARD',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('LOYALITY CARD'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '1',
            'product_desc' => 'PRIVILEGE CARD',
            'product_name' => 'PRIVILEGE CARD',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('PRIVILEGE CARD'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '2',
            'product_desc' => 'FLAT LANYARD',
            'product_name' => 'FLAT LANYARD',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('FLAT LANYARD'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '2',
            'product_desc' => 'TUBE LANYARD',
            'product_name' => 'TUBE LANYARD',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('TUBE LANYARD'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '2',
            'product_desc' => 'SATTAIN LANYARD',
            'product_name' => 'SATTAIN LANYARD',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('SATTAIN LANYARD'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '2',
            'product_desc' => 'MULTI COLOR LANYARD',
            'product_name' => 'MULTI COLOR LANYARD',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('MULTI COLOR LANYARD'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '2',
            'product_desc' => '25 MM DIGITAL',
            'product_name' => '25 MM DIGITAL',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('25 MM DIGITAL'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '2',
            'product_desc' => 'SASH',
            'product_name' => 'SASH',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('SASH'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '3',
            'product_desc' => 'SCHOOL BADGE',
            'product_name' => 'SCHOOL BADGE',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('SCHOOL BADGE'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '3',
            'product_desc' => 'PROMOTIONAL BADGE',
            'product_name' => 'PROMOTIONAL BADGE',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('PROMOTIONAL BADGE'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '3',
            'product_desc' => 'SPORTS BADGE',
            'product_name' => 'SPORTS BADGE',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('SPORTS BADGE'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '3',
            'product_desc' => 'ELECTION BADGE',
            'product_name' => 'ELECTION BADGE',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('ELECTION BADGE'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ], [
            'category_id' => '4',
            'product_desc' => 'WHITE MUG',
            'product_name' => 'WHITE MUG',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('WHITE MUG'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ],  [
            'category_id' => '4',
            'product_desc' => 'INNER COLOR MUG',
            'product_name' => 'INNER COLOR MUG',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('INNER COLOR MUG'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
        ],  [
            'category_id' => '4',
            'product_desc' => 'PATCH MUG',
            'product_name' => 'PATCH MUG',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('PATCH MUG'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '4',
            'product_desc' => 'MAGIC MUG',
            'product_name' => 'MAGIC MUG',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('MAGIC MUG'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '4',
            'product_desc' => 'ANIMAL MUG',
            'product_name' => 'ANIMAL MUG',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('ANIMAL MUG'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '5',
            'product_desc' => 'PRINTED DRY COLLAR NECK FIT T- SHIRT',
            'product_name' => 'PRINTED DRY COLLAR NECK FIT T- SHIRT',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('PRINTED DRY COLLAR NECK FIT T- SHIRT'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '5',
            'product_desc' => 'PRINTED DRY ROUND NECK FIT T- SHIRT',
            'product_name' => 'PRINTED DRY ROUND NECK FIT T- SHIRT',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('PRINTED DRY ROUND NECK FIT T- SHIRT'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '6',
            'product_desc' => 'CAPS',
            'product_name' => 'CAPS',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('CAPS'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '7',
            'product_desc' => 'MOUSE PAD',
            'product_name' => 'MOUSE PAD',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('MOUSE PAD'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '8',
            'product_desc' => 'PEN',
            'product_name' => 'PEN',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('PEN'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '9',
            'product_desc' => 'SIPPER BOTTLES',
            'product_name' => 'SIPPER BOTTLES',
            'product_keyword' => '1',
            'status' => 'ACTIVE',
            'slug' => Str::slug('SIPPER BOTTLES'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '10',
            'product_desc' => 'KEYCHAIN NO. 23',
            'product_name' => 'KEYCHAIN NO. 23',
            'product_keyword' => 'KEYCHAIN NO. 23',
            'status' => 'ACTIVE',
            'slug' => Str::slug('KEYCHAIN NO. 23'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ], [
            'category_id' => '10',
            'product_desc' => 'KEYCHAIN NO. 24',
            'product_name' => 'KEYCHAIN NO. 24',
            'product_keyword' => 'KEYCHAIN NO. 24',
            'status' => 'ACTIVE',
            'slug' => Str::slug('KEYCHAIN NO. 24'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ],  [
            'category_id' => '10',
            'product_desc' => 'KEYCHAIN NO. 25',
            'product_name' => 'KEYCHAIN NO. 25',
            'product_keyword' => 'KEYCHAIN NO. 25',
            'status' => 'ACTIVE',
            'slug' => Str::slug('KEYCHAIN NO. 25'),
            'addonstables' => json_encode(array('card_sizes' => 'card_size', 'printing_type' => 'printing_type', 'barcode' => 'barcode_type', 'lanyard_fitting' => 'lanyard_fitting_name', 'hobler_type' => 'hobler_name')),
            'created_at' => now(),
            'updated_at' => now()
          ] ]);
    }
}
