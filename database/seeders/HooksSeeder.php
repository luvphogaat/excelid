<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('hooks')->insert([
           [
               'id' => NULL,
               'lanyard_id' => 11,
               'qty_id' => 19,
               'hooks_name' => 'REGULAR HOOK',
               'hook_image' => 'NULL',
               'price' => 200
           ],[
                'id' => NULL,
                'lanyard_id' => 11,
                'qty_id' => 20,
                'hooks_name' => 'REGULAR HOOK',
                'hook_image' => 'NULL',
                'price' => 400
            ],[
                'id' => NULL,
                'lanyard_id' => 11,
                'qty_id' => 21,
                'hooks_name' => 'REGULAR HOOK',
                'hook_image' => 'NULL',
                'price' => 600
            ],[
                'id' => NULL,
                'lanyard_id' => 11,
                'qty_id' => 22,
                'hooks_name' => 'REGULAR HOOK',
                'hook_image' => 'NULL',
                'price' => 800
            ],[
                'id' => NULL,
                'lanyard_id' => 11,
                'qty_id' => 23,
                'hooks_name' => 'REGULAR HOOK',
                'hook_image' => 'NULL',
                'price' => 1000
            ],[
                'id' => NULL,
                'lanyard_id' => 11,
                'qty_id' => 24,
                'hooks_name' => 'REGULAR HOOK',
                'hook_image' => 'NULL',
                'price' => 2000
            ],[
                'id' => NULL,
                'lanyard_id' => 11,
                'qty_id' => 25,
                'hooks_name' => 'REGULAR HOOK',
                'hook_image' => 'NULL',
                'price' => 4000
            ],
            // Tube Lanyard
            [
                'id' => NULL,
                'lanyard_id' => 12,
                'qty_id' => 26,
                'hooks_name' => 'REGULAR HOOK',
                'hook_image' => 'NULL',
                'price' => 200
            ],[
                 'id' => NULL,
                 'lanyard_id' => 12,
                 'qty_id' => 27,
                 'hooks_name' => 'REGULAR HOOK',
                 'hook_image' => 'NULL',
                 'price' => 400
             ],[
                 'id' => NULL,
                 'lanyard_id' => 12,
                 'qty_id' => 28,
                 'hooks_name' => 'REGULAR HOOK',
                 'hook_image' => 'NULL',
                 'price' => 600
             ],[
                 'id' => NULL,
                 'lanyard_id' => 12,
                 'qty_id' => 29,
                 'hooks_name' => 'REGULAR HOOK',
                 'hook_image' => 'NULL',
                 'price' => 800
             ],[
                 'id' => NULL,
                 'lanyard_id' => 12,
                 'qty_id' => 30,
                 'hooks_name' => 'REGULAR HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1500
             ],[
                 'id' => NULL,
                 'lanyard_id' => 12,
                 'qty_id' => 31,
                 'hooks_name' => 'REGULAR HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 12,
                 'qty_id' => 32,
                 'hooks_name' => 'REGULAR HOOK',
                 'hook_image' => 'NULL',
                 'price' => 4000
             ],

            //  Sattain 12MM England Hook
            [
                'id' => NULL,
                'lanyard_id' => 13,
                'qty_id' => 55,
                'hooks_name' => 'ENGLAND HOOK',
                'hook_image' => 'NULL',
                'price' => 300
            ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 56,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 600
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 57,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 900
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 58,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1200
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 59,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1500
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 60,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 3000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 61,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 6000
             ],

            //  Sattain 12MM Dog Hook
            [
                'id' => NULL,
                'lanyard_id' => 13,
                'qty_id' => 55,
                'hooks_name' => 'DOG HOOK',
                'hook_image' => 'NULL',
                'price' => 700
            ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 56,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1400
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 57,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2100
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 58,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2800
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 59,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 3500
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 60,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 7000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 61,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 14000
             ],
             //  Sattain 12MM FISH Hook
            [
                'id' => NULL,
                'lanyard_id' => 13,
                'qty_id' => 55,
                'hooks_name' => 'FISH HOOK',
                'hook_image' => 'NULL',
                'price' => 1000
            ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 56,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 57,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 3000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 58,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 4000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 59,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 5000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 60,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 10000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 61,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 20000
             ],

             //  Sattain 16MM England Hook
            [
                'id' => NULL,
                'lanyard_id' => 13,
                'qty_id' => 62,
                'hooks_name' => 'ENGLAND HOOK',
                'hook_image' => 'NULL',
                'price' => 200
            ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 63,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 400
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 64,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 600
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 65,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 800
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 66,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 67,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 68,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 4000
             ],

            //  Sattain 16MM Dog Hook
            [
                'id' => NULL,
                'lanyard_id' => 13,
                'qty_id' => 62,
                'hooks_name' => 'DOG HOOK',
                'hook_image' => 'NULL',
                'price' => 600
            ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 63,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1200
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 64,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1800
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 65,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2400
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 66,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 3000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 67,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 6000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 68,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 12000
             ],
             //  Sattain 16MM FISH Hook
            [
                'id' => NULL,
                'lanyard_id' => 13,
                'qty_id' => 62,
                'hooks_name' => 'FISH HOOK',
                'hook_image' => 'NULL',
                'price' => 900
            ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 63,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1800
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 64,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2700
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 65,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 4000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 66,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 4500
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 67,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 8000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 68,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 16000
             ],

              //  Sattain 20MM England Hook
            [
                'id' => NULL,
                'lanyard_id' => 13,
                'qty_id' => 69,
                'hooks_name' => 'ENGLAND HOOK',
                'hook_image' => 'NULL',
                'price' => 200
            ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 70,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 400
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 71,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 600
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 72,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 800
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 73,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 74,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 75,
                 'hooks_name' => 'ENGLAND HOOK',
                 'hook_image' => 'NULL',
                 'price' => 4000
             ],

            //  Sattain 20MM Dog Hook
            [
                'id' => NULL,
                'lanyard_id' => 13,
                'qty_id' => 69,
                'hooks_name' => 'DOG HOOK',
                'hook_image' => 'NULL',
                'price' => 600
            ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 70,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1200
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 71,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1800
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 72,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2400
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 73,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2500
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 74,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 5000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 75,
                 'hooks_name' => 'DOG HOOK',
                 'hook_image' => 'NULL',
                 'price' => 10000
             ],
             //  Sattain 20MM FISH Hook
            [
                'id' => NULL,
                'lanyard_id' => 13,
                'qty_id' => 69,
                'hooks_name' => 'FISH HOOK',
                'hook_image' => 'NULL',
                'price' => 800
            ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 70,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 1600
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 71,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 2400
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 72,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 3200
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 73,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 4000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 74,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 7000
             ],[
                 'id' => NULL,
                 'lanyard_id' => 13,
                 'qty_id' => 75,
                 'hooks_name' => 'FISH HOOK',
                 'hook_image' => 'NULL',
                 'price' => 12000
             ],
        ]);
    }
}
