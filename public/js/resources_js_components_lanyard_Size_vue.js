"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_lanyard_Size_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/lanyard/Size.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/lanyard/Size.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vueBasePath_components_common_Previous_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vueBasePath/components/common/Previous.vue */ "./resources/js/components/common/Previous.vue");
/* harmony import */ var vueBasePath_components_common_Button_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vueBasePath/components/common/Button.vue */ "./resources/js/components/common/Button.vue");
/* harmony import */ var vueBasePath_components_common_ButtonNoBackground_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vueBasePath/components/common/ButtonNoBackground.vue */ "./resources/js/components/common/ButtonNoBackground.vue");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




(axios__WEBPACK_IMPORTED_MODULE_3___default().defaults.xsrfCookieName) = 'csrftoken';
(axios__WEBPACK_IMPORTED_MODULE_3___default().defaults.xsrfHeaderName) = 'X-CSRFTOKEN';
var apiClient = axios__WEBPACK_IMPORTED_MODULE_3___default().create({
  baseURL: 'https://excelidcardsolutions.com/api/',
  withCredentials: false,
  // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    Button: vueBasePath_components_common_Button_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    Previous: vueBasePath_components_common_Previous_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    ButtonNoBackground: vueBasePath_components_common_ButtonNoBackground_vue__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      lanyardSizes: [],
      lanyardSelectedSize: ''
    };
  },
  computed: {
    showPrevious: function showPrevious() {
      return this.$store.state.previous;
    }
  },
  methods: {
    moveprevious: function moveprevious() {
      this.$store.dispatch('previous');
    },
    nextField: function nextField() {
      this.$store.dispatch('loaderAction', true);
      this.$store.dispatch('lanyardblock/setLanyardSize', this.lanyardSelectedSize);
      this.$store.dispatch('loaderAction', false);
      this.$store.dispatch('next');
    },
    skip: function skip() {}
  },
  // Validations: () {
  // }
  created: function created() {
    var _this = this;

    var selectLanyard = this.$store.getters['lanyardblock/lanyardData'];
    this.$store.dispatch('loaderAction', true);
    apiClient.get("".concat("https://excelidcardsolutions.com/api/", "project/lanyard/").concat(selectLanyard, "/size/list")).then(function (res) {
      if (res.status == 200 || res.status == 201) {
        _this.lanyardSizes = res.data;
        console.log('Size Data', res.data);

        _this.$store.dispatch('loaderAction', false);
      }
    })["catch"](function (err) {
      _this.$store.dispatch('loaderAction', false);
    });
  }
});

/***/ }),

/***/ "./resources/js/components/lanyard/Size.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/lanyard/Size.vue ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Size_vue_vue_type_template_id_a0a61cb0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Size.vue?vue&type=template&id=a0a61cb0& */ "./resources/js/components/lanyard/Size.vue?vue&type=template&id=a0a61cb0&");
/* harmony import */ var _Size_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Size.vue?vue&type=script&lang=js& */ "./resources/js/components/lanyard/Size.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Size_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Size_vue_vue_type_template_id_a0a61cb0___WEBPACK_IMPORTED_MODULE_0__.render,
  _Size_vue_vue_type_template_id_a0a61cb0___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/lanyard/Size.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/lanyard/Size.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/lanyard/Size.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Size_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Size.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/lanyard/Size.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Size_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/lanyard/Size.vue?vue&type=template&id=a0a61cb0&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/lanyard/Size.vue?vue&type=template&id=a0a61cb0& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Size_vue_vue_type_template_id_a0a61cb0___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Size_vue_vue_type_template_id_a0a61cb0___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Size_vue_vue_type_template_id_a0a61cb0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Size.vue?vue&type=template&id=a0a61cb0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/lanyard/Size.vue?vue&type=template&id=a0a61cb0&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/lanyard/Size.vue?vue&type=template&id=a0a61cb0&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/lanyard/Size.vue?vue&type=template&id=a0a61cb0& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "lanyardField", staticStyle: { width: "100%" } },
    [
      _c(
        "div",
        { staticClass: "container-fluid", staticStyle: { padding: "20px" } },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "row scroller",
              staticStyle: {
                "overflow-y": "auto",
                "max-height": "520px",
                padding: "10px"
              }
            },
            _vm._l(_vm.lanyardSizes, function(lanyard, index) {
              return _c("div", { key: index, staticClass: "col-md-3" }, [
                _c("label", [_vm._v(_vm._s(lanyard.lanyard_fitting_size))])
              ])
            }),
            0
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "row", staticStyle: { "margin-top": "80px" } },
        [
          _c(
            "div",
            { staticClass: " btn_previous_rec" },
            [
              _c(
                "Previous",
                {
                  attrs: {
                    savePrevious: _vm.moveprevious,
                    isPrevious: _vm.showPrevious
                  }
                },
                [_c("i", { staticClass: "arrow_grey" })]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "bottom_button" },
            [
              _c(
                "ButtonNoBackground",
                { attrs: { btnclass: ["btn_nobg"], save: _vm.skip } },
                [_vm._v("\n                Skip\n            ")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm.lanyardSelectedSize
            ? _c("Button", {
                attrs: {
                  save: _vm.nextField,
                  btnclass: ["btn1", "btn_next_rec", "btn_next2"]
                },
                scopedSlots: _vm._u(
                  [
                    {
                      key: "icon",
                      fn: function() {
                        return [
                          _vm._v(" Next "),
                          _c("i", { staticClass: "zmdi zmdi-arrow-left" })
                        ]
                      },
                      proxy: true
                    }
                  ],
                  null,
                  false,
                  143370125
                )
              })
            : _vm._e()
        ],
        1
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-12" }, [
        _c("h3", { staticClass: "text-center" }, [
          _vm._v("Select one of the Lanyard Size")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ })

}]);