"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_customization_Customization_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Canvasdesign.vue?vue&type=script&lang=ts&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Canvasdesign.vue?vue&type=script&lang=ts& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (vue__WEBPACK_IMPORTED_MODULE_0__["default"].extend({
  data: function data() {
    return {
      canvas: null
    };
  },
  props: {
    canviData: {
      type: Object,
      required: true
    }
  },
  mounted: function mounted() {},
  methods: {}
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Customization.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Customization.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _common_Previous_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/Previous.vue */ "./resources/js/components/common/Previous.vue");
/* harmony import */ var _common_Button_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/Button.vue */ "./resources/js/components/common/Button.vue");
/* harmony import */ var _Canvasdesign_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Canvasdesign.vue */ "./resources/js/components/customization/Canvasdesign.vue");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_3__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




(axios__WEBPACK_IMPORTED_MODULE_3___default().defaults.xsrfCookieName) = 'csrftoken';
(axios__WEBPACK_IMPORTED_MODULE_3___default().defaults.xsrfHeaderName) = 'X-CSRFTOKEN';
var apiClient = axios__WEBPACK_IMPORTED_MODULE_3___default().create({
  baseURL: "".concat("https://excelidcardsolutions.com/api/"),
  withCredentials: false,
  // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    Canvasdesign: _Canvasdesign_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    Button: _common_Button_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    Previous: _common_Previous_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: {
    canvasData: {
      type: Object,
      required: true
    }
  },
  data: function data() {
    return {
      textData: this.canvasData.data1.text,
      canvas: null,
      copydata: Object.assign(this.canvasData),
      firstRender: null,
      product: null,
      canvasUrl: ''
    };
  },
  computed: {
    showPrevious: function showPrevious() {
      return this.$store.state.customizationblock.disablePrevious;
    }
  },
  mounted: function mounted() {
    this.product = this.$store.getters['productDetail'];
    var data = this.canvasData;
    this.canvas = document.getElementById("myCanvas");
    this.canvas.width = data.canvasWidth * 2;
    this.canvas.height = data.canvasHeight * 2;
    this.firstRender = true;
    this.renderFont();
    this.createCanvas();
  },
  methods: {
    createCanvas: function createCanvas() {
      var that = this;
      var data = this.canvasData;
      var copyData = this.copydata;
      var firstRender = this.firstRender;
      var context = this.canvas.getContext('2d');
      var templateLayout = new Image();
      var wrapText = this.wrapText;
      var createCanvas = this.createCanvas;
      var lineHeight = 20;
      var maxWidth = 200;
      var fontFamily = this.fontName;
      templateLayout.src = data.canvasTemplate1;

      templateLayout.onload = function () {
        context.drawImage(templateLayout, 0, 0, data.canvasWidth * 2, data.canvasHeight * 2);
        data.data1.images.forEach(function (imgObj, index) {
          // if (firstRender || copyData.images[index].src != data.images[index].src) {
          that.firstRender = false;
          var img = new Image();
          img.src = imgObj.src;

          img.onload = function () {
            context.drawImage(img, imgObj.xAxis, imgObj.yAxis, imgObj.width, imgObj.height);
          };

          img.crossOrigin = 'Anonymous'; // }
        });
        data.data1.text.forEach(function (textObj) {
          context.fillStyle = textObj.color;
          context.font = textObj.fontSize + "px " + fontFamily; // context.fillText(textObj.text, textObj.xAxis, textObj.yAxis)

          wrapText(context, textObj.text, textObj.xAxis, textObj.yAxis, textObj.maxWidth, lineHeight);
        });
      };
    },
    createImageCanvas: function createImageCanvas() {},
    createTextCanvas: function createTextCanvas() {},
    wrapText: function wrapText(context, text, x, y, maxWidth, lineHeight) {
      var words = text.split(' ');
      var line = '';

      for (var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + ' ';
        var metrics = context.measureText(testLine);
        var testWidth = metrics.width;

        if (testWidth > maxWidth && n > 0) {
          context.fillText(line, x, y);
          line = words[n] + ' ';
          y += lineHeight;
        } else {
          line = testLine;
        }
      }

      context.fillText(line, x, y);
    },
    onInput: function onInput(value) {
      this.createCanvas();
    },
    nextField: function nextField() {
      var _this = this;

      var session_id = this.$store.state.sessionData;
      var userData = this.$store.state.userData || null;
      var designId = this.$store.getters['customizationblock/customizationOData'].id || null;
      var designData = this.canvasData.data1;
      var productId = this.product ? this.product.id : ''; // let formData = new FormData();

      var formData = {
        session_id: session_id,
        user_id: userData ? userData.id : '',
        data: this.canvasData.data1,
        product_id: productId,
        design_id: designId
      };
      this.$store.dispatch('loaderAction', true);
      apiClient.post("".concat("https://excelidcardsolutions.com/api/", "project/design/customization/save"), formData).then(function (res) {
        if (res.status == 200 || res.status == 201) {
          _this.$store.dispatch('customizationblock/setCustomizationFrontData', _this.canvasData.data1);

          _this.$store.dispatch('customizationblock/setFrontSaved', true);

          if (_this.canvasData.viewType == 1) {
            _this.$store.dispatch('customizationblock/setCustomizationBackData', true);

            _this.$store.dispatch('customizationblock/setBackSaved', true);

            _this.$store.dispatch('customizationblock/setBlockSaved', true);
          }

          _this.$store.dispatch('loaderAction', false);

          localStorage.setItem('projectId', res.data.id);

          _this.$store.dispatch('next');
        }
      })["catch"](function (error) {
        _this.$store.dispatch('loaderAction', false);

        console.log(error, 'Error');
      });
    },
    moveprevious: function moveprevious() {},
    onFileChange: function onFileChange(e, index) {
      var files = e.target.files || e.dataTransfer.files;
      if (!files.length) return;
      this.createImage(files[0], index);
    },
    renderFont: function renderFont() {
      var node = document.createElement('link'); // creates the script tag

      node.href = this.canvasData.fontUrl; // sets the source (insert url in between quotes)

      node.rel = 'rel="stylesheet"'; // makes script run asynchronously

      document.getElementsByTagName('head')[0].appendChild(node);
    },
    createImage: function createImage(file, index) {
      this.canvasData.data1.images[index].src = URL.createObjectURL(file);
      console.log(this.canvasData.data1.images[index]);
      this.createCanvas();
    }
  }
});

/***/ }),

/***/ "./resources/js/components/customization/Canvasdesign.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/customization/Canvasdesign.vue ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Canvasdesign_vue_vue_type_template_id_13f6d906_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Canvasdesign.vue?vue&type=template&id=13f6d906&scoped=true& */ "./resources/js/components/customization/Canvasdesign.vue?vue&type=template&id=13f6d906&scoped=true&");
/* harmony import */ var _Canvasdesign_vue_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Canvasdesign.vue?vue&type=script&lang=ts& */ "./resources/js/components/customization/Canvasdesign.vue?vue&type=script&lang=ts&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Canvasdesign_vue_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Canvasdesign_vue_vue_type_template_id_13f6d906_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _Canvasdesign_vue_vue_type_template_id_13f6d906_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "13f6d906",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/customization/Canvasdesign.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/customization/Customization.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/customization/Customization.vue ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Customization_vue_vue_type_template_id_a07ae808_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Customization.vue?vue&type=template&id=a07ae808&scoped=true& */ "./resources/js/components/customization/Customization.vue?vue&type=template&id=a07ae808&scoped=true&");
/* harmony import */ var _Customization_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Customization.vue?vue&type=script&lang=js& */ "./resources/js/components/customization/Customization.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Customization_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Customization_vue_vue_type_template_id_a07ae808_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _Customization_vue_vue_type_template_id_a07ae808_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "a07ae808",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/customization/Customization.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/customization/Canvasdesign.vue?vue&type=script&lang=ts&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/customization/Canvasdesign.vue?vue&type=script&lang=ts& ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Canvasdesign_vue_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Canvasdesign.vue?vue&type=script&lang=ts& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Canvasdesign.vue?vue&type=script&lang=ts&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Canvasdesign_vue_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/customization/Customization.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/customization/Customization.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Customization_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Customization.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Customization.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Customization_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/customization/Canvasdesign.vue?vue&type=template&id=13f6d906&scoped=true&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/components/customization/Canvasdesign.vue?vue&type=template&id=13f6d906&scoped=true& ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Canvasdesign_vue_vue_type_template_id_13f6d906_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Canvasdesign_vue_vue_type_template_id_13f6d906_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Canvasdesign_vue_vue_type_template_id_13f6d906_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Canvasdesign.vue?vue&type=template&id=13f6d906&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Canvasdesign.vue?vue&type=template&id=13f6d906&scoped=true&");


/***/ }),

/***/ "./resources/js/components/customization/Customization.vue?vue&type=template&id=a07ae808&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/components/customization/Customization.vue?vue&type=template&id=a07ae808&scoped=true& ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Customization_vue_vue_type_template_id_a07ae808_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Customization_vue_vue_type_template_id_a07ae808_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Customization_vue_vue_type_template_id_a07ae808_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Customization.vue?vue&type=template&id=a07ae808&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Customization.vue?vue&type=template&id=a07ae808&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Canvasdesign.vue?vue&type=template&id=13f6d906&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Canvasdesign.vue?vue&type=template&id=13f6d906&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.canviData
    ? _c("div", [
        _c("canvas", {
          staticStyle: { background: "grey", margin: "5px auto" },
          attrs: { id: "myCanvas", width: "650", height: "408" }
        })
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Customization.vue?vue&type=template&id=a07ae808&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/customization/Customization.vue?vue&type=template&id=a07ae808&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticStyle: { width: "100%" } }, [
    _c(
      "div",
      { staticClass: "container-fluid", staticStyle: { padding: "10px" } },
      [
        _c("div", { staticClass: "row", staticStyle: { height: "70vh" } }, [
          _c(
            "div",
            {
              staticClass: "col-md-2 scroller",
              staticStyle: {
                "overflow-y": "scroll",
                height: "70vh",
                background: "#d0c9c95e"
              }
            },
            [
              _c(
                "ul",
                _vm._l(_vm.textData, function(textA, index) {
                  return _c("li", { key: index }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Text " + _vm._s(index + 1))]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: textA.text,
                            expression: "textA.text"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text" },
                        domProps: { value: textA.text },
                        on: {
                          blur: function($event) {
                            return _vm.onInput($event, true)
                          },
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(textA, "text", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "form-label",
                          attrs: { for: "'fontSizeRange'(index+1)" }
                        },
                        [_vm._v("Font Size")]
                      ),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: textA.fontSize,
                            expression: "textA.fontSize"
                          }
                        ],
                        staticClass: "form-range",
                        staticStyle: { color: "red" },
                        attrs: {
                          type: "range",
                          min: "0",
                          max: "100",
                          id: "'fontSizeRange'(index+1)"
                        },
                        domProps: { value: textA.fontSize },
                        on: {
                          change: function($event) {
                            return _vm.onInput($event, true)
                          },
                          __r: function($event) {
                            return _vm.$set(
                              textA,
                              "fontSize",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ])
                  ])
                }),
                0
              ),
              _vm._v(" "),
              _c(
                "ul",
                _vm._l(_vm.canvasData.data1.images, function(image, index) {
                  return _c("li", { key: index }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Image " + _vm._s(index + 1))]),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "form-control",
                        attrs: { type: "file", accept: "image/*" },
                        on: {
                          change: function($event) {
                            return _vm.onFileChange($event, index)
                          }
                        }
                      })
                    ])
                  ])
                }),
                0
              )
            ]
          ),
          _vm._v(" "),
          _vm._m(0)
        ])
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "row", staticStyle: { "margin-top": "80px" } },
      [
        _c("Button", {
          attrs: {
            save: _vm.nextField,
            btnclass: ["btn1", "btn_next_rec", "btn_next2"]
          },
          scopedSlots: _vm._u([
            {
              key: "icon",
              fn: function() {
                return [
                  _vm._v(" Next "),
                  _c("i", { staticClass: "zmdi zmdi-arrow-left" })
                ]
              },
              proxy: true
            }
          ])
        })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-10" }, [
      _c("canvas", {
        staticStyle: {
          background: "grey",
          margin: "5px auto",
          height: "70vh !important"
        },
        attrs: { id: "myCanvas", crossorigin: "anonymous" }
      })
    ])
  }
]
render._withStripped = true



/***/ })

}]);