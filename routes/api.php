<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('project/templates', [ProjectController::class, 'getTemplateDesigns']);

Route::post('project/design/admin/save', [ProjectController::class, 'saveTemplates']);

Route::post('project/design/admin/update/{tid}', [ProjectController::class, 'updateTemplates']);

Route::get('project/font/list', [ProjectController::class, 'listFonts']);

Route::get('projects/design/category/list', [ProductsController::class, 'categoryList']);

Route::get('projects/design/category/{cid}/products/list', [ProductsController::class, 'productList']);

Route::post('projects/design/{pid}/order/temp/save', [ProjectController::class, 'saveGuestData']);

Route::get('project/design/template/edit/{id}', [ProjectController::class, 'templateInfo']);

Route::get('project/{projectId}/status', [ProjectController::class, 'projectStatus']);

Route::get('project/lanyard/list', [ProjectController::class, 'lanyardsList']);

Route::get('project/lanyard/{lanyardId}/size/list', [ProjectController::class, 'lanyardsSelectedSizeList']);

Route::get('project/lanyard/{lanyardId_fittingId}/hooks/list', [ProjectController::class, 'lanyardSelectectFitting']);

Route::get('project/quantity/{quantityId}/print-type/list', [ProjectController::class, 'printtypeFitting']);

// Saving Customization Data, First Screen to save first time
Route::post('project/design/customization/save', [ProjectController::class, 'saveOrderCustomizationData']);

Route::patch('project/design/{pid}/customization/save', [ProjectController::class, 'saveOrderCustomizationBackData']);

// Route::put('project/saveCustomization', [ProjectController::class, '']);

// Route::patch('project/{projectId}/saveCustomization', [ProjectController::class, '']);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// on JobSize Change
Route::get('product/{pid}/size/{sid}/quantity', [ProjectController::class, 'product_getQuantity']);

// onChange Job
Route::get('product/{pid}/size/quantity/{quantityId}/price', [ProjectController::class, 'product_getQuantityPrice']);

// onChange Lanyard - style, size, hooks
Route::get('product/{pid}/quantity/{quantityId}/addon/lanyard/{lid}/data/{type}', [ProjectController::class, 'product_getLanyardStyleDetail']);

Route::get('product/{pid}/qunatity/{quantityId}/addon/print/{printId}', [ProjectController::class, 'product_getPrintingPrice']);




// Route::post('product/{pid}/customDesign/{uid}/artwork/save', [ProjectController::class, 'product_saveCustomDesignArtWork']);
