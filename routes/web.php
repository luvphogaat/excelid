<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// RouFile::exists($product->product_images)te::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [IndexController::class, 'index'])->name('index');

Route::get('index', [IndexController::class, 'index'])->name('index');

Route::get('my-captcha', [IndexController::class, 'myCaptcha'])->name('myCaptcha');

Route::get('refresh_captcha', [IndexController::class, 'refreshCaptcha'])->name('refresh_captcha');

Route::get('terms-and-conditions', [IndexController::class , 'termsConditionPage'])->name('terms-conditions');

Route::get('return-policy', [IndexController::class , 'returnpolicy'])->name('return-policy');

Route::get('privacy-policy', [IndexController::class , 'privacypolicy'])->name('privacy-policy');

Route::get('contact-us', function () {
    return view('contactUs');
})->name('contact-us');

Route::get('products/{id}', [ProductsController::class, 'singleCategory'])->name('products.list');

Route::get('products/{parentslug}/{slug}', [ProductsController::class, 'singleProduct'])->name('product.single');

Route::get('product/{pid}/options', [ProductsController::class, 'designProduct'])->name('design.option');

Route::get('products/{parentslug}/{slug}/designs', [ProjectController::class, 'browseDesigns'])->name('designs.list');

Route::get('products/{parentslug}/{slug}/customDesign/{uid}', [ProjectController::class, 'customDesigns'])->name('designs.custom');

Route::post('products/{parentslug}/{slug}/customDesign/{uid}/save', [ProjectController::class, 'customDesignsSave'])->name('designs.custom.save');

Route::get('cart', [ProductsController::class, 'cart'])->name('cart');

Route::delete('cart/{id}/remove', [ProductsController::class, 'removeCart'])->name('cart.remove');

Route::post('contactus/save', [IndexController::class, 'contactUs'])->name('contactus.save');

Route::any('/products/{parentslug}/{slug}/designs/customize/{id}', [ProjectController::class, 'index'])->name('design.customization');

Route::get('logout', function () {
    return view('index');
})->name('logout');

// Route::post('product/design/project/step', [ProjectController::class, 'changeStep']);

// Route::middleware(['auth:sanctum', 'verified', 'role:User'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

// Route::middleware(['auth:sanctum', 'verified', 'role:User'])->get('/order', function () {
//     return view('order');
// })->name('order');

Route::group(['middleware' => 'role:User,Admin'], function () {
    Route::get('dashboard', [HomeController::class, 'index'])->name('user.dashboard');
    Route::get('order', [HomeController::class, 'orders'])->name('user.order');
    // Route::get('cart', [HomeController::class, 'carts'])->name('user.cart');
    // Route::get('checkout', [HomeController::class, 'checkout'])->name('user.checkout');
});

Route::get('product/application/designs/addon/{categoryId}', [ProjectController::class, 'getProductAddOns']);

Route::post('review/post/{pid}', [ProductsController::class, 'saveReview'])->name('post.review');

Route::get('checkout', [HomeController::class, 'checkout'])->name('checkout');

Route::post('search', [ProductsController::class, 'search'])->name('search.post');

Route::post('order/save', [HomeController::class, 'saveCheckout'])->name('checkout.save');

Route::get('payment-success', [HomeController::class, 'payment_success']);
