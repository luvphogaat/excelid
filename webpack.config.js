const path = require('path');

module.exports = {
    resolve: {
        alias: {
            // '@': path.resolve('resources/js'),
            'vueBaseSassPath': path.resolve('resources/sass'),
            'vueBasePath': path.resolve('resources/js'),
        },
    },
};
