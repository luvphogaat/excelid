<?php

namespace App\Providers;

use Session;
use App\Actions\Jetstream\DeleteUser;
use Illuminate\Support\ServiceProvider;
use Laravel\Jetstream\Jetstream;
use Laravel\Fortify\Fortify;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class JetstreamServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configurePermissions();

        Jetstream::deleteUsersUsing(DeleteUser::class);

        Fortify::authenticateUsing(function (Request $request) {
            $user = User::where('email', $request->email)->first();
        
            if ($user &&
                Hash::check($request->password, $user->password)) {
                $session = Session::getId();
                // dd($user->id);

                $project = DB::table('project')->where('ip_address', $request->ip())->get();
                if ($project !== null)  {
                    DB::table('project')->where('ip_address', $request->ip())->update(['user_id' => $user->id]);
                }
                return $user;
            }
        });
    }

    /**
     * Configure the permissions that are available within the application.
     *
     * @return void
     */
    protected function configurePermissions()
    {
        Jetstream::defaultApiTokenPermissions(['read']);

        Jetstream::permissions([
            // 'create',
            'read',
            'update',
            'delete',
        ]);
    }
}
