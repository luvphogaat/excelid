<?php

namespace App\Providers;

use App\Models\Menu;

use Session;
use Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Paginator::useBootstrap();
        $session = Session::getId();
        Schema::defaultStringLength(191);
        $menuItems = Menu::with('submenus')->orderBy('orderBy')->get();
        // $menu = new \App\Models\Menu;
        // $menuItems = $menu->tree();
        // return view('index')->with('menulist', $menuList);
        // dd($menuItems);
        // $cartCount = 3;
        $cartCount = DB::table('project')->where('ip_address', Request::ip())->where('price',  '!=', 'NULL')->count();
        view()->share(['menuItems' =>  $menuItems, 'cartCount' => $cartCount]);
    }
}
