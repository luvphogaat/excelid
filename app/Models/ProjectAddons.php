<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectAddons extends Model
{
    use HasFactory;

    protected $table = 'project_addons';

    protected $fillable = [
        'id', 'product_id', 'project_id', 'addons_id', 'addons_name'
    ];

}