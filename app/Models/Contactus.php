<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Contactus extends Model
{
    use HasFactory;

    protected $table = 'contactus';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'name', 'email', 'subject', 'message'
    ];

    public static function boot()
    {
        parent::boot();
         static::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }
}
