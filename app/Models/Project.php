<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $table = 'design_templates';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'category_id',
        'product_id',
        'data1',
        'data2',
        'fill_template1',
        'fill_template2',
        'fontName',
        'fontUrl',
        'canvasType',
        'canvasWidth',
        'canvasHeight',
        'borderRound',
        'canvasTemplate1',
        'canvasTemplate2',
        'viewType'
    ];

}
