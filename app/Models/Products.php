<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'products';
    protected $softDelete = true;
    protected $fillable = [
        'id', 'category_id', 'product_desc', 'product_name', 'product_keyword', 'status', 'addonstables'
    ];

    public function category() {
        return $this->hasMany('App\Models\Category', 'id', 'category_id');
    }

    // public function images() {
    //     return $this->hasMany('App\Models\ProductImage', 'product_id', 'id');
    // }

    // public function jobSize() {
    //     return $this->hasMany('App\Models\ProductJobSize', 'product_id', 'id');
    // }

    public function priceQty() {
        return $this->hasMany('App\Models\ProductQtyPrices', 'product_id', 'id');
    }

    public function lanyardSizes() {
        return $this->hasMany('App\Models\LanyardFitting', 'lanyard_id', 'id')->where('category','=', 2);
    }
}
