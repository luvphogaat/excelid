<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'product_id',
        'message',
        'rating',
        'status',
    ];

    public function users() {
        return $this->hasMany('App\Models\User', 'id', 'customer_id');
    }

}
