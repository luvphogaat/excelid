<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Webpatser\Uuid\Uuid;
use Illuminate\Support\Str;


class ProjectOrder extends Model
{
    use HasFactory;

    protected $table = 'project';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'project_name', 'job_size', 'session_id', 'user_id', 'data', 'data2', 'quantity', 'price', 'product_id', 'image_file_path', 'text_file_path', 'artwork_file_path' ,'variable_data', 'design_id', 'created_at', 'updated_at', 'ip_address'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Str::uuid();
        });
    }
    public function getIncrementing()
    {
        return false;
    }

}
