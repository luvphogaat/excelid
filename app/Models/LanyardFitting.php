<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LanyardFitting extends Model
{
    use HasFactory;

    protected $table = 'lanyard_fitting';

    protected $fillable = [
        'id', 'lanyard_id', 'lanyard_fitting_size', 'price', 'category'
    ];
}
