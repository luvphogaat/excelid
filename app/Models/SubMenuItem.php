<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubMenuItem extends Model
{
    use HasFactory;

    protected $fillable = ['title','parent_id', 'child_id', 'link', 'status'];

    public function menu()
    {
        return $this->belongsTo('App\Models\Menu', 'id', 'parent_id')->where('status', 'ENABLED');
    }

    // public function subsubmenus()
    // {
    //     return $this->hasMany('App\Models\SubMenuItem', 'child_id', 'id')->where('status', 'ENABLED');
    // }
}
