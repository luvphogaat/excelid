<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductJobSize extends Model
{
    use HasFactory;

    protected $table = 'job_size';

    protected $fillable = [
        'id', 'size_name', 'product_id'
    ];

    public function jobQtyPrice() {
        return $this->hasMany('App\Models\ProductQtyPrices', 'size_id', 'id');
    }

}
