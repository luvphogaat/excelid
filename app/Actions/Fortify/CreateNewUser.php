<?php

namespace App\Actions\Fortify;

use Session;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();

        $newUser =  User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'role_id' => '2'
        ]);
        if ($newUser) {
            $session = Session::getId();
            $project = DB::table('project')->where('ip_address', \Request::ip())->get();
            if ($project !== null)  {
                DB::table('project')->where('ip_address', \Request::ip())->update(['user_id' => $newUser->id]);
            }
            return $newUser;
        }
    }
}
