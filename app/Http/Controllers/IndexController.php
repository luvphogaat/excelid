<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ProductQtyPrices;
use App\Models\Products;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    //

    public function expo() {
        return view('expo');
    }

    public function index() {
        // $products = Products::with('priceQty')->inRandomOrder()->limit(4)->get();
        $products = Products::with('priceQty')->inRandomOrder()->limit(4)->get();
        foreach($products as $product) {
            // LIMIT 0,1
            $imageData = DB::table('product_images')->where('product_id', $product->id)->first();
            if (!empty($imageData)) {
               $product->product_images = $imageData->product_image;
           }
        }
        return view('index', ['products' => $products]);
    }

    public function contactUs(Request $request) {

        request()->validate([
            'email' => 'required|email',
            'password' => 'required',
            'captcha' => 'required|captcha'
        ],
        ['captcha.captcha'=>'Invalid captcha code.']);
        // dd('You are here');
        $contactus = new Contactus;
        $contactus->name = $request->name;
        $contactus->email = $request->email;
        $contactus->subject = $request->subject;
        $contactus->message = $request->message;
        $contactus->save();
        \Session::flash('flash_message','Application has been successfully submitted.');
        return redirect(route('contact-us'));
    }

    public function myCaptcha()
    {
        return view('myCaptcha');
    }

    public function myCaptchaPost(Request $request)
    {
        request()->validate([
            'email' => 'required|email',
            'password' => 'required',
            'captcha' => 'required|captcha'
        ],
        ['captcha.captcha'=>'Invalid captcha code.']);
        // dd("You are here :) .");
    }

    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

    public function termsConditionPage() {
        return view('termandconditions');
    }
    // returnpolicy
    public function returnpolicy() {
        return view('returnpolicy');
    }

    public function privacypolicy() {
        return view('privacypolicy');
    }   
}

