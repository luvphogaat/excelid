<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\ProductReview;
use App\Models\ProductQtyPrices;
use App\Models\ProductJobSize;
use App\Models\ProductAddon;
use App\Models\ProjectAddons;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Products;
use App\Models\productImages;

class ProductsController extends Controller
{
    //
    public function singleProduct($parentslug, $slug) {
        $product = Products::with('priceQty')->where('slug', $slug)->first();
        $id = $product->id;
        $categoryId = $product->category_id;
        $categoryDetail = DB::table('category')->select('category_title')->where('id', $categoryId)->first();
        $relatedProducts = Products::with('priceQty')->where('category_id', $categoryId)->whereNotIn('id', [$id])->get();
        foreach($relatedProducts as $productRelated) {
            // LIMIT 0,1
            $imageData1 = productImages::where('product_id', $productRelated->id)->first();
            if (!empty($imageData1)) {
               $productRelated->product_images = $imageData1->product_image;

           }
        }
        $reviews = ProductReview::with('users')->where('product_id', $id)->simplePaginate(5);
        $reviewCount = count(ProductReview::with('users')->where('product_id', $id)->get());
        $rating = ProductReview::where('product_id', $id)->avg('rating');
            // LIMIT 0,1
        $imageData = productImages::select('product_image')->where('product_id', $id)->get();
        if (!empty($imageData)) {
            $product->product_images = $imageData;
        }
        $uid = date("Ymdhis") . rand(1, 200);
        // $imageData = DB::table('product_images')->where('product_id', $product->id)->inRandomOrder()->first();
        //     if (!empty($imageData)) {
        //         $product->product_images = $imageData->product_image;
        //     }
        $templates = DB::table('design_templates')->where('product_id', $product->id)->get();
        // dd($product);
        $designCount = count($templates);
        return view('product-single', compact('product', 'relatedProducts', 'reviews', 'reviewCount', 'rating', 'categoryDetail', 'designCount', 'uid'));
    }

    public function singleCategory($href) {
        $productData = array();
        $users = DB::table('category')->where('category_href', $href)->first();
        $cat_id = $users->id;
        $totalProducts = Products::where('category_id', $cat_id)->get()->count();
        $products = Products::with('priceQty')->where('category_id', $cat_id)->orderBy('category_id', 'desc')->get();
        foreach($products as $product) {
             // LIMIT 0,1
             $imageData = DB::table('product_images')->where('product_id', $product->id)->first();
             if (!empty($imageData)) {
                $product->product_images = $imageData->product_image;
            }
        }
        // dd($products);
        return view('products', array('users'=>$users, 'products' => $products, 'productCount' => $totalProducts));
    }

    public function designProduct($pid) {
        $uid = date("Ymdhis") . rand(1, 200);
        $productDetail = Products::where('id', $pid)->first();
        $imageData = DB::table('product_images')->where('product_id', $productDetail->id)->inRandomOrder()->first();
            if (!empty($imageData)) {
                $productDetail->product_images = $imageData->product_image;
            }
        $categoryDetail = DB::table('category')->where('id', $productDetail->category_id)->first();
        $templates = DB::table('design_templates')->where('product_id', $pid)->get();
        return view('subproducts', ['product' => $productDetail, 'category' => $categoryDetail, 'designCount' => count($templates), 'uid' => $uid]);
    }

    public function cart() {
        $orders = array();
        if ( Auth::check() ) {
            $userId = Auth::user()->id;
            $cartData = DB::table('project')->where('user_id', $userId)->where('price',  '!=', 'NULL')->get();
        } else {
            $cartData = DB::table('project')->where('ip_address', request()->ip())->where('price',  '!=', 'NULL')->get();
        }
        foreach (json_decode($cartData, true) as $key => $value1) {
            $addons = array();
            $productId = $value1['product_id'];
            $productDetails = Products::where('id', $productId)->first();
            $imageData = DB::table('product_images')->where('product_id', $productDetails->id)->first();
             if (!empty($imageData)) {
                $productDetails->product_images = $imageData->product_image;
            }
            $categoryId = $productDetails->category_id;
            $categoryDetails = DB::table('category')->where('id', $categoryId)->first();
            $data['id'] = $value1['id'];
            $data['category_name'] = $categoryDetails->category_title;
            $data['product_name'] = $productDetails->product_name;
            $data['design_id'] = $value1['design_id'];
            $data['project_name'] = $value1['project_name'];
            $data['quantity'] = ProductQtyPrices::select('quantity')->where('id', $value1['quantity'])->first()->quantity;
            $data['price'] = $value1['price'];
            $data['variable_data'] = $value1['variable_data']; // job_size
            $data['job_size'] = ProductJobSize::select('size_name')->where('id', $value1['job_size'])->first()->size_name;
            $addonsData = ProjectAddons::where('project_id',$value1['id'])->get();
            if (count($addonsData) > 0) {
                foreach ($addonsData as $key => $value) {
                    switch($value->addons_name){
                        case 'lanyardStyle':
                            $addons['Lanyard Style'] = Products::select('product_name')->where('id', $value->addons_id)->first()->product_name;
                            break;
                        case 'lanyardSize':
                            $addons['Lanyard Size'] = DB::table('job_size')->select('size_name')->where('id', $value->addons_id)->first()->size_name;
                            break;
                        case 'lanyardHook':
                            $addons['Lanyard Hook'] = DB::table('hooks')->select('hooks_name')->where('id', $value->addons_id)->first()->hooks_name;
                            break;
                    }
                }
            }
            $data['addons'] = $addons;
            array_push($orders, $data);
        }

        // dd($orders);
        return view('cart', ['cartData' => $orders]);
    }

    public function removeCart($id) {
        DB::table('project')->where('id', $id)->delete();
        return redirect()->route('cart')
            ->with('success','Cart Removed successfully.');
    }

    public function categoryList() {
        $list = DB::table('category')->get();
        return response()->json($list, 200);
    }

    public function saveReview(Request $request, $pid) {
        $id = Auth::user()->id;
        // dd($request->input('comment'), $pid, $id);
        // ProductReview::create($request->all());
        DB::table('product_reviews')->insert([
            'customer_id' => $id,
            'product_id' => $pid,
            'rating' => $request->input('rating'),
            'message' => $request->input('comment')
        ]);
        return redirect()->route('product.single', $pid)
            ->with('success','Review created successfully.');
    }

    public function search(Request $request) {
        $result = Products::where('product_name', 'like', '%'.$request->searchQuery.'%')->get();
        foreach($result as $product) {
            // LIMIT 0,1
            $imageData = DB::table('product_images')->where('product_id', $product->id)->first();
            if (!empty($imageData)) {
               $product->product_images = $imageData->product_image;
           }
       }
        return view('products-search', ['products' => $result, 'productCount' => count($result)]);
    }

    public function productList($cid) {
        $list = Products::where('category_id', $cid)->get();
        return response()->json($list, 200);
    }
}
