<?php

namespace App\Http\Controllers;


use Session;
use App\Models\Project;
use App\Models\ProjectOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\UploadedFile;
use App\Models\Products;
use App\Models\Category;
use App\Models\ProductQtyPrices;
use App\Models\ProductJobSize;
use App\Models\ProductAddon;
use App\Models\ProjectAddons;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use ImageOptimizer;

// use Intervention\Image\Facades\Image as Image;


class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $viewStep = 1;

    public function index($parentslug, $slug, $id)
    {
        $productDetail = Products::where('slug', $slug)->first();
        $prodid = $productDetail->id;
        // Customization Data
        $api_dataCanvas = array();
        $customizationData = DB::table('design_templates')->where('id', $id)->first();
        $customizationData->data1 = json_decode($customizationData->data1);
        $customizationData->data2 = json_decode($customizationData->data2);

        //  Product Data
        $categoryDetail = Category::where('id', $productDetail['category_id'])->first();
        $productDetail['category'] = $categoryDetail;
        $productDetail['addonstables'] = ProductAddon::where('product_id', $prodid)->get();

        // product - Quantity and Price
        $productsize = ProductJobSize::with('jobQtyPrice')->where('product_id', $prodid)->get();
        // Lanyard Details as Add Ons
        $lanyardStyle = Products::where('category_id', 2)->get();
        $additionalProduct = $lanyardStyle;
        return view('project.template', [
            'customizationData' => $customizationData,
            'productDetail' => $productDetail,
            'productJobSize' => $productsize,
            'additionalProduct' => $additionalProduct
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }

    // public function changeStep(Request $request) {
    //     // return response()->json($request, 200);
    //     $viewStep =
    // }

    public function getProductAddOns($categoryId)
    {

        return response()->json('hello world -> ' . $categoryId);
    }

    public function getTemplateDesigns()
    {
        $product = DB::table('design_templates')->where('category_id', 1)->get();
        $data = array();
        foreach (json_decode($product, true) as $key => $value1) {
            $tempData = $value1;
            if ($tempData['data']) {
                $tempData['data'] = json_decode($tempData['data']);
            }
            array_push($data, $tempData);
        }
        return response()->json($data, 200);

    }

    public function saveTemplates(Request $request)
    {

        // $request->validate([
        //     'file' => 'required|mimes:jpg|max:2048',
        // ]);

        $this->validate($request, ['file' => 'image|mimes:jpeg,png,jpg|max:2048']);
 // $request->validate([
        //     'file' => 'required|mimes:jpg|max:2048',
        // ]);

        $this->validate($request, ['file' => 'image|mimes:jpeg,png,jpg|max:2048']);

        $requestData = $request['data1'];
        $requestData2 = $request['data2'];
        // $templateData = Project::create($request->except('data'));
        $uniqIdQ = microtime(true);
        $environment = env('APP_ENV');
        if ($environment == 'local') {
            $pathFilled = public_path() . '/img/fillTemplates/';
            $path = public_path() . '/img//props/';
            $pathCanvas = public_path() . '/img/templates/';
            $publicPathFilled = 'https://resources.excelidcardsolutions.com/images/fillTemplates/';
            $publicPath = 'https://resources.excelidcardsolutions.com/images/props/';
            $publicPathCanvas = 'https://resources.excelidcardsolutions.com/images/templates/';
        } else if ($environment == 'production') {
            $pathFilled = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/images/products/fillTemplates/';
            $path = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/images/products/props/';
            $pathCanvas = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/images/products/templates/';
            $publicPathFilled = 'https://resources.excelidcardsolutions.com/images/fillTemplates/';
            $publicPath = 'https://resources.excelidcardsolutions.com/images/props/';
            $publicPathCanvas = 'https://resources.excelidcardsolutions.com/images/templates/';
        }

        if (!is_dir($pathFilled)) {
            mkdir($pathFilled);
        }
        if (!is_dir($pathCanvas)) {
            mkdir($pathCanvas);
        }
        if (!is_dir($path)) {
            mkdir($path);
        }

        // managing Image for canvas BG
        if (!empty($request['canvasTemplate1'])) {
            if (str_contains($request['canvasTemplate1'], "data:image")) {
                $image = $request['canvasTemplate1'];
                $image_64 = $image; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = 'bTemplate-' . $uniqIdQ . '.' . $extension;
                $putcontent = file_put_contents($pathCanvas . $imageName, base64_decode($image));
                if ($putcontent) {
                    // $compressThisImg = $pathCanvas . $imageName;
                    // Image::make($compressThisImg)->save($compressThisImg);
                    // $request['canvasTemplate1'] = $publicPathCanvas . $imageName;
                    $request->merge(['canvasTemplate1' => $publicPathCanvas . $imageName]);
                    // array_push($tmpImages, $publicPath . $imageName);

                }
            }
        }
        // managing Image for canvas BG for Back Side
        if (!empty($request['canvasTemplate2'])) {
            if (str_contains($request['canvasTemplate2'], "data:image")) {
                $image = $request['canvasTemplate2'];
                $image_64 = $image; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = 'bTemplate-' . $uniqIdQ . '.' . $extension;
                $putcontent = file_put_contents($pathCanvas . $imageName, base64_decode($image));
                if ($putcontent) {
                    // $compressThisImg = $pathCanvas . $imageName;
                    // Image::make($compressThisImg)->save($compressThisImg);
                    // $request['canvasTemplate2'] = $publicPathCanvas . $imageName;
                    $request->merge(['canvasTemplate2' => $publicPathCanvas . $imageName]);
                    // array_push($tmpImages, $publicPath . $imageName);

                }
            }
        }

        // managing Image for filled Template
        if (!empty($request['fill_template1'])) {
            if (str_contains($request['fill_template1'], "data:image")) {
                $image = $request['fill_template1'];
                $image_64 = $image; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = 'fTemplate-' . $uniqIdQ . '.' . $extension;
                $putcontent = file_put_contents($pathFilled . $imageName, base64_decode($image));
                if ($putcontent) {
                    // $compressThisImg = $pathFilled . $imageName;
                    // Image::make($compressThisImg)->save($compressThisImg);
                    $request->merge(['fill_template1' => $publicPathFilled . $imageName]);
                    // array_push($tmpImages, $publicPath . $imageName);

                }
            }
        }

        // managing Image for filled Template Back Side
        if (!empty($request['fill_template2'])) {
            if (str_contains($request['fill_template2'], "data:image")) {
                $image = $request['fill_template2'];
                $image_64 = $image; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = 'fTemplate-' . $uniqIdQ . '.' . $extension;
                $putcontent = file_put_contents($pathFilled . $imageName, base64_decode($image));
                if ($putcontent) {
                    // $compressThisImg = $pathFilled . $imageName;
                    // Image::make($compressThisImg)->save($compressThisImg);
                    $request->merge(['fill_template2' => $publicPathFilled . $imageName]);
                    // array_push($tmpImages, $publicPath . $imageName);

                }
            }
        }

         // managing Image inside Array from fromt front Side
         if (!empty($request['data1']['images'])) {
            foreach ($request['data1']['images'] as $key => $imageone) {
                if (str_contains($imageone['src'], "data:image")) {
                    // if (base64_decode($imageone['src'], true)) {
                    // is valid
                    $image = $imageone['src'];
                    $image_64 = $image; //your base64 encoded data
                    $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                    $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                    $image = str_replace($replace, '', $image_64);
                    $image = str_replace(' ', '+', $image);
                    $imageName = 'sample-' . microtime(true) . '.' . $extension;
                    $putcontent = file_put_contents($path . $imageName, base64_decode($image));
                    if ($putcontent) {
                        // $compressThisImg = $pathFilled . $imageName;
                        // Image::make($compressThisImg)->save($compressThisImg);
                        $requestData['images'][$key]['src'] = $publicPath . $imageName;
                        // array_push($tmpImages, $publicPath . $imageName);
                    }
                }
            }
        }

        // managing Image inside Array from Back front Side
        if (!empty($request['data2']['images'])) {
            foreach ($request['data2']['images'] as $key => $imageone) {
                if (str_contains($imageone['src'], "data:image")) {
                    // if (base64_decode($imageone['src'], true)) {
                    // is valid
                    $image = $imageone['src'];
                    $image_64 = $image; //your base64 encoded data
                    $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                    $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                    $image = str_replace($replace, '', $image_64);
                    $image = str_replace(' ', '+', $image);
                    $imageName = 'sample-' . microtime(true) . '.' . $extension;
                    $putcontent = file_put_contents($path . $imageName, base64_decode($image));
                    if ($putcontent) {
                        // $compressThisImg = $pathFilled . $imageName;
                        // Image::make($compressThisImg)->save($compressThisImg);
                        $requestData2['images'][$key]['src'] = $publicPath . $imageName;
                        // array_push($tmpImages, $publicPath . $imageName);
                    }
                }
            }
        }

        $request->merge(['data1' => json_encode($requestData)]);
        $request->merge(['data2' => json_encode($requestData2)]);
        Project::create($request->all());
        return response()->json($request->all());
    }

    //update canvas Template Data
    public function updateTemplates(Request $request, $tid)
    {

        // $request->validate([
        //     'file' => 'required|mimes:jpg|max:2048',
        // ]);

        $this->validate($request, ['file' => 'image|mimes:jpeg,png,jpg|max:2048']);

        $requestData = $request['data1'];
        $requestData2 = $request['data2'];
        // $templateData = Project::create($request->except('data'));
        $uniqIdQ = microtime(true);
        $environment = env('APP_ENV');
        if ($environment == 'local') {
            $pathFilled = public_path() . '/img/fillTemplates/';
            $path = public_path() . '/img//props/';
            $pathCanvas = public_path() . '/img/templates/';
            $publicPathFilled = 'https://resources.excelidcardsolutions.com/images/fillTemplates/';
            $publicPath = 'https://resources.excelidcardsolutions.com/images/props/';
            $publicPathCanvas = 'https://resources.excelidcardsolutions.com/images/templates/';
        } else if ($environment == 'production') {
            $pathFilled = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/images/products/fillTemplates/';
            $path = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/images/products/props/';
            $pathCanvas = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/images/products/templates/';
            $publicPathFilled = 'https://resources.excelidcardsolutions.com/images/fillTemplates/';
            $publicPath = 'https://resources.excelidcardsolutions.com/images/props/';
            $publicPathCanvas = 'https://resources.excelidcardsolutions.com/images/templates/';
        }

        if (!is_dir($pathFilled)) {
            mkdir($pathFilled);
        }
        if (!is_dir($pathCanvas)) {
            mkdir($pathCanvas);
        }
        if (!is_dir($path)) {
            mkdir($path);
        }

        // managing Image for canvas BG
        if (!empty($request['canvasTemplate1'])) {
            if (str_contains($request['canvasTemplate1'], "data:image")) {
                $image = $request['canvasTemplate1'];
                $image_64 = $image; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = 'bTemplate-' . $uniqIdQ . '.' . $extension;
                $putcontent = file_put_contents($pathCanvas . $imageName, base64_decode($image));
                if ($putcontent) {
                    // $compressThisImg = $pathCanvas . $imageName;
                    // Image::make($compressThisImg)->save($compressThisImg);
                    // $request['canvasTemplate1'] = $publicPathCanvas . $imageName;
                    $request->merge(['canvasTemplate1' => $publicPathCanvas . $imageName]);
                    // array_push($tmpImages, $publicPath . $imageName);

                }
            }
        }
        // managing Image for canvas BG for Back Side
        if (!empty($request['canvasTemplate2'])) {
            if (str_contains($request['canvasTemplate2'], "data:image")) {
                $image = $request['canvasTemplate2'];
                $image_64 = $image; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = 'bTemplate-' . $uniqIdQ . '.' . $extension;
                $putcontent = file_put_contents($pathCanvas . $imageName, base64_decode($image));
                if ($putcontent) {
                    // $compressThisImg = $pathCanvas . $imageName;
                    // Image::make($compressThisImg)->save($compressThisImg);
                    // $request['canvasTemplate2'] = $publicPathCanvas . $imageName;
                    $request->merge(['canvasTemplate2' => $publicPathCanvas . $imageName]);
                    // array_push($tmpImages, $publicPath . $imageName);

                }
            }
        }

        // managing Image for filled Template
        if (!empty($request['fill_template1'])) {
            if (str_contains($request['fill_template1'], "data:image")) {
                $image = $request['fill_template1'];
                $image_64 = $image; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = 'fTemplate-' . $uniqIdQ . '.' . $extension;
                $putcontent = file_put_contents($pathFilled . $imageName, base64_decode($image));
                if ($putcontent) {
                    // $compressThisImg = $pathFilled . $imageName;
                    // Image::make($compressThisImg)->save($compressThisImg);
                    $request->merge(['fill_template1' => $publicPathFilled . $imageName]);
                    // array_push($tmpImages, $publicPath . $imageName);

                }
            }
        }

        // managing Image for filled Template Back Side
        if (!empty($request['fill_template2'])) {
            if (str_contains($request['fill_template2'], "data:image")) {
                $image = $request['fill_template2'];
                $image_64 = $image; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = 'fTemplate-' . $uniqIdQ . '.' . $extension;
                $putcontent = file_put_contents($pathFilled . $imageName, base64_decode($image));
                if ($putcontent) {
                    // $compressThisImg = $pathFilled . $imageName;
                    // Image::make($compressThisImg)->save($compressThisImg);
                    $request->merge(['fill_template2' => $publicPathFilled . $imageName]);
                    // array_push($tmpImages, $publicPath . $imageName);

                }
            }
        }

         // managing Image inside Array from fromt front Side
         if (!empty($request['data1']['images'])) {
            foreach ($request['data1']['images'] as $key => $imageone) {
                if (str_contains($imageone['src'], "data:image")) {
                    // if (base64_decode($imageone['src'], true)) {
                    // is valid
                    $image = $imageone['src'];
                    $image_64 = $image; //your base64 encoded data
                    $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                    $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                    $image = str_replace($replace, '', $image_64);
                    $image = str_replace(' ', '+', $image);
                    $imageName = 'sample-' . microtime(true) . '.' . $extension;
                    $putcontent = file_put_contents($path . $imageName, base64_decode($image));
                    if ($putcontent) {
                        // $compressThisImg = $pathFilled . $imageName;
                        // Image::make($compressThisImg)->save($compressThisImg);
                        $requestData['images'][$key]['src'] = $publicPath . $imageName;
                        // array_push($tmpImages, $publicPath . $imageName);
                    }
                }
            }
        }

        // managing Image inside Array from Back front Side
        if (!empty($request['data2']['images'])) {
            foreach ($request['data2']['images'] as $key => $imageone) {
                if (str_contains($imageone['src'], "data:image")) {
                    // if (base64_decode($imageone['src'], true)) {
                    // is valid
                    $image = $imageone['src'];
                    $image_64 = $image; //your base64 encoded data
                    $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                    $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                    $image = str_replace($replace, '', $image_64);
                    $image = str_replace(' ', '+', $image);
                    $imageName = 'sample-' . microtime(true) . '.' . $extension;
                    $putcontent = file_put_contents($path . $imageName, base64_decode($image));
                    if ($putcontent) {
                        // $compressThisImg = $pathFilled . $imageName;
                        // Image::make($compressThisImg)->save($compressThisImg);
                        $requestData2['images'][$key]['src'] = $publicPath . $imageName;
                        // array_push($tmpImages, $publicPath . $imageName);
                    }
                }
            }
        }

        $request->merge(['data1' => json_encode($requestData)]);
        $request->merge(['data2' => json_encode($requestData2)]);
        // Project::create($request->all());
        $updateProject = Project::find($tid);
        $updateProject->category_id = $request['category_id'];
        $updateProject->product_id = $request['product_id'];
        $updateProject->data1 = $request['data1'];
        $updateProject->data2 = $request['data2'];
        $updateProject->fill_template1 = $request['fill_template1'];
        $updateProject->fill_template2 = $request['fill_template2'];
        $updateProject->fontName = $request['fontName'];
        $updateProject->fontUrl = $request['fontUrl'];
        $updateProject->canvasType = $request['canvasType'];
        $updateProject->canvasWidth = $request['canvasWidth'];
        $updateProject->canvasHeight = $request['canvasHeight'];
        $updateProject->borderRound = $request['borderRound'];
        $updateProject->canvasTemplate1 = $request['canvasTemplate1'];
        $updateProject->canvasTemplate2 = $request['canvasTemplate2'];
        $updateProject->viewType = $request['viewType'];
        $updateProject->save();
        return response()->json($updateProject, 200);
    }

    public function listFonts()
    {
        $fontList = DB::table('fonts')->get();
        return response()->json($fontList, 200);
    }

    public function browseDesigns($parentslug, $slug)
    {
        $productDetail = Products::where('slug', $slug)->first();
        $pid = $productDetail->id;
        $imageData = DB::table('product_images')->where('product_id', $productDetail->id)->first();
        if (!empty($imageData)) {
           $productDetail->product_images = $imageData->product_image;
        }
        $categoryDetail = DB::table('category')->where('id', $productDetail->category_id)->first();
        $templates = DB::table('design_templates')->where('category_id', $categoryDetail->id)->where('product_id', $pid)->paginate(20);
        // $templatesCount = DB::table('design_templates')->where('category_id', $categoryDetail->id)->get();
        return view('templates', ['templates' => $templates, 'product' => $productDetail, 'category' => $categoryDetail]);
    }

    public function saveGuestData($pid, Request $request)
    {
        $fileZipUrl = '';
        $fileExcelUrl = '';
        $putcontent = '';
        // insertOrIgnore //insert // insertGetId
        $uniqIdQ = $pid;
        $environment = env('APP_ENV');
        if ($environment == 'local') {
            $pathZip = public_path() . '/users/zip/';
            $pathExcel = public_path() . '/users/excel/';
            $publicPathZip = 'https://resources.excelidcardsolutions.com/users/zip/';
            $publicPathExcel = 'https://resources.excelidcardsolutions.com/users/excel/';
        } else if ($environment == 'production') {
            $pathZip = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com//users/zip/';
            $pathExcel = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/users/excel/';
            $publicPathZip = 'https://resources.excelidcardsolutions.com/users/zip/';
            $publicPathExcel = 'https://resources.excelidcardsolutions.com/users//excel/';
        }
        // return response()->json($request->all());

        try {
            // Excel Sheet ->type: "text/csv", // zip->
            $validator = Validator::make($request->all(), [
                'quantity' => 'required',
                'price' => 'required',
                'variable_data' => 'required',
                'job_size' => 'required',
                'project_name' => 'required',
            ]);

            if ( $request->input('image_file_path') &&  $request->input('text_file_path')) {
                $validator1 = Validation::make($request->all(), [
                    'text_file_path' => 'required|mimes:application/csv,application/excel,
                    application/vnd.ms-excel, application/vnd.msexcel,
                    text/csv, text/anytext, text/plain, text/x-c,
                    text/comma-separated-values,
                    inode/x-empty,
                    application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                ]);
            }


            if (!is_dir($pathZip.'/'.$pid)) {
                mkdir($pathZip.'/'.$pid);
            }
            if (!is_dir($pathExcel.'/'.$pid)) {
                mkdir($pathExcel.'/'.$pid);
            }

            if($request->hasFile('image_file_path')){
                $file = $request->file('image_file_path');
                $putcontent = $file->move($pathZip.'/'.$pid, $file->getClientOriginalName());
                $fileZipUrl = $publicPathZip . $pid . '/' . $file->getClientOriginalName();
                DB::table('project')->where('id', $pid)->update([
                    'image_file_path' => $fileZipUrl
                ]);
            }
            if($request->hasFile('text_file_path')){
                $file = $request->file('text_file_path');
                $putcontent = $file->move($pathExcel.'/'.$pid, $file->getClientOriginalName());
                $fileZipExcel = $publicPathExcel . $pid . '/' . $file->getClientOriginalName();
                DB::table('project')->where('id', $pid)->update([
                    'text_file_path' => $fileZipExcel,
                ]);
            }

            $projectOrder = ProjectOrder::find($pid);
            $projectOrder->quantity= $request->input('quantity');
            $projectOrder->price = $request->input('price');
            $projectOrder->variable_data = $request->input('variable_data');
            $projectOrder->job_size = $request->input('job_size');
            $projectOrder->project_name = $request->input('project_name');
            $projectOrder->save();

            $dataAddon = $request->input();
            if (!empty($dataAddon['lanyardStyle'])) {
                $projectaddons = new ProjectAddons();
                $projectaddons->product_id = $projectOrder->product_id;
                $projectaddons->project_id = $pid;
                $projectaddons->addons_id = $dataAddon['lanyardStyle'];
                $projectaddons->addons_name = 'lanyardStyle';
                $projectaddons->save();
            }
            if (!empty($dataAddon['lanyardSize'])) {
                $projectaddons = new ProjectAddons();
                $projectaddons->product_id = $projectOrder->product_id;
                $projectaddons->project_id = $pid;
                $projectaddons->addons_id = $dataAddon['lanyardSize'];
                $projectaddons->addons_name = 'lanyardSize';
                $projectaddons->save();
            }
            if (!empty($dataAddon['lanyardHook'])) {
                $projectaddons = new ProjectAddons();
                $projectaddons->product_id = $projectOrder->product_id;
                $projectaddons->project_id = $pid;
                $projectaddons->addons_id = $dataAddon['lanyardHook'];
                $projectaddons->addons_name = 'lanyardHook';
                $projectaddons->save();
            }
            if (!empty($dataAddon['printingType'])) {
                $projectaddons = new ProjectAddons();
                $projectaddons->product_id = $projectOrder->product_id;
                $projectaddons->project_id = $pid;
                $projectaddons->addons_id = $dataAddon['printingType'];
                $projectaddons->addons_name = 'printingType';
                $projectaddons->save();
            }
            $projectDetails = ProjectOrder::where('id', '=', $pid)->first();
            $projectDetails->data = json_decode($projectDetails->data);
            $projectDetails->data2 = json_decode($projectDetails->data2);
            return response()->json($projectDetails, 200);
        } catch (Exception $e) {
            return response()->json($e);
        }

    }

    public function templateInfo($id)
    {
        try {
            $templateData = DB::table('design_templates')->where('id', $id)->first();
            return response()->json($templateData);
        } catch (Exception $e) {

            return response()->json($e);
        }

    }

    public function projectStatus($projectId) {
        try {
            $templateData = DB::table('project')->where('id', $projectId)->first();
            $templateData->data = json_decode($templateData->data);
            $templateData->data2 = json_decode($templateData->data2);
            $designData = DB::table('design_templates')->where('id', $templateData->design_id)->first();
            $templateData->designData = $designData;
            return response()->json($templateData);
        } catch (Exception $e) {
            return  response()->json($e);
        }
    }

    //  get all Lanyards Name for addson
    public function lanyardsList() {
        try {
            $products = Products::with('lanyardSizes')->where('category_id', 2)->get();

            foreach($products as $product) {
                $imageData = DB::table('product_images')->where('product_id', $product->id)->first();
                if (!empty($imageData)) {
                    $product->product_images = $imageData->product_image;
                }
            }
            return response()->json($products);
        } catch( Exception $e) {
            return  response()->json($e);
        }
    }

    public function lanyardsSelectedSizeList($lanyardId) {
        try {
            $lanyardsSize = DB::table('lanyard_fitting')->where('lanyard_id', $lanyardId)->get();
            return response()->json($lanyardsSize);
        } catch(Exception $e) {
            return  response()->json($e);
        }
    }

    public function lanyardSelectectFitting($lanyardId_fittingId) {
        try {
            $lanyardfitting = explode('_', $lanyardId_fittingId);
            $lanyardId = $lanyardfitting[0 ];
            $fittingId = $lanyardfitting[1];
            $lanyardHooks = DB::table('hooks')->where('lanyard_id', $lanyardId)->get();
            return response()->json($lanyardHooks);
        } catch(Exception $e) {
            return  response()->json($e);
        }
    }

    public function printtypeFitting($quantityId) {
        try {
            $printList = DB::table('printing_type')->where('qty_id', $quantityId)->get();
            return response()->json($printList);
        } catch(Exception $e) {
            return  response()->json($e);
        }
    }

    // Saving the customization Data
    public function saveOrderCustomizationData(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'session_d' => 'required',
                'data' => 'required',
                'product_id' => 'required',
                'design_id' => 'required',
            ]);
            $request->merge(['data' => json_encode($request->input('data'))]);
            $request->merge(['ip_address' =>  $request->ip()]);
            $saveDetails = ProjectOrder::create($request->all());
            $projectDetails = ProjectOrder::where('id', '=', $saveDetails->id)->first();
            $projectDetails->data = json_decode($projectDetails->data);
            $projectDetails->data2 = json_decode($projectDetails->data2);
            return response()->json($projectDetails, 200);
        } catch (Exception $e) {
            return response()->json($e);
        }

    }

    public function saveOrderCustomizationBackData($pid, Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                'data2' => 'required',
            ]);
            $request->merge(['data2' => json_encode($request->input('data2'))]);
            $projectOrder = ProjectOrder::find($pid);
            $projectOrder->data2 = $request->input('data2');
            $projectOrder->save();
            $projectDetails = ProjectOrder::where('id', '=', $pid)->first();
            $projectDetails->data = json_decode($projectDetails->data);
            $projectDetails->data2 = json_decode($projectDetails->data2);
            return response()->json($projectDetails, 200);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }


    // Functions for api Job Size, Qty - Price, adds ons - Total Price.

    public function customDesigns($parentslug, $slug, $uid){
        // upload custom artwork
        $totalPrice = 0;
        // Get Product Details
        $productDetail = Products::where('slug', $slug)->first();
        $pid = $productDetail->id;

        // Getting Images the selected Product
        $imageData = DB::table('product_images')->where('product_id', $productDetail->id)->first();
        if (!empty($imageData)) {
           $productDetail->product_images = $imageData->product_image;
        }

        // Getting Category Detail for the Product
        $categoryDetail = DB::table('category')->where('id', $productDetail->category_id)->first();

        //Getting Sizes for the product
        $jobSizeList = ProductJobSize::with('jobQtyPrice')->where('product_id', $pid)->get();
        // Getting Quantity and price for the product
        $jobQtyList = count($jobSizeList) > 0 ? $jobSizeList[0]->jobQtyPrice : [];

        // Getting Adds on List if Any related to the product
        $addons =  ProductAddon::where('product_id', $pid)->get();
        $totalPrice = count($jobQtyList) > 0 ? $jobQtyList[0]->price : 0;
        $addonLanyardStyleList = [];
        $addonLanyardSizeList = [];
        $addonLanyardHookList = [];
        $addonLanyardStyleId = $pid;
        $addonLanyardSizeId = '';
        // dd(($addons));
        $addonsList = array();
        $addonsList['addonLanyardStyleList'] = [];
        $addonsList['addonLanyardSizeList'] = [];
        $addonsList['addonLanyardHookList'] = [];
        // foreach ($addons as $key => $value) {
        //     switch($value->addon_name) {
        //         case 'lanyard':
        //             $addonLanyardStyleList = Products::where('category_id', 2)->get();
        //             // $addonLanyardStyleList['addonstables'] = json_decode($addonLanyardStyleList['addonstables']);
        //             $addonLanyardStyleId = $addonLanyardStyleList[0]->id;
        //             $addonLanyardSizeList = DB::table('lanyard_fitting')->where('lanyard_id', $addonLanyardStyleId)->get();
        //             $addonLanyardSizeId = $addonLanyardSizeList[0]->id;
        //             $addonsList['addonLanyardStyleList'] = $addonLanyardStyleList;
        //             $addonsList['addonLanyardSizeList'] = $addonLanyardSizeList;

        //             // Lanyard  Hooks
        //             $lanyardSize = DB::table('lanyard_fitting')->where('lanyard_id', $addonLanyardStyleId)->get();
        //             if ( count($lanyardSize) > 0 ){
        //                 $addonLanyardSizeId = $lanyardSize[0]->id;
        //                 $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $addonLanyardStyleId)->get();
        //                 $totalPrice += $addonLanyardHookList[0]->price;
        //                 $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
        //             }
        //             break;
        //         case 'hooks':
        //             // $addonLanyardStyleId = $pid;
        //             $lanyardSize = DB::table('lanyard_fitting')->where('lanyard_id', $addonLanyardStyleId)->get();
        //             if ( count($lanyardSize) > 0 ){
        //                 $addonLanyardSizeId = $lanyardSize[0]->id;
        //                 $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $addonLanyardStyleId)->get();
        //                 $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
        //             }
        //             break;
        //     }
        // }

        return view('customDesignUpload', [
            'product' => $productDetail,
            'category' => $categoryDetail,
            'addonLanyardStyleList' => $addonsList['addonLanyardStyleList'],
            'addonLanyardSizeList' => $addonsList['addonLanyardSizeList'],
            'addonLanyardHookList' => $addonsList['addonLanyardHookList'],
            'jobSizeList' => $jobSizeList,
            'jobQtyList' => $jobQtyList,
            'totalPrice' => $totalPrice,
            'uid' => $uid
        ]);
    }

    public function getQtyPrice($pid, $sizeId) {
        try {
            $jobSizeList = ProductJobSize::with('jobQtyPrice')->where('product_id', $pid)->get();
            // Getting Quantity and price for the product
            return $jobQtyList = count($jobSizeList) > 0 ? $jobSizeList[0]->jobQtyPrice : [];
        } catch(Exception $e) {
            return  response()->json($e);
        }
    }

    public function product_getQuantity($pid, $sizeId) {
        try {
            // ProductJobSize
            // $qtyPrices = ProductQtyPrices::where('product_id', $pid)->where('size_id', $sizeId)->get();
            $jsonData = array();
            $totalPrice = 00.00;
            // ProductJobQtyPrice
            $jobQtyList = ProductQtyPrices::where('product_id', $pid)->where('size_id', $sizeId)->get();

            // Getting Adds on List if Any related to the product
            $addons =  ProductAddon::where('product_id', $pid)->get();
            // dd($jobQtyList);
            $totalPrice = $jobQtyList[0]->price;
            $addonLanyardStyleList = [];
            $addonLanyardSizeList = [];
            $addonLanyardHookList = [];
            $addonLanyardStyleId = $pid;
            $addonLanyardSizeId = '';
            $addonsList = array();
            // $addonsList['addonLanyardStyleList'] = [];
            // $addonsList['addonLanyardSizeList'] = [];
            // $addonsList['addonLanyardHookList'] = [];
            foreach ($addons as $key => $value) {
                switch($value->addon_name) {
                    case 'lanyard':
                        $addonLanyardStyleList = Products::where('category_id', 2)->get();
                        // $addonLanyardStyleList['addonstables'] = json_decode($addonLanyardStyleList['addonstables']);
                        $addonLanyardStyleId = $addonLanyardStyleList[0]->id;
                        $addonLanyardSizeList = DB::table('job_size')->where('product_id', $addonLanyardStyleId)->get();
                        $addonLanyardSizeId = $addonLanyardSizeList[0]->id;
                        $addonsList['addonLanyardStyleList'] = $addonLanyardStyleList;
                        $addonsList['addonLanyardSizeList'] = $addonLanyardSizeList;

                        // Lanyard  Hooks
                        $lanyardSize = DB::table('lanyard_fitting')->where('lanyard_id', $addonLanyardStyleId)->get();
                        if ( count($lanyardSize) > 0 ){
                            $addonLanyardSizeId = $lanyardSize[0]->id;
                            $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $addonLanyardStyleId)->where('qty_id', $jobQtyList[0]->id)->get();
                            $totalPrice += $addonLanyardHookList[0]->price;
                            $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
                        }
                        break;
                    case 'hooks':
                        // $addonLanyardStyleId = $pid;
                        $lanyardSize = DB::table('lanyard_fitting')->where('lanyard_id', $addonLanyardStyleId)->get();
                        if ( count($lanyardSize) > 0 ){
                            $addonLanyardSizeId = $lanyardSize[0]->id;
                            $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $addonLanyardStyleId)->get();
                            $totalPrice += $addonLanyardHookList[0]->price;
                            $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
                        }
                        break;
                    case 'printingType':
                        $printType = DB::table('printing_type')->where('qty_id', $jobQtyList[0]->id)->get();
                        $addonsList['addonPrintingList'] = $printType;
                        break;
                }
            }
            $jsonData['jobQtyList'] = $jobQtyList;
            $jsonData['totalPrice'] = $totalPrice;
            $jsonData['addons'] = $addonsList;
            return response()->json($jsonData, 200);
        } catch( Exception $e) {
            return  response()->json($e);
        }
    }

    public function product_getQuantityPrice($pid, $quantityId) {
        try {
            $jsonData = array();
            $totalPrice = 00.00;
            // ProductJobQtyPrice
            $jobQtyList = ProductQtyPrices::where('product_id', $pid)->where('id', $quantityId)->get();


            // Getting Adds on List if Any related to the product
            $addons =  ProductAddon::where('product_id', $pid)->get();
            // dd($jobQtyList);
            $totalPrice += $jobQtyList[0]->price;
            $addonLanyardStyleList = [];
            $addonLanyardSizeList = [];
            $addonLanyardHookList = [];
            $addonLanyardStyleId = $pid;
            $addonLanyardSizeId = '';
            $addonsList = array();
            // $addonsList['addonLanyardStyleList'] = [];
            // $addonsList['addonLanyardSizeList'] = [];
            // $addonsList['addonLanyardHookList'] = [];
            foreach ($addons as $key => $value) {
                switch($value->addon_name) {
                    case 'printingType':
                        $printType = DB::table('printing_type')->where('qty_id', $quantityId)->get();
                        // dd($printType);
                        if (count($printType) > 0) {
                            $totalPrice += $printType[0]->price;
                        }
                        $addonsList['addonPrintingList'] = $printType;
                        break;
                    case 'lanyard':
                        $addonLanyardStyleList = Products::where('category_id', 2)->get();
                        // $addonLanyardStyleList['addonstables'] = json_decode($addonLanyardStyleList['addonstables']);
                        $addonLanyardStyleId = $addonLanyardStyleList[0]->id;
                        $addonLanyardSizeList = DB::table('job_size')->where('product_id', $addonLanyardStyleId)->get();
                        $addonLanyardSizeId = $addonLanyardSizeList[0]->id;
                        $addonsList['addonLanyardStyleList'] = $addonLanyardStyleList;
                        $addonsList['addonLanyardSizeList'] = $addonLanyardSizeList;

                        // Lanyard  Hooks
                        $lanyardSize = DB::table('job_size')->where('product_id', $addonLanyardStyleId)->get();
                        if ( count($lanyardSize) > 0 ){
                            $addonLanyardSizeId = $lanyardSize[0]->id;
                            $qtyNumber = $jobQtyList[0]->quantity;
                            $lanyardFirstSizeId = $addonLanyardSizeList[0]->id;
                            $lanyardQtyRecord = ProductQtyPrices::where('product_id', $addonLanyardStyleId)->where('size_id', $lanyardFirstSizeId)->get();
                            // dd(gettype($lanyardQtyRecord[3]->quantity));
                            $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $addonLanyardStyleId)->limit(1)->get();
                            $totalPrice += $addonLanyardHookList[0]->price;
                            $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
                        }
                        break;
                    case 'hooks':
                        // $addonLanyardStyleId = $pid;
                        $lanyardSize = DB::table('lanyard_fitting')->where('lanyard_id', $addonLanyardStyleId)->get();
                        if ( count($lanyardSize) > 0 ){
                            $addonLanyardSizeId = $lanyardSize[0]->id;
                            $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $addonLanyardStyleId)->where('qty_id', $quantityId)->get();
                            $totalPrice += $addonLanyardHookList[0]->price;
                            $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
                        }
                        break;
                }
            }
            $jsonData['jobQtyList'] = $jobQtyList;
            $jsonData['totalPrice'] = $totalPrice;
            $jsonData['addons'] = $addonsList;
            return response()->json($jsonData, 200);
        } catch( Exception $e) {
            return  response()->json($e);
        }
    }


    public function product_getLanyardStyleDetail(Request $request, $pid, $quantityId, $lid, $type)  {
        try {
            $jobQtyList = ProductQtyPrices::where('product_id', $pid)->where('id', $quantityId)->get();
            $totalPrice = $jobQtyList[0]->price;

            if ($type == 'styles') {
                // List for Lanyard Sizes - 12M / 16M / 20M
                $addonLanyardSizeList = ProductJobSize::where('product_id', $lid)->get();
                $addonLanyardSizeId = $addonLanyardSizeList[0]->id;
                $addonsList['addonLanyardSizeList'] = $addonLanyardSizeList;

                // Getting Lanyard  Hooks
                if ( count($addonLanyardSizeList) > 0) {
                    $lanyardQtyList = ProductQtyPrices::where('product_id', $lid)->where('size_id', $addonLanyardSizeId)->get();
                    $ClosestQuantity = $this->getClosest($jobQtyList[0]->quantity , $lanyardQtyList);
                    $qtySizeId = '';
                    foreach ($lanyardQtyList as $key => $value) {
                        # code...
                        if ( (int)$ClosestQuantity == (int)$value->quantity ) {
                            $qtySizeId = $value->id;
                        }
                    }
                    $addonLanyardSizePrice = ProductQtyPrices::where('id', $qtySizeId)->first()->price;
                    $totalPrice += $addonLanyardSizePrice;
                    $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $lid)->where('qty_id', $qtySizeId)->get();
                    $totalPrice += $addonLanyardHookList[0]->price;
                    $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
                    $jsonData['addons'] = $addonsList;
                }
            } else if($type == 'sizes') {
                // Hooks
                $addonLanyardSizeId = $request->size;
                // Getting Lanyard  Hooks
                $lanyardQtyList = ProductQtyPrices::where('product_id', $lid)->where('size_id', $addonLanyardSizeId)->get();
                $ClosestQuantity = $this->getClosest($jobQtyList[0]->quantity , $lanyardQtyList);
                $qtySizeId = '';
                foreach ($lanyardQtyList as $key => $value) {
                    # code...
                    if ( (int)$ClosestQuantity == (int)$value->quantity ) {
                        $qtySizeId = $value->id;
                    }
                }
                $addonLanyardSizePrice = ProductQtyPrices::where('id', $qtySizeId)->first()->price;
                $totalPrice += $addonLanyardSizePrice;
                $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $lid)->where('qty_id', $qtySizeId)->get();
                $totalPrice += $addonLanyardHookList[0]->price;
                $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
                $jsonData['addons'] = $addonsList;
            } else if($type == 'hooks') {
                // Total Price
                $addonLanyardSizeId = $request->size;
                $addonLanyardHookId = $request->hook;
                // Getting Lanyard  Hooks
                $lanyardQtyList = ProductQtyPrices::where('product_id', $lid)->where('size_id', $addonLanyardSizeId)->get();
                $addonLanyardHookList = DB::table('hooks')->where('id', $addonLanyardHookId)->first();
                $totalPrice += $addonLanyardHookList->price;
                $addonLanyardSizePrice = ProductQtyPrices::where('id', $addonLanyardHookList->qty_id)->first()->price;
                $totalPrice += $addonLanyardSizePrice;
            }

            $jsonData['totalPrice'] = $totalPrice;
            return response()->json($jsonData, 200);
        } catch (Exception $e) {
            return  response()->json($e);
        }
    }

    public function product_getPrintingPrice($pid, $quantityId, $printId){
        try {
            $jsonData = array();
            $totalPrice = 00.00;
            // ProductJobQtyPrice
            $jobQtyList = ProductQtyPrices::where('product_id', $pid)->where('id', $quantityId)->get();
            $printType = DB::table('printing_type')->where('id', $printId)->first();
            $totalPrice += (float)$printType->price;
            // Getting Adds on List if Any related to the product
            $addons =  ProductAddon::where('product_id', $pid)->get();
            $totalPrice += (float)$jobQtyList[0]->price;
            // dd($totalPrice);
            $addonLanyardStyleList = [];
            $addonLanyardSizeList = [];
            $addonLanyardHookList = [];
            $addonLanyardStyleId = $pid;
            $addonLanyardSizeId = '';
            $addonsList = array();
            foreach ($addons as $key => $value) {
                switch($value->addon_name) {
                    case 'lanyard':
                        $addonLanyardStyleList = Products::where('category_id', 2)->get();
                        $addonLanyardStyleId = $addonLanyardStyleList[0]->id;
                        $addonLanyardSizeList = DB::table('job_size')->where('product_id', $addonLanyardStyleId)->get();
                        $addonLanyardSizeId = $addonLanyardSizeList[0]->id;
                        $addonsList['addonLanyardStyleList'] = $addonLanyardStyleList;
                        $addonsList['addonLanyardSizeList'] = $addonLanyardSizeList;

                        // Lanyard  Hooks
                        $lanyardSize = DB::table('job_size')->where('product_id', $addonLanyardStyleId)->get();
                        if ( count($lanyardSize) > 0 ){
                            $addonLanyardSizeId = $lanyardSize[0]->id;
                            $qtyNumber = $jobQtyList[0]->quantity;
                            $lanyardFirstSizeId = $addonLanyardSizeList[0]->id;
                            $lanyardQtyRecord = ProductQtyPrices::where('product_id', $addonLanyardStyleId)->where('size_id', $lanyardFirstSizeId)->get();
                            // dd(gettype($lanyardQtyRecord[3]->quantity));
                            $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $addonLanyardStyleId)->limit(1)->get();
                            $totalPrice += $addonLanyardHookList[0]->price;
                            $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
                        }
                        break;
                    case 'hooks':
                        // $addonLanyardStyleId = $pid;
                        $lanyardSize = DB::table('lanyard_fitting')->where('lanyard_id', $addonLanyardStyleId)->get();
                        if ( count($lanyardSize) > 0 ){
                            $addonLanyardSizeId = $lanyardSize[0]->id;
                            $addonLanyardHookList = DB::table('hooks')->where('lanyard_id', $addonLanyardStyleId)->where('qty_id', $quantityId)->get();
                            $totalPrice += $addonLanyardHookList[0]->price;
                            $addonsList['addonLanyardHookList'] = $addonLanyardHookList;
                        }
                        break;
                    // case 'printingType':
                    //     $printType = DB::table('printing_type')->where('qty_id', $quantityId)->get();
                    //     $totalPrice += $printType[0]->price;
                    //     $addonsList['addonPrintingList'] = $printType;
                    //     break;
                }
            }
            $jsonData['jobQtyList'] = $jobQtyList;
            $jsonData['totalPrice'] = $totalPrice;
            $jsonData['addons'] = $addonsList;
            return response()->json($jsonData, 200);
        } catch( Exception $e) {
            return  response()->json($e);
        }
    }

    // product_saveCustomDesignArtWork
    public function customDesignsSave($uid, $pSlug, Request $request){
        try {
            $validator = Validator::make($request->all(), [
                    'uploadArtWork' => 'required',
                    'price' => 'required',
                    'project_name' => 'required',
                    'job_size' => 'required',
                    'quantity' => 'required',
                    'lanyardStyle' => 'sometimes|required',
                    'lanyardSize' => 'sometimes|required',
                    'lanyardHook' => 'sometimes|required',
                    'uploadFieldName' => 'sometimes|required',
                    'uploadFieldNameImage' => 'sometimes|required'
            ]);
            $productDetail = Products::where('slug', $pSlug)->first();
            $pid = $productDetail->id;

            $environment = env('APP_ENV');
            if ($environment == 'local') {
                $pathZip = public_path() . '/users/zip/';
                $pathExcel = public_path() . '/users/excel/';
                $publicArt = public_path() . '/users/customArtWork/';
                $publicPathZip = 'https://resources.excelidcardsolutions.com/users/zip/';
                $publicPathExcel = 'https://resources.excelidcardsolutions.com/users/excel/';
                $publicPathArtWork = 'https://resources.excelidcardsolutions.com/users/customArtWork/';

            } else if ($environment == 'production') {
                $pathZip = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/users/zip/';
                $pathExcel = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/users/excel/';
                $publicArt = '/home/wkpmjb478h7x/public_html/resources.excelidcardsolutions.com/users/customArtWork/';
                $publicPathZip = 'https://resources.excelidcardsolutions.com/users/zip/';
                $publicPathExcel = 'https://resources.excelidcardsolutions.com/users//excel/';
                $publicPathArtWork = 'https://resources.excelidcardsolutions.com/users/customArtWork/';
            }
            if($request->hasFile('uploadFieldNameImage')){
                if (!file_exists($pathZip.$uid)) {
                    mkdir($pathZip.$uid);
                }
                $file = $request->file('uploadFieldNameImage');
                $putcontent = $file->move($pathZip.'/'.$uid, $file->getClientOriginalName());
                $fileZipUrl = $publicPathZip . $uid . '/' . $file->getClientOriginalName();
                // $request->merge(['uploadFieldNameImage' => $fileZipUrl]);
                $request->request->add(['image_file_path' => $fileZipUrl]); //add request
            }

            if($request->hasFile('uploadFieldName')){
                if (!file_exists($pathExcel.$uid)) {
                    mkdir($pathExcel.$uid);
                }
                $file = $request->file('uploadFieldName');
                $putcontent = $file->move($pathExcel.$uid, $file->getClientOriginalName());
                $fileZipUrl = $publicPathExcel . $uid . '/' . $file->getClientOriginalName();
                // $request->merge(['uploadFieldName' => $fileZipUrl]);
                $request->request->add(['text_file_path' => $fileZipUrl]); //add request
            }

            if($request->hasFile('uploadArtWork')){
                try{
                    if (!file_exists($publicArt.$uid)) {
                        mkdir($publicArt.$uid, 0777);
                    }
                    $file = $request->file('uploadArtWork');
                    $putcontent = $file->move($publicArt.$uid, $file->getClientOriginalName());
                    $fileZipUrl = $publicPathArtWork . $uid . '/' . $file->getClientOriginalName();
                    // $request->merge(['uploadArtWork' => $fileZipUrl]);
                    $request->request->add(['artwork_file_path' => $fileZipUrl]); //add request
                } catch(Exception $ex) {
                    return response()->json($ex);
                }
            }

            $request->request->add(['data' => '0']); //add request
            $request->request->add(['design_id' => 0]); //add request
            $request->request->add(['product_id' => $pid]); //add request
            $request->request->add(['session_id' => Session::getId()]); //add request
            if (Auth::user()) {
                $request->request->add(['user_id' => Auth::user()->id]); //add request
            } else {
                $request->request->add(['user_id' => 0]); //add request
            }
            if (!empty($request->input('uploadFieldName')) && !empty($request->input('uploadFieldNameImage'))) {
                $request->request->add(['variable_data' => 'Yes']); //add request
            } else {
                $request->request->add(['variable_data' => 'No']); //add request
            }
            $request->request->add(['ip_address' =>  $request->ip()]);
            $saveDetails = ProjectOrder::create($request->all());
            $data = $request->input();

            if (!empty($data['lanyardStyle'])) {
                $projectaddons = new ProjectAddons();
                $projectaddons->product_id = $pid;
                $projectaddons->project_id = $saveDetails->id;
                $projectaddons->addons_id = $data['lanyardStyle'];
                $projectaddons->addons_name = 'lanyardStyle';
                $projectaddons->save();
            }
            if (!empty($data['lanyardSize'])) {
                $projectaddons = new ProjectAddons();
                $projectaddons->product_id = $pid;
                $projectaddons->project_id = $saveDetails->id;
                $projectaddons->addons_id = $data['lanyardSize'];
                $projectaddons->addons_name = 'lanyardSize';
                $projectaddons->save();
            }
            if (!empty($data['lanyardHook'])) {
                $projectaddons = new ProjectAddons();
                $projectaddons->product_id = $pid;
                $projectaddons->project_id = $saveDetails->id;
                $projectaddons->addons_id = $data['lanyardHook'];
                $projectaddons->addons_name = 'lanyardHook';
                $projectaddons->save();
            }
            return redirect()->route('cart')
            ->with('success','');

        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    function getClosest($search, $arr) {
        $closest = null;
        foreach ($arr as $item) {
            // dump(abs((int)$search - $closest), abs((int)($item->quantity) - (int)$search));
           if (abs((int)$search - $closest) >= abs((int)($item->quantity) - (int)$search)) {
              $closest = $item->quantity;
           }
        }
        return $closest;
     }
}
