<?php

namespace App\Http\Controllers;

use App\Mail\OrdersMail;
use App\Mail\OrdersMailConfirmed;
use App\Mail\OrdersMailPaymentConfirmation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\Products;
use App\Models\ProductQtyPrices;
use App\Models\ProductJobSize;
use App\Models\ProductAddon;
use App\Models\ProjectAddons;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('user.dashboard');
    }

    public function orders()
    {
        $orders = array();
        $orders_1 = DB::table('order')->get();
        if(count($orders_1) > 0) {
            foreach (json_decode($orders_1, true) as $key => $value1) {
                $productId = $value1['product_id'];
                $productDetails = Products::where('id', $productId)->first();
                $imageData = DB::table('product_images')->where('product_id', $productId)->first();
                if (!empty($imageData)) {
                    $productDetails->product_images = $imageData->product_image;
                }
                $categoryId = $productDetails->category_id;
                $categoryDetails = DB::table('category')->where('id', $categoryId)->first();
                $userId = $value1['user_id'];
                if (!empty($userId)) {
                    $userDetails = DB::table('users')->where('id', $userId)->first();
                    $data['user_name'] = $userDetails ? $userDetails->name: '';
                    $data['user_email'] = $userDetails ? $userDetails->email : '';
                } else {
                    $data['user_name'] = 'Guest';
                    $data['user_email'] = 'Guest';
                }
                $data['id'] = $value1['id'];
                $data['order_id'] = $value1['order_id'];
                $data['category_name'] = $categoryDetails->category_title;
                $data['product_name'] = $productDetails->product_name;
                $data['design_id'] = $value1['design_id'];
                $data['status'] = $value1['status'];
                $data['order_date'] = $value1['created_at'];
                $data['design_data'] = $value1['data'];
                $data['quantity'] = $value1['quantity'];
                $data['price'] = $value1['price'];
                $data['variable_date'] = $value1['variable_data'];
                $paymentStatus = DB::table('payment')->where('payment_link_reference_id', $value1['payment_reference_id'])->first();
                if ( $paymentStatus ){
                    $data['paymentLink'] = $paymentStatus->paymentLink ? $paymentStatus->paymentLink : '';
                    $data['paymentStatus'] = $paymentStatus->payment_link_status ? $paymentStatus->payment_link_status : '';
                    $data['paymentStatus'] = $paymentStatus->payment_link_status ? $paymentStatus->payment_link_status : '';
                }

                array_push($orders, $data);
            }
        }
        // dd($orders);
        return view('user.order', ['orders' => $orders]);
    }

    public function carts()
    {
        $orders = array();
        if (Auth::check()) {
            $userId = Auth::user()->id;
            $cartData = DB::table('project')->where('user_id', $userId)->where('price',  '!=', 'NULL')->get();
        } else {
            $cartData = DB::table('project')->where('ip_address', request()->ip())->where('price',  '!=', 'NULL')->get();
        }
        foreach (json_decode($cartData, true) as $key => $value1) {
            $productId = $value1['product_id'];
            $productDetails = Products::where('id', $productId)->first();
            $imageData = DB::table('product_images')->where('product_id', $productDetails->id)->first();
             if (!empty($imageData)) {
                $productDetails->product_images = $imageData->product_image;
            }
            $categoryId = $productDetails->category_id;
            $categoryDetails = DB::table('category')->where('id', $categoryId)->first();
            $designData = DB::table('design_templates')->where('id', $value1['design_id'])->first();
            $data['id'] = $value1['id'];
            $data['category_name'] = $categoryDetails->category_title;
            $data['product_name'] = $productDetails->product_name;
            $data['design_id'] = $value1['design_id'];
            $data['design_data'] = $value1['data'];
            $data['quantity'] = $value1['quantity'];
            $data['price'] = $value1['price'];
            $data['variable_date'] = $value1['variable_data'];
            $data['designImage'] = $designData->fill_template1;
            $data['designImage2'] = $designData->fill_template2;
            array_push($orders, $data);
        }
        // dd($orders);
        return view('user.cart', ['cartData' => $orders]);
    }

    public function checkout()
    {

        // Set your secret key. Remember to switch to your live secret key in production.
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        // \Stripe\Stripe::setApiKey('sk_test_51IKMi9Jgdw2bw88H7iwU7rlooTYS8wQcl0ESJJsgeaPRJDnDfeUGxcLkQa4So6J0VHX3ymjUyaOpqH6mp0SWN6Jg00jD5iJi3w');

        // \Stripe\PaymentIntent::create([
        // 'amount' => 1000,
        // 'currency' => 'usd',
        // 'payment_method_types' => ['card'],
        // 'receipt_email' => 'jenny.rosen@example.com',
        // ]);

        $orders = array();
        $totalAmount = 0;
        $userId = Auth::user()->id;
        $userDetails = DB::table('user_details')->where('user_id', $userId)->first();
        $cartData = DB::table('project')->where('user_id', $userId)->where('price',  '!=', 'NULL')->get();
        if (count($cartData) > 0) {
            foreach (json_decode($cartData, true) as $key => $value1) {
                $addons = array();
                $productId = $value1['product_id'];
                $productDetails = Products::where('id', $productId)->first();
                $imageData = DB::table('product_images')->where('product_id', $productDetails->id)->first();
                if (!empty($imageData)) {
                    $productDetails->product_images = $imageData->product_image;
                }
                $categoryId = $productDetails->category_id;
                $categoryDetails = DB::table('category')->where('id', $categoryId)->first();
                // $designData = DB::table('design_templates')->where('id', $value1['design_id'])->first();
                $data['id'] = $value1['id'];
                $data['category_name'] = $categoryDetails->category_title;
                $data['product_name'] = $productDetails->product_name;
                $data['design_id'] = $value1['design_id'];
                $data['design_data'] = $value1['data'];
                $data['quantity'] =  ProductQtyPrices::select('quantity')->where('id', $value1['quantity'])->first()->quantity;
                $data['price'] = $value1['price'];
                $data['variable_data'] = $value1['variable_data'];
                $data['job_size'] = ProductJobSize::select('size_name')->where('id', $value1['job_size'])->first()->size_name;
                $addonsData = ProjectAddons::where('project_id',$value1['id'])->get();
                if (count($addonsData) > 0) {
                    foreach ($addonsData as $key => $value) {
                        switch($value->addons_name){
                            case 'lanyardStyle':
                                $addons['Lanyard Style'] = Products::select('product_name')->where('id', $value->addons_id)->first()->product_name;
                                break;
                            case 'lanyardSize':
                                $addons['Lanyard Size'] = DB::table('job_size')->select('size_name')->where('id', $value->addons_id)->first()->size_name;
                                break;
                            case 'lanyardHook':
                                $addons['Lanyard Hook'] = DB::table('hooks')->select('hooks_name')->where('id', $value->addons_id)->first()->hooks_name;
                                break;
                        }
                    }
                }
                $data['addons'] = $addons;
                $totalAmount = $totalAmount + $value1['price'];
                array_push($orders, $data);
            }
            return view('user.checkout', ['checkoutData' => $orders, 'totalAmount' => $totalAmount, 'userDetails' => $userDetails]);
        } else {
            return redirect()->route('index');
        }
    }

    public function payment(Request $request)
    {
        // Set your secret key. Remember to switch to your live secret key in production.
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        // \Stripe\Stripe::setApiKey('sk_test_51IKMi9Jgdw2bw88H7iwU7rlooTYS8wQcl0ESJJsgeaPRJDnDfeUGxcLkQa4So6J0VHX3ymjUyaOpqH6mp0SWN6Jg00jD5iJi3w');

        // $paymentIntent = \Stripe\PaymentIntent::create([
        // 'amount' => 1000,
        // 'currency' => 'usd',
        // 'payment_method_types' => ['card'],
        // 'receipt_email' => 'jenny.rosen@example.com',
        // ]);
        // $intent = $paymentIntent->client_secret;
        // dd($intent);

/*
 * import checksum generation utility
 * You can get this utility from https://developer.paytm.com/docs/checksum/
 */
        // require_once "PaytmChecksum.php";

        // $paytmParams = array();

        // $paytmParams["body"] = array(
        //     "mid" => "YOUR_MID_HERE",
        //     "linkType" => "GENERIC",
        //     "linkDescription" => "Test Payment",
        //     "linkName" => "Test",
        // );

        // /*
        //  * Generate checksum by parameters we have in body
        //  * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
        //  */
        // $checksum = PaytmChecksum::generateSignature(json_encode($paytmParams["body"], JSON_UNESCAPED_SLASHES), "YOUR_MERCHANT_KEY");

        // $paytmParams["head"] = array(
        //     "tokenType" => "AES",
        //     "signature" => $checksum,
        // );

        // $post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);

        // /* for Staging */
        // $url = "https://securegw-stage.paytm.in/link/create";

        // /* for Production */
        // // $url = "https://securegw.paytm.in/link/create";

        // $ch = curl_init($url);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        // $response = curl_exec($ch);
        // print_r($response);

    }

    public function saveCheckout(Request $request)
    {
        $successPayUrl = env('APP_URL').'/payment-success';
        $endTime = strtotime(date('Y-m-d H:i:s')) + 3000; //3000 = 50 min X 60 sec
        $orderNumber = date("Ymdhis") . rand(1, 200);
        $referenceNumber = uniqid("EXCEL");
        $mailData = array();
        $totalAmount = 0;
        // $orderNumber = '#'.str_pad(10 + 1, 8, "0", STR_PAD_LEFT);
        $userId = Auth::user()->id;
        $userEmail = Auth::user()->email;
        $cartDetails = DB::table('project')->where('user_id', $userId)->where('price',  '!=', 'NULL')->get();

        if (count($cartDetails) > 0) {

            foreach (json_decode($cartDetails, true) as $key => $value1) {
                $addons = array();
                    $orderCheck = DB::table('order')->insert([
                        'order_id' => $orderNumber,
                        'project_name' => $value1['project_name'],
                        'job_size' => $value1['job_size'],
                        'session_id' => $value1['session_id'],
                        'user_id' => $value1['user_id'],
                        'data' => $value1['data'],
                        'data2' => $value1['data2'],
                        'quantity' => $value1['quantity'],
                        'price' => $value1['price'],
                        'product_id' => $value1['product_id'],
                        'image_file_path' => $value1['image_file_path'],
                        'text_file_path' => $value1['text_file_path'],
                        'variable_data' => $value1['variable_data'],
                        'artwork_file_path' => $value1['artwork_file_path'],
                        'design_id' => $value1['design_id'],
                        'ip_address' => $value1['ip_address'],
                        'payment_reference_id' => $referenceNumber
                    ]);
                    if ($orderCheck) {
                        DB::table('project')->where('id', $value1['id'])->delete();
                    }
                $product_id = $value1['product_id'];
                $productDetails = Products::select('product_name', 'category_id')->where('id', $product_id)->first();
                $imageData = DB::table('product_images')->where('product_id', $productDetails->id)->first();
                if (!empty($imageData)) {
                    $productDetails->product_images = $imageData->product_image;
                }
                $productImage = DB::table('product_images')->select('product_image')->where('product_id', $product_id)->first()->product_image;
                $designDetails = DB::table('design_templates')->select('fill_template1', 'fill_template2')->where('id', $value1['design_id'])->first();
                $categoryDetails = DB::table('category')->select('category_title')->where('id', $productDetails->category_id)->first();
                $tmpData = array();
                $tmpData['product_name'] = $productDetails->product_name;
                $tmpData['product_images'] = $productImage;
                // $tmpData['design_template'] = $designDetails->fill_template1;
                // $tmpData['design_template2'] = $designDetails->fill_template2;
                $tmpData['category'] = $categoryDetails->category_title;
                $tmpData['quantity'] = ProductQtyPrices::select('quantity')->where('id', $value1['quantity'])->first()->quantity;
                $tmpData['price'] = $value1['price'];
                $totalAmount = $totalAmount + $value1['price'];
                $tmpData['job_size'] = ProductJobSize::select('size_name')->where('id', $value1['job_size'])->first()->size_name;
                $addonsData = ProjectAddons::where('project_id',$value1['id'])->get();
                if (count($addonsData) > 0) {
                    foreach ($addonsData as $key => $value) {
                        switch($value->addons_name){
                            case 'lanyardStyle':
                                $addons['Lanyard Style'] = Products::select('product_name')->where('id', $value->addons_id)->first()->product_name;
                                break;
                            case 'lanyardSize':
                                $addons['Lanyard Size'] = DB::table('job_size')->select('size_name')->where('id', $value->addons_id)->first()->size_name;
                                break;
                            case 'lanyardHook':
                                $addons['Lanyard Hook'] = DB::table('hooks')->select('hooks_name')->where('id', $value->addons_id)->first()->hooks_name;
                                break;
                        }
                    }
                }
                $tmpData['addons'] = $addons;
                array_push($mailData, $tmpData);

            }
            DB::table('user_details')->updateOrInsert(
                ['user_id' => $userId],
                [
                'company_name' => $request->company_name,
                'country' => $request->country,
                'street_address_1' => $request->street_address_1,
                'street_address_2' => $request->street_address_2,
                'town_city' => $request->town_city,
                'phone_number' => $request->phone_number
            ]);

            //curl request for payment link generation

            $postData = array(
                "amount" => $totalAmount * 100,
                "currency" => "INR",
                "accept_partial" => false,
                "expire_by" => `.$endTime.`,
                "reference_id" => $referenceNumber,
                "description" => "Payment for Order No #" . $orderNumber,
                "customer" => array(
                    "name" => Auth::user()->name,
                    "contact" => $request->phone_number,
                    "email" => Auth::user()->email,
                ),
                "notify" => array(
                    "sms" => true,
                    "email" => true,
                ),
                "reminder_enable" => true,
                "callback_url" => $successPayUrl,
                "callback_method" => "get",
            );

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.razorpay.com/v1/payment_links/',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_HTTPHEADER => array(
                    'Content-type: application/json',
                    'Authorization: Basic cnpwX3Rlc3Rfc0Y4ajZ2ek9yb2pXYUE6R0ZSMWRnT0N4RDRqR052OVlIcWxYS0Fu',
                ),
            ));

            $response = curl_exec($curl);
            $paymentLink = json_decode($response)->short_url;
            $paymentResponse = json_decode($response);
            curl_close($curl);

            DB::table('payment')->insert([
                'paymentLink' => $paymentResponse->short_url,
                'amount' => $paymentResponse->amount/100,
                'link_expiry' => $paymentResponse->expire_by,
                'link_creation' => $paymentResponse->created_at,
                'currency' => $paymentResponse->currency,
                'payment_link_reference_id' => $paymentResponse->reference_id
            ]);
            $url = env('APP_URL') . 'order';

            // $maildata = Mail::to($userEmail)->queue(new OrdersMail(
            //     $orderNumber,
            //     $mailData,
            //     $totalAmount,
            //     $request->all(),
            //     '',
            //     $url
            // ));
            $maildata = Mail::to($userEmail)->queue(new OrdersMail($orderNumber, $mailData, $totalAmount, $request->all(), $paymentLink, $url));
            // dd($userEmail, $response);
            // dd('Hi this is end of function');
            return redirect($paymentResponse->short_url);
            // return redirect()->route('user.order');
        } else {
            return redirect()->route('user.order');
        }
    }

    public function payment_success(Request $request)
    {
        // dd($request->razorpay_payment_id);
        $orderDetails = DB::table('order')->where('payment_reference_id', $request->razorpay_payment_link_reference_id)->get();
        $mailData = array();
        $totalAmount = 0;
        $userEmail = Auth::user()->email;
        $orderNumber = $orderDetails[0]->order_id;
        $orderDate = $orderDetails[0]->created_at;
        foreach (json_decode($orderDetails, true) as $key => $value1) {
            $tmpData = array();
            $addons = array();
            $productDetails = Products::select('product_name', 'category_id')->where('id', $value1['product_id'])->first();
            $imageData = DB::table('product_images')->where('product_id', $productDetails->id)->first();
            if (!empty($imageData)) {
                $productDetails->product_images = $imageData->product_image;
            }
            $productImage = DB::table('product_images')->select('product_image')->where('product_id', $value1['product_id'])->first()->product_image;
            $designDetails = DB::table('design_templates')->select('fill_template1', 'fill_template2')->where('id', $value1['design_id'])->first();
            if ( $designDetails ){
                $tmpData['design_template'] = $designDetails->fill_template1;
                $tmpData['design_template2'] = $designDetails->fill_template2;
            } else {
                $tmpData['design_template'] = '';
                $tmpData['design_template2'] = '';
            }
            $categoryDetails = DB::table('category')->select('category_title')->where('id', $productDetails->category_id)->first();
            $tmpData['product_name'] = $productDetails->product_name;
            $tmpData['product_images'] = $productImage;
            $tmpData['category'] = $categoryDetails->category_title;
            $tmpData['quantity'] = ProductQtyPrices::select('quantity')->where('id', $value1['quantity'])->first()->quantity;
            $tmpData['price'] = $value1['price'];
            $tmpData['job_size'] = ProductJobSize::select('size_name')->where('id', $value1['job_size'])->first()->size_name;
            $addonsData = ProjectAddons::where('project_id',$value1['id'])->get();
            if (count($addonsData) > 0) {
                foreach ($addonsData as $key => $value) {
                    switch($value->addons_name){
                        case 'lanyardStyle':
                            $addons['Lanyard Style'] = Products::select('product_name')->where('id', $value->addons_id)->first()->product_name;
                            break;
                        case 'lanyardSize':
                            $addons['Lanyard Size'] = DB::table('job_size')->select('size_name')->where('id', $value->addons_id)->first()->size_name;
                            break;
                        case 'lanyardHook':
                            $addons['Lanyard Hook'] = DB::table('hooks')->select('hooks_name')->where('id', $value->addons_id)->first()->hooks_name;
                            break;
                    }
                }
            }
            $tmpData['addons'] = $addons;
        $totalAmount = $totalAmount + $value1['price'];
        array_push($mailData, $tmpData);
        };

        $userDetails = DB::table('user_details')->where('user_id', Auth::user()->id)->first();
        DB::table('payment')->where('payment_link_reference_id', $request->razorpay_payment_link_reference_id)
        ->limit(1)
        ->update(array(
            'payment_id' => $request->razorpay_payment_id,
            'payment_link_id' => $request->razorpay_payment_link_id,
            'payment_link_status' => $request->razorpay_payment_link_status,
            'signature' => $request->razorpay_signature
        ));
        // dd($userDetails);
        $maildata = Mail::to($userEmail)->queue(new OrdersMailConfirmed($orderNumber, $mailData, $totalAmount, $userDetails, $orderDate));
        $maildata1 = Mail::to($userEmail)->queue(new OrdersMailPaymentConfirmation($orderNumber, $mailData, $totalAmount, $userDetails, $orderDate , $request->razorpay_payment_link_reference_id));
        return redirect()->route('user.order')
            ->with('success','Order created successfully.');
    }
}
