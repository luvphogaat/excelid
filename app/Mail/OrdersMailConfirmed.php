<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrdersMailConfirmed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $orderNumber;
    public $mailDetails;
    public $totalAmount;
    public $shipDetails;
    public $orderDate;
    public $subject = 'Order Confirmed';
    public function __construct($orderNumber, $mailData, $totalAmount, $shipDetails,  $orderDate)
    {
        //
        // $this->order = $order;
        $this->orderNumber = $orderNumber;
        $this->mailDetails = $mailData;
        $this->totalAmount = $totalAmount;
        $this->shipDetails = $shipDetails;
        $this->orderDate = $orderDate;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@excelidcardsolutions.com', 'EXCEL ID CARD SOLUTIONS')
        ->subject('Order #' . $this->orderNumber .' - Order Confirmation')
        ->bcc('luv.phogaat@gmail.com')
        ->bcc('excelidcardsolution@gmail.com')
                ->markdown('emails.orders.confirmed');
    }
}
