<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrdersMailPaymentConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $orderNumber;
    public $mailDetails;
    public $totalAmount;
    public $shipDetails;
    public $orderDate;
    public $reference_id;
    public $subject = 'Payment Confirmed';
    public function __construct($orderNumber, $mailData, $totalAmount, $shipDetails,  $orderDate, $reference_id)
    {
        //
        // $this->order = $order;
        $this->orderNumber = $orderNumber;
        $this->mailDetails = $mailData;
        $this->totalAmount = $totalAmount;
        $this->shipDetails = $shipDetails;
        $this->orderDate = $orderDate;
        $this->reference_id =$reference_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@excelidcardsolutions.com', 'EXCEL ID CARD SOLUTIONS')
        ->subject('Order #' . $this->orderNumber .' - Payment Confirmation')
        ->bcc('luv.phogaat@gmail.com')
        ->bcc('excelidcardsolution@gmail.com')
            ->markdown('emails.orders.paymentReceived');
    }
}
