<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrdersMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $orderNumber;
    public $mailDetails;
    public $totalAmount;
    public $shipDetails;
    public $orderDate;
    public $paymentLink;
    public $subject = 'Order Pending';
    public $url;
    public function __construct($orderNumber, $mailData, $totalAmount, $shipDetails, $paymentLink, $url)
    {
        //
        // $this->order = $order;
        $this->orderNumber = $orderNumber;
        $this->mailDetails = $mailData;
        $this->totalAmount = $totalAmount;
        $this->shipDetails = $shipDetails;
        $this->orderDate = date('M j, Y');
        $this->paymentLink = $paymentLink;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@excelidcardsolutions.com', 'EXCEL ID CARD SOLUTIONS')
                    ->subject('Order #' . $this->orderNumber)
                    // ->bcc('excelidcardsolution@gmail.com')
                    ->bcc('luv.phogaat@gmail.com')
                    ->markdown('emails.orders.placed');
    }
}
