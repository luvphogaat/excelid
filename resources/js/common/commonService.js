


const allBlocks = ['customizationblock', 'summaryblock'];


const nextBlock = (current = 'customizationblock') => {
    // let jobindex = allBlocks.indexOf('experienceblock');
//    if (is_fresher && jobindex>-1) {

//        allBlocks[jobindex] = 'educationblock';
//        allBlocks.pop();
//    }
   let currentIndex = allBlocks.indexOf(current);
   let nextIndex = currentIndex + 1;
   return allBlocks[nextIndex];
};

const previousBlock = (current = 'customizationblock') => {
   let currentIndex = allBlocks.indexOf(current);
   let previousIndex = currentIndex - 1;
   return allBlocks[previousIndex];
};


const getCurrentBlockFields = (current = 'customizationblock') => {
    let fields;
    switch (current) {
        case 'customizationblock':
            fields = ['customization', 'customizationBack'];
            break;
        // case 'lanyardblock':
        //     fields = ['lanyardField', 'lanyardHook'];
        //     break;
        case 'summaryblock':
            fields = ['summary'];
            break;
    }

    return fields;
};

function getClosestElement(el, value) {
    while (el != null) {
        if (el.matches(value)) {
            break;
        }

        el = el.parentElement || el.parentNode;
    }

    return el;
}


module.exports = {
    nextBlock,
    getCurrentBlockFields,
    getClosestElement,
    allBlocks,
    previousBlock
};