import axios from 'axios';

axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';

const apiClient = axios.create({
    baseURL: 'https://excelidcardsolutions.com/api/',
    withCredentials: false, // This is the default
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
});

const getEvent = url => apiClient.get(url);
const postEvent = (url, data) => apiClient.post(url, data);
const putEvent = (url, data) => apiClient.put(url, data);
const patchEvent = (url, data) => apiClient.patch(url, data);


module.exports = {
    getEvent,
    postEvent,
    putEvent,
    patchEvent
};
