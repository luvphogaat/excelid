import Vue from 'vue';
import Vuex from 'vuex';


import * as customizationblock from './modules/customizationblock.js';
// import * as lanyardblock from './modules/lanyardblock.js';
import * as summaryblock from './modules/summaryblock.js';

import {
    nextBlock,
    getCurrentBlockFields,
    getClosestElement,
    allBlocks,
    previousBlock
} from '../common/commonService';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        customizationblock,
        summaryblock,
        // lanyardblock
    },
    state: {
        activeBlock: 'customizationblock',
        previous: true,
        sessionData: null,
        userData: null,
        totalPrice: 460,
        productDetail: '',
        loader: false
    },
    mutations: {
        CURRENT_NEXTBLOCK(state, payload) {
            state[payload.current].display = false;
            state[payload.next].display = true;
            state.activeBlock = payload.next;
        },
        CURRENT_PREVIOUSBLOCK(state, payload) {
            state[payload.current].display = false;
            state[payload.previous].display = true;
            state.activeBlock = payload.previous;
        },
        CURRENT_BLOCK(state, event) {
            state[event.current].display = false;
        },
        NEXT_BLOCK(state, event) {
            state[event.next].display = true;
        },
        SET_SESSIONDATA(state, value) {
            state.sessionData = value
        },
        SET_USERDATA(state, value) {
            state.userData = value
        },
        LOADER(state, value) {
            state['loader'] = value;
        },
        SET_PREVIOUS(state, value) {
            state['previous'] = value;
        },
        SET_PRODUCTDETAIL(state, value) {
            state['productDetail'] = value;
        },
        SET_ACTIVEBLOCK(state, value) {
            state.activeBlock = value;
        },
    },
    actions: {
        loaderAction({ commit }, event) {
            commit('LOADER', event);
        },
        nextForm({ commit }, event) {
            commit('NEXT_BLOCK', event);
        },
        currentForm({ commit }, event) {
            commit('CURRENT_BLOCK', event);
        },
        setSessionData({commit}, event) {
            commit('SET_SESSIONDATA', event);
        },
        setUserData({commit}, event) {
            commit('SET_USERDATA', event);
        },
        setProductDetail({commit}, event) {
            commit('SET_PRODUCTDETAIL', event);
        },
        setPrevious({ commit }, value) {
            commit('SET_PREVIOUS', value);
        },
        getInitialData({ dispatch, commit, state }, data) {
            let initialData = data['statusData'];
            let blockList = [...allBlocks];
            let customizationData = {
                customization: initialData['data'] || '',
                customizationBack: initialData['data2'] || ''
            };
            if ( initialData['data'] != '' ) {
                dispatch('customizationblock/setFrontSaved', true);
            }
            if (initialData['designData'].viewType == 2  && initialData['data2'] != '' ) {
                dispatch('customizationblock/setBackSaved', true);
                dispatch('customizationblock/setBlockSaved', true);
            }
            if (initialData['designData'].viewType == 1 ) {
                dispatch('customizationblock/setBackSaved', true);
                dispatch('customizationblock/setBlockSaved', true);
            }

            dispatch(`customizationblock/setDump`, { data: customizationData });
            if (initialData['designData'].viewType == 1 ) {
                dispatch('customizationblock/setCustomizationBackData', true);
            }
            for (let i = 0; i < blockList.length; i++) {
                let stoploop = false;
                let fields = getCurrentBlockFields(blockList[i]);
                for (let j = 0; j < fields.length; j++) {
                    if (
                        (state[blockList[i]].data[fields[j]] == '' || state[blockList[i]].data[fields[j]] == null)
                    ) {
                        dispatch(`${blockList[i]}/next`, {
                            active: fields[j]
                        });
                        stoploop = true;
                        break;
                    }
                }
                if (stoploop) {
                    commit('SET_ACTIVEBLOCK', blockList[i]);
                    commit('NEXT_BLOCK', {
                        next: blockList[i]
                    });
                    break;
                } else {
                    commit('CURRENT_BLOCK', {
                        current: blockList[i]
                    });
                }
            }
        },
        next({ dispatch, commit, state }, data) {
            let particularBlock = state[state.activeBlock];
            let fields = getCurrentBlockFields(state.activeBlock);
            let emptyFields = false;
            let field;
            dispatch('setPrevious', true);
            for (let i = 0; i < fields.length; i++) {
                if (
                    (particularBlock.data[fields[i]] == '' || particularBlock.data[fields[i]] == null)
                ) {
                    emptyFields = true;
                    field = fields[i];
                    break;
                }
                 else {
                }

                if (i == fields.length - 1) {
                    if (!particularBlock['blockSaved']) {
                        emptyFields = true;
                        field = fields[i];
                        break;
                    }
                    if (state.activeBlock == 'customizationblock') {
                        let breakloop = false;
                        let currentBlockField = getCurrentBlockFields(state.activeBlock);
                        for (let i = 0; i < currentBlockField; i++) {
                            if (!particularBlock['frontSaved']) {
                                emptyFields = true;
                                field = 'customization';
                                breakloop = true;
                                break;
                            }
                            if (!particularBlock['backSaved']) {
                                emptyFields = true;
                                field = 'customizationBack';
                                breakloop = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (emptyFields && !particularBlock['is_skip']) {
                // dispatch('loaderAction', false);
                dispatch(`${state.activeBlock}/next`, {
                    active: field
                });
                // dispatch(`progressBarIncrement`);
            } else {
                let next = nextBlock(state.activeBlock, false);
                // dispatch('loaderAction', false);
                if (!next) {
                    localStorage.removeItem('projectId');
                    window.location.href = `https://excelidcardsolutions.com/cart`;
                } else {
                    // dispatch('setPrevious', false);
                    //dispatch(`progressblock/INCREASE_PROGRESS`);
                    // dispatch(`progressBarIncrement`);
                    commit('CURRENT_NEXTBLOCK', {
                        current: state.activeBlock,
                        next
                    });
                }
            }
        },
        previous({ dispatch, commit, state }, payload) {
            let fields = getCurrentBlockFields(state.activeBlock);
            let currentBlockActiveField = state[state.activeBlock]['active'];
            let currentFieldIndex = fields.indexOf(currentBlockActiveField);
            if (currentFieldIndex == 0) {
                let previous = previousBlock(state.activeBlock, false);
                let previousFieldList = getCurrentBlockFields(previous);
                commit('CURRENT_PREVIOUSBLOCK', {
                    current: state.activeBlock,
                    previous
                });
                let previousField = (state.activeBlock == 'customizationblock' && state[previous].originalData.viewType == 1)
                    ? 'customization' : previousFieldList[previousFieldList.length - 1];
                dispatch(`${state.activeBlock}/previous`, {
                    active: previousField,
                    disablePrevious: true
                });
            } else {
                if (currentFieldIndex - 1 == 0) {
                    dispatch('setPrevious', false);
                }
                dispatch(`${state.activeBlock}/previous`, {
                    active: fields[currentFieldIndex - 1],
                    disablePrevious: false
                });
                // dispatch(`progressBarDecrement`);
            }
        }
    },
    getters: {
        previous: state => state.previous,
        activeBlock: state => state.activeBlock,
        productDetail: state => state.productDetail
    }
});
