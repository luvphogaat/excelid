export const namespaced = true;

export const state = {
    display: true,
    disablePrevious: true,
    data: {
        lanyardField: '',
        lanyardHook: '',
    },
    lanyardSize: '',
    lanyardSaved: false,
    active: 'lanyardField',
    blockSaved: true,
};

export const mutations = {
    ACTIVE_BLOCK(state, payload) {
        state['active'] = payload.active;
    },
    SET_LANYARDNAME(state,value){
        state.data.lanyardField = value;
    },
    PREVIOUS_FIELD(state, payload) {
        state['active'] = payload.active;
        state['disablePrevious'] = payload.disablePrevious;
    },
    SET_LANYARDSIZE(state, value) {
        state.lanyardSize = value;
    },
    SET_LANYARDHOOKS(state, value) {
        state.data.lanyardHook = value;
    }
};


export const actions = {
    next({ commit }, event) {
        commit('ACTIVE_BLOCK', event);
    },
    setlanyardName({ commit }, event) {
        commit('SET_LANYARDNAME', event);
    },
    setLanyardSize({commit}, event) {
        commit('SET_LANYARDSIZE', event);
    },
    setLanyardHook({commit}, event) {
        commit('SET_LANYARDHOOKS', event);
    },
    previous({ commit }, event) {
        commit('PREVIOUS_FIELD', event);
    }
    
};

export const getters = {
    lanyardData: state => state.data.lanyardField,
    activeBlock: state => state.active,
    showPrevious: state => state.disablePrevious,
    lanyardSize: state => state.lanyardSize,
    lanyardHook: state => state.data.lanyardHook
}
