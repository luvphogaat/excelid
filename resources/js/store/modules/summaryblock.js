export const namespaced = true;

export const state = {
    display: false,
    active: 'summary',
    disablePrevious: true,
    data: {
        quantity: '',
        price: '',
        variableData: ''
    },
    addons: '',
    summarySaved: false,
    blockSaved: false
};

export const mutations = {
    ACTIVE_BLOCK(state, payload) {
        state['active'] = payload.active;
    },
    SET_SUMMARYDATA(state,value){
        state.data.summary = value;
    },
    SET_BLOCKSAVED(state,value){
        state.blockSaved = value;
    },
    SET_ADDONDATA(state, value) {
        state.addons = value;
    }

};


export const actions = {
    next({ commit }, event) {
        commit('ACTIVE_BLOCK', event);
    },
    setSummaryData({ commit }, event) {
        commit('SET_SUMMARYDATA', event);
    },
    setBlockSaved({commit},value){
        commit('SET_BLOCKSAVED',value);
    },
    setAddOns({commit}, event) {
        commit('SET_ADDONDATA', event);
    }
};


export const getters = {
    activeBlock: state => state.active,
    addons: state => state.addons
}

