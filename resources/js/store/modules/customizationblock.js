export const namespaced = true;

export const state = {
    display: true,
    active: 'customization',
    disablePrevious: false,
    originalData: '',
    data: {
        customization: '',
        customizationBack: ''
    },
    original: '',
    templateUrlImg: '',
    frontSaved: false,
    backSaved: false,
    blockSaved: false

};

export const mutations = {
    ACTIVE_BLOCK(state, payload) {
        state['active'] = payload.active;
    },
    SET_CUSTOMIZATIONFRONTDATA(state,value){
        state.data.customization = value;
    },
    SET_CUSTOMIZATIONBACKDATA(state,value){
        state.data.customizationBack = value;
    },
    SET_CUSTOMIZATIONODATA(state,value){
        state.originalData = value;
    },
    SET_BLOCKSAVED(state,value){
        state.blockSaved = value;
    },
    SET_FRONTBLOCKSAVED(state,value){
        state.frontSaved = value;
    },
    SET_BACKBLOCKSAVED(state,value){
        state.backSaved = value;
    },
    PREVIOUS_FIELD(state, payload) {
        state['active'] = payload.active;
        state['disablePrevious'] = payload.disablePrevious;
    },
    SET_PREVIOUS(state, value) {
        state['disablePrevious'] = value;
    },
    SET_DUMP(state, payload) {
        state.data = { ...state.data, ...payload.data };
    },
};


export const actions = {
    previous({ commit }, event) {
        commit('PREVIOUS_FIELD', event);
    },
    setCustomizationFrontData({ commit }, event) {
        commit('SET_CUSTOMIZATIONFRONTDATA', event);
    },
    setCustomizationOData({ commit }, event) {
        commit('SET_CUSTOMIZATIONODATA', event);
    },
    setCustomizationBackData({ commit }, event) {
        commit('SET_CUSTOMIZATIONBACKDATA', event);
    },
    next({ commit }, event) {
        commit('ACTIVE_BLOCK', event);
    },
    setBlockSaved({commit},value){
        commit('SET_BLOCKSAVED',value);
    },
    setFrontSaved({commit},value){
        commit('SET_FRONTBLOCKSAVED',value)
    },
    setBackSaved({commit},value){
        commit('SET_BACKBLOCKSAVED',value)
    },
    setDump({ commit }, event) {
        commit('SET_DUMP', event);
    },
};
export const getters = {
    activeBlock: state => state.active,
    showPrevious: state => state.disablePrevious,
    customizationData: state => state.data.customization,
    customizationOData: state => state.originalData,
    customizationDataBack : state => state.data.customizationBack
}
