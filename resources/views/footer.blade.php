
    <!-- Footer Start -->
    @if(!Request::is('login'))
    <footer class="sigma_footer sigma_footer-dark">

        <!-- Middle Footer -->
        <div class="sigma_footer-middle">
            @if(Request::is('cart'))
            <div class="float-right">
                <img src="{{ asset('img/paymentOption.png') }}" style="width:300px; height: auto; margin-right: 20px" />
            </div>
            @endif
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 footer-widget" style="text-align:center">
                        <img src="{{ asset('img/logo_white.png') }}" class="mb-4" alt="logo" style="width: 155px; height: auto; margin: 0px auto">
                        <div class="">
                            <div class="sigma_contact-info-item">
                                <h6 class="widget-title" style="margin-bottom:0px">Location</h6>
                                <p>2600/5, 1st Floor, Beadon Pura</p>
                                <p>Karol Bagh, Delhi, India - 110005</p>
                            </div>
                            <div class="sigma_contact-info-item">
                                <h6 class="widget-title" style="margin-bottom:0px">Mobile Number</h6>
                                <p>+91-8383006931</p>
                            </div>
                            <div class="sigma_contact-info-item">
                                <h6 class="widget-title" style="margin-bottom:0px">Email</h6>
                                <p>excelidcardsolutions@yahoo.com</p>
                            </div>
                            <div class="sigma_contact-info-item">
                                <ul class="sigma_sm" style="text-align: center;width: 50px;margin: 0px auto;">
                                    <li> <a href="https://www.facebook.com/ExcelIdCardSolutions"> <i
                                                class="fab fa-facebook-f"></i> </a> </li>
                                    <li> <a href="https://www.instagram.com/p/CLgPXD7nyXz/?igshid=spya82794cjk"> <i
                                                class="fab fa-instagram"></i> </a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 d-none d-lg-block footer-widget">
                        <h5 class="widget-title">Top Categories</h5>
                        <ul>
                            <li> <a href="{{ route('products.list', 'plastic_cards_and_accessories') }}">Plastic Cards and Accessories</a> </li>
                            <li> <a href="{{ route('products.list', 'customized_lanyard') }}">Customized Lanyard</a> </li>
                            <li> <a href="{{ route('products.list', 'badges') }}">Button Badges</a> </li>
                            <li> <a href="{{ route('products.list', 'mugs') }}">Mugs</a> </li>
                            <li> <a href="{{ route('products.list', 't_shirt') }}">T-Shirts</a> </li>
                        </ul>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 footer-widget">
                        <h5 class="widget-title">Top Products</h5>
                        <ul>
                            <li> <a href="{{ route('product.single', ['plastic_cards_and_accessories', 'student-id-card-single-sided']) }}">Student Ids</a> </li>
                            {{-- <li> <a href="{{ route('product.single', 5) }}">Visting Cards</a> </li> --}}
                            <li> <a href="{{ route('product.single', ['customized_lanyard', 'flat-lanyard']) }}">Lanyards</a> </li>
                            <li> <a href="{{ route('product.single', ['badges', 'school-badge']) }}">School Badges</a> </li>
                            <li> <a href="{{ route('product.single', ['mugs', 'magic-mug']) }}">Magic Mugs</a> </li>
                        </ul>
                    </div>
                   
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 footer-widget">
                            <h5 class="widget-title">Others</h5>
                            <ul>
                                <li> <a href="{{ route('index') }}">Home</a> </li>
                                <li> <a href="{{ route('contact-us') }}">Contact Us</a> </li>
                                <li> <a href="{{ route('terms-conditions') }}">Terms & Conditions</a></li>
                                <li> <a href="{{ route('return-policy') }}">Return Policy</a></li>
                                <li> <a href="{{ route('privacy-policy') }}">Privacy Policy</a></li>
                            </ul>
                        </div>
                </div>
            </div>
        </div>

        <!-- Footer Bottom -->
        <div class="sigma_footer-bottom">
            <div class="container">
                <div class="sigma_footer-copyright">
                    <p> Copyright © <?php echo date("Y") ?>. All Rights Reserved. </p>
                </div>
            </div>
        </div>

    </footer>
    @endif
    <!-- Footer End -->