<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Order') }}
        </h2>
    </x-slot>
    <!-- Subheader Start -->
    <div class="sigma_subheader primary-bg">

        <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture" />

        <!-- Top Left Wave -->
        <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div>

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>

        <div class="container">
            <div class="sigma_subheader-inner">
                <h1>My Orders</h1>
            </div>
        </div>
    </div>
    <!-- Subheader End -->
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="container">
                    @if (count($orders) > 0)
                    <div class="card p-2">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Order Id</th>
                                    <th>Category Name</th>
                                    <th>Product Name</th>
                                    <th class="text-center">Design Selected</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Payment Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $key => $order)
                                <tr>
                                    <th scope="row">{{ $key + 1 }}</th>
                                    <td>{{ $order['order_id'] }}</td>
                                    <td>{{ $order['category_name'] }}</td>
                                    <td>{{ $order['product_name'] }}</td>
                                   
                                    <td class="text-center">
                                        @if ($order['design_id'] != 0)
                                            Pre-Selected Design
                                        @elseif ($order['design_id'] == 0)
                                            Custom Design
                                        @endif
                                    </td>
                                    <td class="text-center text-light">
                                        @if ($order['status'] == 0) <span class="badge bg-danger">Pending</span>
                                        @elseif ($order['status'] == 1) <span class="badge bg-primary">In Progress</span>
                                        @elseif ($order['status'] == 2) <span class="badge bg-success">Completed</span>
                                        @endif
                                    </td> 
                                    <td class="text-center text-light">
                                    @isset($order['paymentStatus'])
                                       @if($order['paymentStatus'] == 'paid')
                                        <span class="badge bg-success">Paid</span>
                                       @else
                                       <a target=”_blank” href="{{ $order['paymentLink'] }}" class="btn bg-light">Pay Now</a> 
                                       @endif
                                       @endisset
                                    </td> 
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- {{ $orders->links() }} --}}
                    </div>
                    @else
                    <div class="container-fluid text-center">
                        <img src="https://cdn.dribbble.com/users/1168645/screenshots/3152485/no-orders_2x.png?compress=1&resize=400x300" style="width:70%; margin: 0px auto"/>
                        <h3>No Orders</h3>
                        <a class="btn btn-outline-success" href="{{ route('index') }}">GO TO HOMEPAGE</a>
                        <br/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>