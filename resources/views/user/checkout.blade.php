<x-app-layout>
    <!-- Subheader Start -->
    <div class="sigma_subheader primary-bg">

        <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture" />

        <!-- Top Left Wave -->
        <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div>

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>

        <div class="container">
            <div class="sigma_subheader-inner">
                <h1>Checkout</h1>
            </div>
        </div>
    </div>
    <!-- Subheader End -->

    <!-- Checkout Start -->
    <div class="section">
        <div class="container">

            <form method="post" action="{{ route('checkout.save') }}">
                @csrf
                <div class="row">
                    <div class="col-xl-6">

                        {{-- <!-- Login -->
                        @if(!Auth::check())
                            <div class="sigma_notice">
                                <p>Are you a returning customer? <a href="#">Click here to login</a> </p>
                            </div>
                        @endif
                        <div class="sigma_notice-content">
                            <form action="{{ route('login') }}" method="post" >
                                <div class="row">
                                    <div class="col-xl-6 form-group">
                                        <label>Email Address</label>
                                        <input type="text" class="form-control" name="login-email"
                                            placeholder="Email Address" value="">
                                    </div>
                                    <div class="col-xl-6 form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="login-pass" placeholder="Password"
                                            value="">
                                    </div>
                                    <div class="col-12 form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="rememberMe">
                                            <label class="custom-control-label" for="rememberMe">Remember Me</label>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="sigma_btn-custom" name="button">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div> --}}
                        <!-- Coupon Code -->
                        {{-- <div class="sigma_notice">
                            <p>Do you have a coupon code? <a href="#">Click here to apply</a> </p>
                        </div> --}}
                        {{-- <div class="sigma_notice-content">
                            <p>If you have a coupon code, apply it below</p>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Coupon Code">
                                <div class="input-group-append">
                                    <button class="sigma_btn-custom shadow-none btn-sm" type="button">Apply
                                        Code</button>
                                </div>
                            </div>
                        </div> --}}

                        <!-- Buyer Info Start -->
                        <h4>Billing Details</h4>
                        <div class="row">
                            <div class="form-group col-xl-12">
                                <label>Name <span class="text-danger">*</span></label>
                                <input type="text" placeholder="Name" name="name" class="form-control" value="{{ Auth::user()->name }}"
                                    required="" readonly>
                            </div>
                            <div class="form-group col-xl-12">
                                <label>Company Name</label>
                                <input type="text" placeholder="Company Name (Optional)" name="company_name"
                                    class="form-control" value="{{ $userDetails ? $userDetails->company_name : '' }}">
                            </div>
                            <div class="form-group col-xl-12">
                                <label>Country <span class="text-danger">*</span></label>
                                <select class="form-control" name="country" required value="{{ $userDetails ? $userDetails->country : '' }}">
                                    <option value="">Select a Country</option>
                                    <option value="India" selected="$userDetails ? $userDetails->country : ''">India</option>
                                </select>
                            </div>
                            <div class="form-group col-xl-6">
                                <label>Street Address 1 <span class="text-danger">*</span></label>
                                <input type="text" placeholder="Street Address One" name="street_address_1" class="form-control"
                                    value="{{ $userDetails ? $userDetails->street_address_1 : '' }}" required="">
                            </div>
                            <div class="form-group col-xl-6">
                                <label>Street Address 2</label>
                                <input type="text" placeholder="Street Address Two (Optional)" name="street_address_2"
                                    class="form-control" value="{{ $userDetails ? $userDetails->street_address_2 : '' }}">
                            </div>
                            <div class="form-group col-xl-12">
                                <label>Town / City <span class="text-danger">*</span></label>
                                <input type="text" placeholder="Town/City" name="town_city" class="form-control" value="{{ $userDetails ? $userDetails->town_city : '' }}"
                                    required="">
                            </div>
                            <div class="form-group col-xl-6">
                                <label>Phone Number <span class="text-danger">*</span></label>
                                <input type="text" placeholder="Phone Number" name="phone_number" class="form-control" value="{{ $userDetails ? $userDetails->phone_number : '' }}"
                                    required="">
                            </div>
                            <div class="form-group col-xl-6">
                                <label>Email Address <span class="text-danger">*</span></label>
                                <input type="email" placeholder="Email Address" name="email" class="form-control" readonly
                                    value="{{ Auth::user()->email }}" required="">
                            </div>
                        </div>
                        <!-- Buyer Info End -->

                    </div>
                    <div class="col-xl-6 checkout-billing">
                        <!-- Order Details Start -->
                        <table class="sigma_responsive-table">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $checkoutData as $checkout )
                                <tr>
                                    <td data-title="Product">
                                        <div class="sigma_cart-product-wrapper">
                                            <div class="sigma_cart-product-body">
                                                <h6> {{  $checkout['product_name'] }} </h6>
                                                <p>{{ $checkout['category_name'] }}</p>
                                                @if (count($checkout['addons']) > 0)
                                                @foreach ($checkout['addons'] as $key => $addon)
                                                <p>{{ $key }}: {{ $addon }}</p>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <td data-title="Quantity">{{ $checkout['quantity'] }}</td>
                                    <td data-title="Total"> <strong>Rs. {{ $checkout['price'] }}</strong> </td>
                                </tr>
                                @endforeach
                                <tr class="total">
                                    <td>
                                        <h6 class="mb-0">Grand Total</h6>
                                    </td>
                                    <td></td>
                                    <td> <strong>Rs. {{ $totalAmount }}</strong> </td>
                                </tr>
                            </tbody>
                        </table>

                        <button type="submit" class="sigma_btn-custom primary btn-block">Place Order</button> 

                        <!-- Order Details End -->
                        <img src="{{ asset('img/paymentOption.png') }}" style="width:100%; height: auto; margin-top: 20px" />

                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- Checkout End -->

</x-app-layout>