<x-app-layout>
    <x-slot name="logo">
        {{-- <x-jet-authentication-card-logo /> --}}
    </x-slot>
      <!-- Subheader Start -->
  <div class="sigma_subheader primary-bg">

    <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture" />

    <!-- Top Left Wave -->
    <div class="sigma_subheader-shape circles">
      <div class="circle circle-lg circle-1 primary-dark-bg"></div>
      <div class="circle circle-sm circle-2 bg-white"></div>
      <div class="circle circle-md circle-3 secondary-bg"></div>
    </div>

    <!-- Bottom Wave -->
    <div class="sigma_subheader-shape waves">
      <div class="wave"></div>
      <div class="wave"></div>
    </div>

    <div class="container">
      <div class="sigma_subheader-inner">
        <h1>Cart</h1>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Pages</a></li>
            <li class="breadcrumb-item active" aria-current="page">Cart</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
  <!-- Subheader End -->

 <!--Cart Start -->
 <div class="section">
    <div class="container">

      <!-- Cart Table Start -->
      <table class="sigma_responsive-table">
        <thead>
          <tr>
            <th class="remove-item"></th>
            <th>Product</th>
            <th class="text-center">Job Size</th>
            <th>Quantity</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($cartData as $cart)
          {{-- {{ dd($cart) }} --}}
          <tr>
            <td class="remove">
              <button type="button" class="close-btn close-danger remove-from-cart">
                <span></span>
                <span></span>
              </button>
            </td>
            <td data-title="Product">
              <div class="sigma_cart-product-wrapper">
                <img src="{{ $cart['designImage'] }}" alt="prod1">
                <div class="sigma_cart-product-body">
                <h6>{{ $cart['project_name'] }}</h6>
                <p>Product : {{ $cart['product_name'] }} </p>
                <p>Category: {{ $cart['category_name'] }}</p>
                @if (count($cart['addons']) > 0)
                @foreach ($cart['addons'] as $key => $addon)
                <p>{{ $key }}: {{ $addon }}</p>
                @endforeach
                @endif
                </div>
              </div>
            </td>
            <td class="quantity text-center" data-title="Quantity">
                <strong class="text-center">{{ $cart['job_size'] }}</strong>
            </td>
            <td class="quantity" data-title="Quantity">
              <strong class="text-center">{{ $cart['quantity'] }}</strong>
            </td>
            <td data-title="Total"> <strong>Rs. {{ $cart['price'] }}/-</strong> </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <!-- Cart Table End -->

      <!-- Coupon Code Start -->
      {{-- <div class="row">
        <div class="col-lg-5">
          <div class="form-group mb-0">
            <div class="input-group mb-0">
              <input type="text" class="form-control" placeholder="Enter Coupon Code" aria-label="Coupon Code">
              <div class="input-group-append">
                <button class="sigma_btn-custom shadow-none" type="button">Apply</button>
              </div>
            </div>
          </div>
        </div>
      </div> --}}
      <!-- Coupon Code End -->

    </div>
  </div>
  <!-- Cart End -->
  <a class="btn btn-success" href="{{ route('user.checkout') }}">CHECKOUT</a>
</x-app-layout>