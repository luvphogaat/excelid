
    <!-- Footer Start -->
    @if(!Request::is('login'))
    <footer class="sigma_footer sigma_footer-dark">

        <!-- Middle Footer -->
        <div class="sigma_footer-middle">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 footer-widget">
                        <img src="https://via.placeholder.com/155x50" class="mb-4" alt="logo">
                        <p>9000 Regency Parkway, Suite 400 Cary, NC 27518</p>
                        <ul class="sigma_sm">
                            {{-- <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
                            <li> <a href="#"> <i class="fab fa-instagram"></i> </a> </li>
                            <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
                            <li> <a href="#"> <i class="fab fa-linkedin-in"></i> </a> </li> --}}
                            <li> <a target="_blank" href="https://www.facebook.com/ExcelIdCardSolutions"> <i class="fab fa-facebook-f"></i> </a> </li>
                            <li> <a target="_blank" href="https://www.instagram.com/p/CLgPXD7nyXz/?igshid=spya82794cjk"> <i class="fab fa-instagram"></i> </a> </li>
                        </ul>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 footer-widget">
                        <h5 class="widget-title">Information</h5>
                        <ul>
                            <li> <a href="index.html">Home</a> </li>
                            <li> <a href="blog-grid.html">Blog</a> </li>
                            <li> <a href="about-us.html">About Us</a> </li>
                            <li> <a href="shop.html">Shop</a> </li>
                            <li> <a href="contact-us.html">Contact Us</a> </li>
                        </ul>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 footer-widget">
                        <h5 class="widget-title">Others</h5>
                        <ul>
                            <li> <a href="wishlist.html">Wishlist</a> </li>
                            <li> <a href="services.html">Services</a> </li>
                            <li> <a href="product-single.html">Printer</a> </li>
                            <li> <a href="shop.html">Products</a> </li>
                            <li> <a href="blog-grid.html">Blog</a> </li>
                        </ul>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 d-none d-lg-block footer-widget">
                        <h5 class="widget-title">Top Categories</h5>
                        <ul>
                            <li> <a href="#">Printers</a> </li>
                            <li> <a href="#">Print Outs</a> </li>
                            <li> <a href="#">Papers</a> </li>
                            <li> <a href="#">Back to School</a> </li>
                            <li> <a href="#">Pens</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Bottom -->
        <div class="sigma_footer-bottom">
            <div class="container">
                <div class="sigma_footer-copyright">
                    <p> Copyright © 2020. All Rights Reserved. </p>
                </div>
            </div>
        </div>

    </footer>
    @endif
    <!-- Footer End -->

    <!-- Vendor Scripts -->
    <script src="{{ asset('js/plugins/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/plugins/popper.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/imagesloaded.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.zoom.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.inview.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.event.move.js') }}"></script>
    <script src="{{ asset('js/plugins/wow.min.js') }}"></script>
    <script src="{{ asset('js/plugins/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/plugins/slick.min.js') }}"></script>
    <script src="{{ asset('js/plugins/ion.rangeSlider.min.js') }}"></script>

    <!-- Tilakam Scripts -->
    <script src="{{ asset('js/main.js') }}"></script>