<aside class="sigma_aside sigma_aside-left">

    <a class="navbar-brand" href="index"> <img src="{{ asset('img/eids_logo.png') }}" alt="logo"> </a>

    <!-- Menu -->
    <ul>
        @foreach ($menuItems as $item)
                    <li class="menu-item menu-item-has-children">
                        <a href="{{ url($item->link) }}">{{ $item->title }}</a>
                        @if (count($item->submenus) > 0)
                        <ul class="sub-menu">
                            @foreach($item->submenus as $submenu)
                            <li class="menu-item">
                                <a href="{{ url('product/'.$submenu->link)}}">{{ $submenu->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endforeach
    </ul>

</aside>
<div class="sigma_aside-overlay aside-trigger-left"></div>
<!-- Header Start -->
<header
    class="sigma_header header-1 can-sticky custom-header-absolute" style="z-index:999;">
    <!-- Middle Header Start -->
    <div class="sigma_header-middle">
        <div class="container-fluid">
            <nav class="navbar">
                <!-- Logo Start -->
                <div class="sigma_logo-wrapper">
                    <a class="navbar-brand" href="{{ route('index') }}">
                        <img src="{{ asset('img/eids_logo.png') }}" alt="logo" style="margin: 0px auto">
                    </a>
                </div>
                <form style="width:60%" action="{{ route('search.post') }}" method="post">
                    @csrf
                    <div class="row no-gutters align-items-center" style="border: 1px solid grey; border-radius: 20px">
                        <div class="col-auto" style="padding:10px">
                            <i class="fas fa-search h4 text-body"></i>
                        </div>
                        <!--end of col-->
                        <div class="col">
                            <input name="searchQuery" class="form-control form-control-lg form-control-borderless" type="search" placeholder="Search" style="border: none; width: 90%">
                        </div>
                        <!--end of col-->
                    </div>
                </form>
                <!-- Logo End -->
                <!-- Menu -->
                {{-- <ul class="navbar-nav">
                    @foreach ($menuItems as $item)
                    <li class="menu-item menu-item-has-children">
                        <a href="{{ url($item->link) }}">{{ $item->title }}</a>
                        @if (count($item->submenus) > 0)
                        <ul class="sub-menu">
                            @foreach($item->submenus as $submenu)
                            <li class="menu-item">
                                <a href="{{ url('product/'.$submenu->link)}}">{{ $submenu->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endforeach
                </ul> --}}

                <!-- Controls -->
                <div style="display:inline-flex; flex-wrap:wrap; gap:12px;">
                    <a href="{{ route('contact-us') }}"
                    class="sigma_btn-custom btn-sm btn-pill d-none d-lg-block ">
                        <i class="fas fa-user-headset" style="font-size:20px;margin-right:5px"></i><span>Support</span>
                    </a>
                    <a href="{{ route('cart') }}"
                        class="sigma_btn-custom btn-sm btn-pill d-none d-lg-block">
                        <i class="fas fa-shopping-cart"
                            style="font-size:20px;margin-right:5px"></i><span>Cart</span>
                            @if($cartCount > 0)
                                <span class="cart-badge-count">{{$cartCount}}</span>
                            @endif
                    </a>
                    @if (Route::has('login'))
                    @auth
                    {{-- <a href="{{ route('login') }}"
                    class="sigma_btn-custom btn-sm btn-pill d-none d-lg-block
                    {{ (Request::is('contact-us') || Request::is('login') || Request::is('register') || Request::is('dashboard')) ? 'dark' : 'light'  }}">
                    <i class="fas fa-user"></i>&nbsp;<span>{{ Auth::user()->name }}</span>
                    </a> --}}
                    <div class="hidden sm:flex sm:items-center sm:ml-6">
                        <x-jet-dropdown align="right" width="48">
                            <x-slot name="trigger">
                                @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                                <button
                                    class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition duration-150 ease-in-out">
                                    <img class="h-12 w-12 rounded-full object-cover"
                                        src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                                </button>
                                @else
                                <button
                                    class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                                    <div> <i class="fas fa-user"></i>&nbsp;&nbsp;{{ Auth::user()->name }}</div>

                                    <div class="ml-1">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20">
                                            <path fill-rule="evenodd"
                                                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    </div>
                                </button>
                                @endif
                            </x-slot>

                            <x-slot name="content">
                                <!-- Account Management -->
                                <div class="block px-4 py-2 text-xs text-gray-400">
                                    {{ __('Manage Account') }}
                                </div>

                                <x-jet-dropdown-link href="{{ route('profile.show') }}">
                                    {{ __('My Profile') }}
                                </x-jet-dropdown-link>

                                <x-jet-dropdown-link href="{{ route('user.order') }}">
                                    {{ __('My Orders') }}
                                </x-jet-dropdown-link>

                                @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                                <x-jet-dropdown-link href="{{ route('api-tokens.index') }}">
                                    {{ __('API Tokens') }}
                                </x-jet-dropdown-link>
                                @endif

                                <div class="border-t border-gray-100"></div>

                                <!-- Team Management -->
                                @if (Laravel\Jetstream\Jetstream::hasTeamFeatures())
                                <div class="block px-4 py-2 text-xs text-gray-400">
                                    {{ __('Manage Team') }}
                                </div>

                                <!-- Team Settings -->
                                <x-jet-dropdown-link href="{{ route('teams.show', Auth::user()->currentTeam->id) }}">
                                    {{ __('Team Settings') }}
                                </x-jet-dropdown-link>

                                @can('create', Laravel\Jetstream\Jetstream::newTeamModel())
                                <x-jet-dropdown-link href="{{ route('teams.create') }}">
                                    {{ __('Create New Team') }}
                                </x-jet-dropdown-link>
                                @endcan

                                <div class="border-t border-gray-100"></div>

                                <!-- Team Switcher -->
                                <div class="block px-4 py-2 text-xs text-gray-400">
                                    {{ __('Switch Teams') }}
                                </div>

                                @foreach (Auth::user()->allTeams() as $team)
                                <x-jet-switchable-team :team="$team" />
                                @endforeach

                                <div class="border-t border-gray-100"></div>
                                @endif

                                <!-- Authentication -->
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf

                                    <x-jet-dropdown-link href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                    this.closest('form').submit();">
                                        {{ __('Logout') }}
                                    </x-jet-dropdown-link>
                                </form>
                            </x-slot>
                        </x-jet-dropdown>
                    </div>
                    @else
                    <a href="{{ route('login') }}"
                        class="sigma_btn-custom btn-sm btn-pill d-none d-lg-block">
                        <i class="fas fa-user" style="font-size:20px;margin-right:5px"></i><span>Account</span>
                    </a>
                    @endif
                    @endif
                </div>

                <!-- Mobile Toggler -->
                <div class="aside-toggler style-2 aside-trigger-left">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                {{-- @endif --}}
            </nav>


            <nav class="navbar">
                <!-- Logo Start -->
                {{-- <div class="sigma_logo-wrapper">
                    <a class="navbar-brand" href="{{ route('index') }}">
                        <img src="{{ asset('img/eids_logo.png') }}" alt="logo">
                    </a>
                </div> --}}
                <!-- Logo End -->
                <!-- Menu -->
                <ul class="navbar-nav"  style="margin:0px auto;">
                    @foreach ($menuItems as $item)
                    <li class="menu-item menu-item-has-children">
                        <a href="{{ url($item->link) }}">{{ $item->title }}</a>
                        @if (count($item->submenus) > 0)
                        <ul class="sub-menu">
                            @foreach($item->submenus as $submenu)
                            <li class="menu-item">
                                <a href="{{ url('product/'.$submenu->link)}}">{{ $submenu->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>

                <!-- Controls -->

                <!-- Mobile Toggler -->
                {{-- <div class="aside-toggler style-2 aside-trigger-left">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div> --}}
                {{-- @endif --}}
            </nav>
        </div>
    </div>
    <!-- Middle Header End -->

</header>
<!-- Header End -->