<x-main-layout>
    <x-slot name="logo">
        <x-jet-authentication-card-logo />
    </x-slot>
    <!-- Subheader Start -->
    <div class="sigma_subheader dark-overlay primary-overlay bg-cover bg-norepeat"
        style="background-image: url('{{ asset('img/banner/SLIDE '.rand(1,6).'.jpg') }}')">

        {{-- <!-- <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture"> -->

        <!-- Top Left Wave -->
        <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div>

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div> --}}

        <div class="container ">
            <div class="sigma_subheader-inner">
                <h1>{{ $product->product_name }}</h1>
                <h1>Custom Templates</h1>
            </div>
        </div>
    </div>
    <!-- Subheader End -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Products</a></li>
            <li class="breadcrumb-item"><a href="#">{{ $category->category_title }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $product->product_name }}</li>
        </ol>
    </nav>
    <!-- Products Start -->
    <div class="section">
        <div class="container">
            <div class="container custom-upload">
                <h2 class="text-center">Add your Custom Artwork</h2>
                <form action="{{ route('designs.custom.save', [request()->route('parentslug'), $product->slug, $uid]) }}" method="post" id="customDesignForm" enctype="multipart/form-data">
                @csrf
                    <div class="row p-3">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 masonry-item">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 p-3">
                                            <div>
                                                <div class="">
                                                    <label>Upload your Design/Art work</label>
                                                </div>
                                                <button class="custom-file-upload btn"
                                                    onclick="$('#uploadArtWork').trigger('click'); return false;">Upload
                                                    Image</button>
                                            </div>
                                            <div>Supported Format - CDR, AI, INDD, EPS, PDF, JPG, TIF, DOC, DOCX, RTF,
                                                TXT,
                                                ODT,
                                                ZIP, RAR, PNG,</div>
                                            <input type="file" name="uploadArtWork" id="uploadArtWork" required />
                                            @error('uploadArtWork')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                            <br />
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                                    aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 p-3">
                                            <div>
                                                <div class="">
                                                    <label>Upload file containing lists for variable data</label>
                                                </div>
                                                <button class="custom-file-upload btn">Upload List</button>
                                            </div>
                                            <div>Supported Format(Excel Sheet)</div>
                                            <input type="file"
                                                accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                                name="uploadFieldName" id="uploadFieldName" />
                                            <br />
                                            <!-- <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div> -->
                                        </div>
                                        <div class="col-md-12 p-3">
                                            <label>Images - Supported Format(Zip)</label>
                                            <div>
                                                <button class="custom-file-upload btn">Upload Images</button>
                                            </div>
                                            <input type="file" class="btn" accept=".zip,.rar,.7zip"
                                                name="uploadFieldNameImage" id="uploadFieldNameImage" />
                                            <br />
                                            <!-- <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 50%"
                                        aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 masonry-item p-3">

                            <div class="row">
                                <div class="col-md-6">
                                    <label class=""><label>Product</label> - <span
                                            class="uppercase">{{ $product->product_name }}</span> </label>
                                </div>
                                <div class="col-md-6" style="right: 10px; text-transform: uppercase">
                                    <div class="form-group">
                                        <label class=""><b>Price : Rs. <label
                                                    id="totalPrice">{{ sprintf("%.2f",$totalPrice) }}</label></b></label>
                                                    <input type="hidden" name="price" value='{{ sprintf("%.2f",$totalPrice) }}' id="totalPriceInput" />
                                    </div>
                                </div>
                                <div class="col-md-12" v-if="product.category">
                                    <label class=""><label>Category</label> - <span class="uppercase">
                                            {{ $category->category_title }}</span> </label>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Project Name</label>
                                        <input type="text" class="form-control border-grey" required name="project_name" />
                                    </div>
                                    @error('project_name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Job Size</label>
                                        <select class="form-control border-grey" name="job_size"
                                            onchange="getQty({{ $product->id }}, this.value)">
                                            @foreach ($jobSizeList as $jobSize)
                                            <option value="{{ $jobSize->id }}">{{ $jobSize->size_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <select class="form-control border-grey" name="quantity" id="jobSizeQty"
                                            onchange="getQtyPrice({{ $product->id }}, this.value)">
                                            @foreach ($jobQtyList as $jobQty)
                                            <option value="{{ $jobQty->id }}">{{ $jobQty->quantity }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="addonsBlock" class="row">
                            </div>
                            <div id="loaderDiv" class="loading" style="display:block">
                                {{-- <img src="{{ asset('img/ajax-loader.gif') }}" style="margin: 0px auto;width: 20px;" /> --}}
                            </div>
                            <button class="btn btn-block btn-outline-success" id="submitDisabled" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @section('script')
    <script>
        const envVariables = "{{ env('MIX_VUE_APP_ENV_URL') }}";
        window.onload = function () {
            let pid = "{{ $product->id }}";
            let qtyList = @json($jobQtyList);
            if (qtyList.length > 0) {
                let sizeId = qtyList[0].id;
                getQtyPrice(pid, sizeId);
            } else {
                $("#loaderDiv").hide();
            }
        };

        function getQtyPrice(pId, quantityId) {
            $sizeId = document.getElementById('jobSizeQty').value;
            document.getElementById('addonsBlock').innerHTML = '';
            $.ajax({
                type: "GET",
                url: `${envVariables}product/${pId}/size/quantity/${quantityId}/price`,
                beforeSend: function () {
                    $("#loaderDiv").show();
                    $('#submitDisabled').attr('disabled', 'disabled');
                },
                complete: function (data) {
                    $("#loaderDiv").hide();
                    $('#submitDisabled').removeAttr('disabled');

                },
            }).done(function (res) {
                let data = res;
                document.getElementById('totalPrice').innerHTML = parseFloat(data.totalPrice);
                document.getElementById('totalPriceInput').value = parseFloat(data.totalPrice);
                if (data.addons) {
                    for (const [key, value] of Object.entries(data.addons)) {
                        if (key == 'addonPrintingList' && value.length > 0) {
                            generateSelectBoxAndOptions(value, 'PrintingBlock', 'Printing Type', 'printingType', 'printingType', 'printing_type', 'addonsBlock');
                            $("#printingType").bind("change", function () {
                                onChangeprintingType(this.value);
                            });
                        } else if (key == 'addonLanyardStyleList' && value.length > 0) {
                            $('<div/>', {
                                id: 'lanyardSelectionChoice',
                                'class': 'col-md-6'
                            })
                            .append($('<div/>', {
                                    'class': 'form-group'
                                })
                                .append($("<label/>", {
                                    html: 'Do you want Lanyard'
                                }))
                                .append($("<select/>", {
                                    class: "form-control border-grey",
                                    name: 'lanyardSelectionChoice',
                                    id: 'lanyardSelectionChoice'
                                })
                                    .append($('<option />', {html: 'Yes', value: 'yes' , selected: true}))
                                    .append($('<option />', {html: 'No', value: 'no'}))
                                ).attr('onChange', 'onChangeLanyardChoiceSelection(event)')
                            )
                            .appendTo('#addonsBlock');
                            generateSelectBoxAndOptions(value, 'lanyardStyleBlock', 'Lanyard Style', 'lanyardStyle', 'lanyardStyle', 'product_name', 'addonsBlock');
                            $("#lanyardStyle").bind("change", function () {
                                onChangeLanyardStyle(this.value);
                            });
                        } else if (key == 'addonLanyardSizeList' && value.length > 0) {
                            generateSelectBoxAndOptions(value, 'lanyardSizeBlock', 'Lanyard Size', 'lanyardSize', 'lanyardSize', 'size_name', 'addonsBlock');
                            $("#lanyardSize").bind("change", function () {
                                onChangeLanyardSize(this.value);
                            });
                        } else if (key == 'addonLanyardHookList' && value.length > 0) {
                            generateSelectBoxAndOptions(value, 'lanyardHookBlock', 'Lanyard Hooks', 'lanyardHook', 'lanyardHook', 'hooks_name', 'addonsBlock');
                            $("#lanyardHook").bind("change", function () {
                                onChangeLanyardHook(this.value);
                            });
                        }
                    };
                }
            });
        }

        function getQty(pId, sizeId) {
            document.getElementById('jobSizeQty').innerHTML = '';
            document.getElementById('addonsBlock').innerHTML = '';
            $.ajax({
                type: "GET",
                url: `${envVariables}product/${pId}/size/${sizeId}/quantity`,
                beforeSend: function () {
                    $("#loaderDiv").show();
                    $('#submitDisabled').attr('disabled', 'disabled');
                },
                complete: function (data) {
                    $("#loaderDiv").hide();
                    $('#submitDisabled').removeAttr('disabled');
                },
            }).done(function (res) {
                let data = res;
                document.getElementById('totalPrice').innerHTML = parseFloat(data.totalPrice);
                document.getElementById('totalPriceInput').value = parseFloat(data.totalPrice);
                data.jobQtyList.forEach(element => {
                    $('<option/>', {
                        'value': element.id,
                        'html': element.quantity
                    }).appendTo('#jobSizeQty');
                });
                if (data.addons) {
                    for (const [key, value] of Object.entries(data.addons)) {
                        if (key == 'addonLanyardHookList' && value.length > 0) {
                            generateSelectBoxAndOptions(value, 'lanyardHookBlock', 'Lanyard Hooks', 'lanyardHook', 'lanyardHook', 'hooks_name', 'addonsBlock');
                            $("#lanyardHook").bind("change", function () {
                                onChangeLanyardHook(this.value);
                            });
                        }
                    };
                }
            });
        }

        function onChangeprintingType(){
            // if (printingTypeId.type != 'load') {
                let pid = "{{ $product->id }}";
                let quantityId = document.getElementById('jobSizeQty').value;
                $.ajax({
                    type: "GET",
                    url: `${envVariables}product/${pid}/qunatity/${quantityId}/addon/print/${printingTypeId}`,
                    beforeSend: function () {
                        $("#loaderDiv").show();
                        $('#submitDisabled').attr('disabled', 'disabled');
                    },
                    complete: function (data) {
                        $("#loaderDiv").hide();
                        $('#submitDisabled').removeAttr('disabled');
                    },
                }).done(function (res) {
                    let data = res;
                    document.getElementById('totalPrice').innerHTML = parseFloat(data.totalPrice);
                    document.getElementById('totalPriceInput').value = parseFloat(data.totalPrice);
                });
            // }
        }

        function onChangeLanyardStyle(lanId) {
            if (lanId) {
                let pid = "{{ $product->id }}";
                let quantityId = document.getElementById('jobSizeQty').value;
                document.getElementById('lanyardSizeBlock').innerHTML = '';
                document.getElementById('lanyardHookBlock').innerHTML = '';
                document.getElementById('totalPrice').innerHTML = 0.0;
                $.ajax({
                    type: "GET",
                    url: `${envVariables}product/${pid}/quantity/${quantityId}/addon/lanyard/${lanId}/data/styles`,
                    beforeSend: function () {
                        $("#loaderDiv").show();
                    },
                    complete: function (data) {
                        $("#loaderDiv").hide();
                    },
                }).done(function (res) {
                    let data = res;
                    document.getElementById('totalPrice').innerHTML = parseFloat(data.totalPrice);
                    document.getElementById('totalPriceInput').value = parseFloat(data.totalPrice);
                    if (data.addons) {
                        for (const [key, value] of Object.entries(data.addons)) {
                            if (key == 'addonLanyardSizeList') {
                                generateSelectBoxAndOptions(value, '', 'Lanyard Size', 'lanyardSize', 'lanyardSize', 'size_name', 'lanyardSizeBlock');
                                $("#lanyardSize").bind("change", function () {
                                    onChangeLanyardSize(this.value);
                                });
                            } else if (key == 'addonLanyardHookList') {
                                generateSelectBoxAndOptions(value, '', 'Lanyard Hooks', 'lanyardHook', 'lanyardHook', 'hooks_name', 'lanyardHookBlock');
                                $("#lanyardHook").bind("change", function () {
                                    onChangeLanyardHook(this.value);
                                });
                            }
                        };
                    }
                });
            }
        }

        function onChangeLanyardSize(sizeId) {
            if (sizeId) {
                let pid = "{{ $product->id }}";
                let quantityId = document.getElementById('jobSizeQty').value;
                let lanId = document.getElementById('lanyardStyle').value;
                document.getElementById('lanyardHookBlock').innerHTML = '';
                document.getElementById('totalPrice').innerHTML = 0.0;
                $.ajax({
                    type: "GET",
                    url: `${envVariables}product/${pid}/quantity/${quantityId}/addon/lanyard/${lanId}/data/sizes/?size=${sizeId}`,
                    beforeSend: function () {
                        $("#loaderDiv").show();
                    },
                    complete: function (data) {
                        $("#loaderDiv").hide();
                    },
                }).done(function (res) {
                    let data = res;
                    document.getElementById('totalPrice').innerHTML = parseFloat(data.totalPrice);
                    document.getElementById('totalPriceInput').value = parseFloat(data.totalPrice);
                    if (data.addons) {
                        for (const [key, value] of Object.entries(data.addons)) {
                            if (key == 'addonLanyardHookList') {
                                generateSelectBoxAndOptions(value, '', 'Lanyard Hooks', 'lanyardHook', 'lanyardHook', 'hooks_name', 'lanyardHookBlock');
                                $("#lanyardHook").bind("change", function () {
                                    onChangeLanyardHook(this.value);
                                });
                            }
                        };
                    }
                });
            }
        }

        function onChangeLanyardHook(hookId, type = 'addons') {
            if (hookId) {
                let pid = "{{ $product->id }}";
                let quantityId = document.getElementById('jobSizeQty').value;
                if (type == 'addons') {
                    let lanId = document.getElementById('lanyardStyle').value;
                } else {
                    let lanId = "{{ $product->id }}";
                }
                // document.getElementById('lanyardHookBlock').innerHTML = '';
                document.getElementById('totalPrice').innerHTML = 0.0;
                $.ajax({
                    type: "GET",
                    url: `${envVariables}product/${pid}/quantity/${quantityId}/addon/lanyard/${lanId}/data/hooks/?hook=${hookId}`,
                    beforeSend: function () {
                        $("#loaderDiv").show();
                    },
                    complete: function (data) {
                        $("#loaderDiv").hide();
                    },
                }).done(function (res) {
                    let data = res;
                    document.getElementById('totalPrice').innerHTML = parseFloat(data.totalPrice);
                    document.getElementById('totalPriceInput').value = parseFloat(data.totalPrice);
                    // if (data.addons) {
                    //     for (const [key, value] of Object.entries(data.addons)) {
                    //         if (key == 'v') {
                    //             generateSelectBoxAndOptions(value, '', 'Lanyard Hooks', 'lanyardHook', 'lanyardHook', 'hooks_name', 'lanyardHookBlock');
                    //             $("#lanyardHook").bind("change", function () {
                    //                 onChangeLanyardHook(this.value);
                    //             });
                    //         }
                    //     };
                    // }
                });
            }
        }

        function generateSelectBoxAndOptions(value, blockName, labelName, selectName, selectIdName, elementName, appendTo){
            if (blockName != '') {
                ($('<div/>', {
                        id: blockName,
                        'class': 'col-md-6'
                    })
                    .append($('<div/>', {
                            'class': 'form-group'
                        })
                        .append($("<label/>", {
                            html: labelName
                        }))
                        .append($("<select/>", {
                            class: "form-control border-grey",
                            name: selectName,
                            id: selectIdName
                        }))
                    )
                )
                .appendTo('#' + appendTo);
                value.forEach(element => {
                    $('<option/>', {
                        'value': element.id,
                        'html': element[elementName]
                    }).appendTo('#' + selectName);
                });
            } else {
                ($('<div/>', {
                        'class': 'form-group'
                    })
                    .append($("<label/>", {
                        html: labelName
                    }))
                    .append($("<select/>", {
                        class: "form-control border-grey",
                        name: selectName,
                        id: selectIdName
                    }))
                ).appendTo('#' + appendTo);
                value.forEach(element => {
                    $('<option/>', {
                        'value': element.id,
                        'html': element[elementName]
                    }).appendTo('#' + selectName);
                });
            }
        }

        function onChangeLanyardChoiceSelection(event) {
            let value = event.target.value;
            if (value == 'yes') {
                $('#lanyardStyleBlock').show();
                $('#lanyardSizeBlock').show();
                $('#lanyardHookBlock').show();
            } else if (value == 'no'){
                $('#lanyardStyleBlock').hide();
                $('#lanyardSizeBlock').hide();
                $('#lanyardHookBlock').hide();
            }
        }

    </script>
    @endsection
</x-main-layout>
