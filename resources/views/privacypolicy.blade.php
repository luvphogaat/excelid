<x-main-layout>

    <x-slot name="logo">
    </x-slot>
    <!-- Subheader Start -->
    <div class="sigma_subheader primary-bg">

        <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture" />

        <!-- Top Left Wave -->
        <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div>

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>

        <div class="container">
            <div class="sigma_subheader-inner">
                <h1>Privacy Policy</h1>
            </div>
        </div>
    </div>
    <!-- Subheader End -->

    <!--Cart Start -->
    <div class="section">
        <div class="container">
            <div style="font-size:16px;">
                <p>
                    The User hereby consents, expresses and agrees that he has read and fully understands the Privacy
                    Policy of excelidcardsolutions.com. The user further consents that the terms and contents of such
                    Privacy Policy are acceptable to him.
                </p>
                <h6>What we collect: We may collect the following information</h6>
                <ol>
                    <li>Name and job title</li>
                    <li>Contact information including email address</li>
                </ol>
                <br/>
                <h6>REGISTRATION</h6>
                <p>Registration of your Details on the website is mandatory to utilize all or any services on the
                    website whether priced or unpriced.</p>
                <p>The Registration process would involve the revelation of your details and creation of user-id and
                    password. You are entirely responsible for maintaining the confidentiality of your password and
                    account, and for any and all activities which occur under your account or password. You agree to
                    immediately notify us of any unauthorized use of your account, password, or any other breach of
                    security known to you. We will not be liable for any loss that you may incur as a result of someone
                    else using or accessing</p>
                <p>Your password or account, either with or without your knowledge. However, you may be held
                    liable for losses incurred by us or any other party as a result of someone else using or accessing
                    your password or account. You will not use anyone else&#39;s account at any time without the
                    permission of the account holder. You may change your password or profile by following
                    instructions on the site.</p>
            </div>
        </div>
    </div>
</x-main-layout>