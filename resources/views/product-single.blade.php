<x-main-layout>
    <x-slot name="logo">
        <x-jet-authentication-card-logo />
    </x-slot>

    <!-- Subheader Start -->
    <div class="sigma_subheader dark-overlay primary-overlay bg-cover bg-norepeat"
    style="background-image: url('{{ asset('img/banner/SLIDE '.rand(1,6).'.jpg') }}')">

        {{-- <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture"> --}}

        <!-- Top Left Wave -->
        {{-- <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div> --}}

        <!-- Bottom Wave -->
        {{-- <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div> --}}

        <div class="container">
            <div class="sigma_subheader-inner">
                <h1 style="font-size: 50px;">Product Details</h1>
            </div>
        </div>
    </div>
    <!-- Subheader End -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Products</a></li>
            <li class="breadcrumb-item"><a href="#">{{ $categoryDetail->category_title }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $product->product_name }}</li>
        </ol>
    </nav>
    <!-- Product Content Start -->
    <div class="section" style="">
        <div class="container-fluid p-5">
            <div class="row p-10">
                <div class="col-md-6">
                     @if (count($product->product_images) > 0)
                    {{-- <div id="thumbnail-container" class="thumbnail-container_class">
                        @foreach( $product->product_images as $key => $productImage )
                            <img class="thumbnail @if ($key == 0 ) focused @endif" src="{{ url($productImage->product_image) }}" style="margin-bottom: 15px"/>
                        @endforeach
                    </div> --}}
                    <div class="sigma_product-single-thumb" style="margin: 0px auto">
                        <img src="{{ url($product->product_images[0]->product_image) }}" style="border-radius: 2px; width: 100%;max-width: 70%;height: auto; margin: 0px auto" />
                    </div>
                    @else
                    <!-- <div class="sigma_product-single-thumb float-right" style="right: 100px">
                        {{-- <img src="{{ asset('img/no-img.png') }}" style="width:500px; height: 420px;" alt="No Product Image"> --}}
                    </div> -->
                    {{-- <div id="thumbnail-container" class="thumbnail-container_class">
                        <img class="thumbnail focused" src="{{ asset('img/no-img.png') }}" style="margin-bottom: 15px"/>
                    </div> --}}
                    <div class="sigma_product-single-thumb" style="margin: 0px auto">
                        <img src="{{ asset('img/no-img.png') }}" style="border-radius: 15px; width: 100%;max-width: 700px;height: auto; margin: 0px auto" />
                    </div>
                    @endif
                </div>
                <div class="col-md-5 card">

                    <div class="sigma_product-single-content card-body">

                        <h4 class="entry-title"> {{ $product->product_name }} </h4>

                        @if (count($product->priceQty) > 0)
                        <div class="sigma_product-price">
                            <span>&#x20B9;&nbsp;{{$product->priceQty[0]->price }}</span>
                            <span>&#x20B9;&nbsp;{{($product->priceQty[0]->price + 150)}} </span>
                            &nbsp;<div>(QTY:  {{ $product->priceQty[0]->quantity }} )</div>
                        </div>
                        @endif

                        <div class="sigma_rating-wrapper">
                            <div class="sigma_rating">
                                @for($i=1; $i<=5; $i++ )
                                    @if ($rating >= $i)
                                        <i class="fa fa-star active"></i>
                                    @else
                                        <i class="fa fa-star"></i>
                                    @endif
                                @endfor
                            </div>
                            @if ($reviewCount > 0)
                            <span>{{ $reviewCount }} Reviews</span>
                            @endif
                        </div>

                        <p><strong>Availablity: 
                            @if ($product->status == 'ACTIVE')
                                <span>In Stock</span>
                            @else
                                <span>Not In Stock</span>
                            @endif
                        </strong> </p>

                        {{-- <p class="sigma_product-excerpt">{!! $product->product_desc !!}</p> --}}
                        <div class="col-md-12 masonry-item">
                            <h3 class="text-left">GET STARTED</h3>
                            <div class="row masonry">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 masonry-item">
                                <a href="{{  route('designs.custom', [request()->route('parentslug'), $product->slug, $uid]) }}" class="btn_custom">
                                    <div class="btn_custom_title-text upload_design ">
                                        <!-- <img src="https://www.printstop.co.in/themes/printstop/images/browse.png" /> -->
                                        <i class="fa fa-upload image-icon pull-left text-white float-left" aria-hidden="true"></i>
                                        <h4 class="text-white">Upload your own Design</h4>
                                        <i class="fa fa-angle-right pull-right text-white float-right"></i>
                                    </div>
                                </a>
                            </div>
                                @if ($designCount > 0)
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 masonry-item" style="margin-top: 20px;">
                                    <a href="{{  route('designs.list', [request()->route('parentslug'), $product->slug]) }}" class="btn_custom">
                                        <div class="btn_custom_title-text browse_design">
                                            <!-- <img src="https://www.printstop.co.in/themes/printstop/images/browse.png" /> -->
                                            <i class="fa fa-search image-icon pull-left text-white float-left" aria-hidden="true"></i>
                                            <h4 class="text-white">Browse our Designs</h4>
                                            <i class="fa fa-angle-right pull-right text-white float-right"></i>
                                        </div>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                        {{-- <form class="sigma_product-atc-form">
                            <div class="qty-outter">
                                <a href="{{ route('design.option', $product->id) }}" class="sigma_btn-custom secondary btn-pill">Select Now</a> --}}
                                {{-- <div class="qty-inner">
                                    <h6>Qty:</h6>
                                    <div class="qty">
                                        <span class="qty-subtract"><i class="fa fa-minus"></i></span>
                                        <input type="text" name="qty" value="1">
                                        <span class="qty-add"><i class="fa fa-plus"></i></span>
                                    </div>
                                </div> --}}
                            {{-- </div>

                        </form> --}}

                        <!-- Post Meta Start -->
                        <div class="sigma_post-single-meta">
                            {{-- <div class="sigma_post-single-meta-item sigma_post-share">
                                <h6>Share</h6>
                                <ul class="sigma_sm">
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-linkedin-in"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div> --}}
                            <div class="sigma_post-single-meta-item">
                                <div class="sigma_product-controls">
                                    <a href="#" data-toggle="tooltip" title="Compare"> <i class="far fa-signal-4"></i>
                                    </a>
                                    <a href="#" ata-toggle="tooltip" title="Wishlist"> <i class="far fa-heart"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Post Meta End -->

                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- Product Content End -->

    <!-- Additional Information Start -->
    <div class="section pt-0">
        <div class="container p-5">
            <div class="sigma_product-additional-info">

                <ul class="nav" id="bordered-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="tab-product-desc-tab" data-toggle="pill" href="#tab-product-desc"
                            role="tab" aria-controls="tab-product-desc" aria-selected="true">Description</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab-product-info-tab" data-toggle="pill" href="#tab-product-info"
                            role="tab" aria-controls="tab-product-info" aria-selected="false">Additional Information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab-product-reviews-tab" data-toggle="pill" href="#tab-product-reviews"
                            role="tab" aria-controls="tab-product-reviews" aria-selected="false">Reviews </a>
                    </li>
                </ul>

                <div class="tab-content" id="bordered-tabContent">
                    <div class="tab-pane fade show active" id="tab-product-desc" role="tabpanel"
                        aria-labelledby="tab-product-desc-tab">
                        <h4>Description</h4>
                        {!! $product->product_desc !!}
                    </div>
                    <div class="tab-pane fade" id="tab-product-info" role="tabpanel"
                        aria-labelledby="tab-product-info-tab">
                        <h4>Additional Information</h4>

                        {{-- <table>
                            <thead>
                                <tr>
                                    <th scope="col">Attributes</th>
                                    <th scope="col">Values</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td> <strong>Color</strong> </td>
                                    <td>blue, red, yellow, green</td>
                                </tr>
                                <tr>
                                    <td> <strong>Material</strong> </td>
                                    <td>wood, plastic, stainless steel</td>
                                </tr>
                            </tbody>
                        </table> --}}
                    </div>
                    <div class="tab-pane fade" id="tab-product-reviews" role="tabpanel"
                        aria-labelledby="tab-product-reviews-tab">
                        <h4>Leave a Review</h4>

                        <div class="sigma_rating-wrapper">
                            <div class="sigma_rating" id="ratingByCustomer">
                                <i class="fa fa-star" data-rating="1"></i>
                                <i class="fa fa-star" data-rating="2"></i>
                                <i class="fa fa-star" data-rating="3"></i>
                                <i class="fa fa-star" data-rating="4"></i>
                                <i class="fa fa-star" data-rating="5"></i>
                            </div>
                            <span>Your Review</span>
                        </div>

                        <!-- Review Form start -->
                        <div class="comment-form">
                            <form method="post" action="{{ route('post.review', $product->id) }}">
                                @csrf
                                <input type="hidden" value="5" name="rating" id="ratingByCustomerValue"/>
                                <div class="row">
                                    {{-- <div class="col-md-6 form-group">
                                        <input type="text" class="form-control" placeholder="Full Name" name="name"
                                            value="">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="email" class="form-control" placeholder="Email Address"
                                            name="email" value="">
                                    </div> --}}
                                    <!-- <div class="col-md-12 form-group">
                                        <input id="input-1" name="rate" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="" data-size="xs">
                                    </div> -->
                                    <div class="col-md-12 form-group">
                                        <textarea class="form-control" placeholder="Type your comment..." name="comment"
                                            rows="7"></textarea>
                                    </div>
                                </div>

                                @if (Auth::check())
                                    <button type="submit" class="sigma_btn-custom secondary btn-pill" name="button">Post
                                        Review
                                    </button>
                                @else
                                    <a type="button" class="sigma_btn-custom secondary btn-pill" name="button" href="{{ route('login') }}">
                                        Login to Add Review
                                    </a>
                                @endif
                            </form>
                        </div>
                        <!-- Review Form End -->

                        <!-- Reviews Start -->
                        <div class="comments-list">
                            <ul>
                                @foreach($reviews as $review)
                                <li class="comment-item">
                                    @if (!empty($review->profile_photo_path))
                                        <img src="{{ $review->profile_photo_path }}" alt="comment author">
                                    @else
                                        <img src="{{ asset('img/profile.png') }}" alt="comment author">
                                    @endif
                                    <div class="comment-body">
                                        @if(!empty($review->users[0]))
                                            <h5>{{ $review->users[0]->name }}</h5>
                                        @else
                                            <h5>Anonymous User</h5>
                                        @endif
                                        <div class="sigma_rating">
                                            @for($i=1; $i<=5; $i++ )
                                                @if ($review->rating >= $i)
                                                    <i class="fa fa-star active"></i>
                                                @else
                                                    <i class="fa fa-star"></i>
                                                @endif
                                            @endfor
                                        </div>

                                        <p>{{ $review->message }}</p>
                                        {{-- <a href="#" class="btn-link"> Reply <i class="far fa-reply"></i> </a> --}}
                                        <span><i class="far fa-clock"></i> 
                                            {{-- January 13 2020 --}}
                                            @if (!empty($review->created_at))
                                                {{ $review->created_at->format('d/m/Y') }}
                                            @endif
                                        </span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                            {{ $reviews->links() }}
                        </div>
                        <!-- Reviews End -->

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Additional Information End -->

    <!-- Related Products start -->
    @if (count($relatedProducts) > 0)
    <div class="section pt-0">
        <div class="container-fluid p-5">

            <div class="section-title text-center">
                <h4 class="title">Related Products</h4>
                {{-- <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis
                    ac lectus.</p> --}}
            </div>

            <div class="sigma_related-posts">

                <!-- Products Post Start -->
                <div class="sigma_related-slider">

                    <!-- Product Start -->

                    @foreach ($relatedProducts as $relatedProduct)
                    <div class="sigma_product">
                        {{-- <div class="sigma_product-badge sigma_badge-featured">
                            <i class="fa fa-star"></i>
                        </div> --}}
                        <div class="sigma_product-thumb">
                            <a href="{{ route('product.single', [request()->route('parentslug'), $relatedProduct->slug]) }}">
                                @if (!empty($relatedProduct->product_images))
                                    <img src="{{ $relatedProduct->product_images }}" style="width:80%; height: auto;margin:0px auto" alt="Product Image">
                                @else
                                    <img src="{{ asset('img/no-img.png') }}" style="width:auto; 80%: auto;margin:0px auto" alt="No Product Image">
                                @endif
                            </a>
                        </div>
                        <div class="sigma_product-body">
                            <h5 class="sigma_product-title"> <a href="{{ route('product.single', [request()->route('parentslug'), $relatedProduct->slug]) }}">{{$relatedProduct->product_name}}</a> </h5>
                            <div class="sigma_product-price">
                            <span>&#x20B9;&nbsp;{{ $relatedProduct->price }}</span>
                            </div>
                            <a href="{{ route('product.single', [request()->route('parentslug'), $relatedProduct->slug]) }}" class="sigma_btn-custom btn-sm dark btn-pill">Select Now</a>

                        </div>
                    </div>
                    @endforeach
                    <!-- Product End -->

                    <!-- Product Start -->
                {{-- <div class="sigma_product">
                    <div class="sigma_product-thumb">
                        <a href="product-single.html"><img src="https://via.placeholder.com/270x260"
                                alt="product"></a>
                    </div>
                    <div class="sigma_product-body">
                        <h5 class="sigma_product-title"> <a href="product-single.html">Package Design</a> </h5>
                        <div class="sigma_product-price">
                            <span>19$</span>
                        </div>
                        <a href="product-single.html" class="sigma_btn-custom btn-sm dark btn-pill">Buy Now</a>

                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="sigma_product">
                    <div class="sigma_product-badge sigma_badge-sale">
                        25% Off
                    </div>
                    <div class="sigma_product-thumb">
                        <a href="product-single.html"><img src="https://via.placeholder.com/270x260"
                                alt="product"></a>
                    </div>
                    <div class="sigma_product-body">
                        <h5 class="sigma_product-title"> <a href="product-single.html">Cover Van</a> </h5>
                        <div class="sigma_product-price">
                            <span>19$</span>
                            <span>29$</span>
                        </div>
                        <a href="product-single.html" class="sigma_btn-custom btn-sm dark btn-pill">Buy Now</a>

                    </div>
                </div>
                <!-- Product End -->

                <!-- Product Start -->
                <div class="sigma_product">
                    <div class="sigma_product-badge sigma_badge-featured">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="sigma_product-thumb">
                        <a href="product-single.html"><img src="https://via.placeholder.com/270x260"
                                alt="product"></a>
                    </div>
                    <div class="sigma_product-body">
                        <h5 class="sigma_product-title"> <a href="product-single.html">Book Cover</a> </h5>
                        <div class="sigma_product-price">
                            <span>29$</span>
                        </div>
                        <a href="product-single.html" class="sigma_btn-custom btn-sm dark btn-pill">Buy Now</a>

                    </div>
                </div> --}}
                    <!-- Product End -->

                </div>
                <!-- Products End -->

            </div>

        </div>
    </div>
    @endif
    <!-- Related Products End -->

    <!-- Clients Start -->
    {{-- <div class="section pt-0">
        <div class="container">
            <div class="row no-gutters">

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

            </div>
        </div>
    </div> --}}
    <!-- Clients End -->
    @section('script')
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <script>
    $(document).ready(function(){
        // $("#thumbnail-container img").click(function () {
        //     $("#thumbnail-container img").css("border", "1px solid #ccc");
        //     var src = $(this).attr("src");
        //     $(".product-view img").attr("src", src);
        //     $(".sigma_product-single-zoom .zoomImg").attr("src", src);
        //     $(this).css("border", "#fbb20f 2px solid");

        // });
        $('#ratingByCustomer > i').click(function() {
            $(this).addClass('active').siblings().removeClass('active');
            // $(this).parent().attr('data-rating', $(this).data('rating'));
            var stars = $(this).parent().children('#ratingByCustomer  i');
            var onStar = parseInt($(this).data('rating'), 10); // The star currently selected
            for (i = 0; i < stars.length; i++) {
                $(stars[i]).removeClass('active');
            }
            for (i = 0; i < onStar; i++) {
                $(stars[i]).addClass('active');
            }
            $('#ratingByCustomerValue').val(onStar);
        });
    });
    </script>
    @endsection
</x-main-layout>