<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Excelidcardsolutions</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/animate.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/slick-theme.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/ion.rangeSlider.min.css') }}">
    
    
        <!-- Icon Fonts -->
        <link rel="stylesheet" href="{{ asset('fonts/flaticon/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/font-awesome.min.css') }}">
        <!-- Tilakam Style sheet -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/icons/favicon.jpg') }}">

        @livewireStyles

        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.js" defer></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            {{-- @livewire('navigation-dropdown') --}}
                @include('user.header')
            <!-- Page Heading -->
            {{-- <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header> --}}

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>

        @stack('modals')

        @livewireScripts
          <!--Start of Tawk.to Script-->
          <!-- <script defer type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/6034b5659c4f165d47c61bf5/1ev6u9755';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script> -->
        <!--End of Tawk.to Script-->
    </body>
</html>
