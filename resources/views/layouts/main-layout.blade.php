<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('baseHref')
        <title>Excelidcardsolutions</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        @yield('style')
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/animate.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/slick-theme.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/ion.rangeSlider.min.css') }}">
    
    
        <!-- Icon Fonts -->
        <link rel="stylesheet" href="{{ asset('fonts/flaticon/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins/font-awesome.min.css') }}">
        <!-- Tilakam Style sheet -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
        <!-- Favicon -->
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/icons/favicon.jpg') }}">

        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.js" defer></script>
    </head>
    <body class="font-sans antialiased">
        @if(!Request::is('*/designs/customize/*'))
            @if (Auth::check())
                @include('user.header')
            @else
                @include('header')
            @endif
        @endif
        {{ $slot }}
        @if(!Request::is('*/designs/customize/*'))
            @include('../footer')
        @endif

        <!-- Vendor Scripts -->
        <script src="{{ asset('js/plugins/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('js/plugins/popper.min.js') }}"></script>
        <script src="{{ asset('js/plugins/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/plugins/imagesloaded.min.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery.countdown.min.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery.zoom.min.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery.inview.min.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery.event.move.js') }}"></script>
        <script src="{{ asset('js/plugins/wow.min.js') }}"></script>
        <script src="{{ asset('js/plugins/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('js/plugins/slick.min.js') }}"></script>
        <script src="{{ asset('js/plugins/ion.rangeSlider.min.js') }}"></script>

        <!--Start of Tawk.to Script-->
        <!-- <script defer type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/6034b5659c4f165d47c61bf5/1ev6u9755';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script> -->
        <!--End of Tawk.to Script-->
        <!-- Tilakam Scripts -->
        <script src="{{ asset('js/main.js') }}"></script>
        @yield('script')
        <!-- {{-- <div class="btn-group-fab" role="group" aria-label="FAB Menu">
            <div>
                <button type="button" class="btn btn-main btn-primary has-tooltip" data-placement="left" title="Menu">
                    <i class="fa fa-bars"></i> 
                </button>
                <button type="button" class="btn btn-sub custom-btn-whatsapp has-tooltip" data-placement="left" title="Fullscreen">
                    <i class="fab fa-whatsapp"></i>
                </button>
                <button type="button" class="btn btn-sub custom-btn-mail has-tooltip" data-placement="left" title="Save"> 
                    <i class="fas fa-envelope-open-text"></i>
                </button>
                <button type="button" class="btn btn-sub custom-btn-headset has-tooltip" data-placement="left"
                    title="Download"> <i class="fa fa-headset"></i> 
                </button>
            </div>
        </div> --}} -->
          <!-- {{-- <script>
              $(function() {
                $('.btn-group-fab').on('click', '.btn', function() {
                    $('.btn-group-fab').toggleClass('active');
                });
                $('has-tooltip').tooltip();
                });
          </script> --}} -->
    </body>
</html>
