<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'ExcelIDSolutions')
<img src="https://resources.excelidcardsolutions.com/images/eids_logo.png" class="logo" alt="ExcelIDSolutions Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
