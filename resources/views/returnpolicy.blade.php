<x-main-layout>

    <x-slot name="logo">
    </x-slot>
    <!-- Subheader Start -->
    <div class="sigma_subheader primary-bg">

        <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture" />

        <!-- Top Left Wave -->
        <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div>

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>

        <div class="container">
            <div class="sigma_subheader-inner">
                <h1>Return and Refund Policy</h1>
            </div>
        </div>
    </div>
    <!-- Subheader End -->

    <!--Cart Start -->
    <div class="section">
        <div class="container">
            <div style="font-size:16px;">
                <p>
                    Thanks for shopping with excelidcardsolutions. We appreciate the fact that you like to buy the stuff
                    we build. We also want to make sure you have a rewarding experience while you’re exploring. Evaluating
                    and purchasing our products.
                </p>
                <p>
                        As with any shopping experience there are terms and conditions that apply to transactions at our
                        company. We’ll be as brief as our attorneys will allow. The main thing to remember is that by
                        placing an  order or making a purchase from us, you agree to the terms along with our privacy policy.
                </p>
                <p>
                    If for any reason, you are not completely satisfied with any good service that we provide, don’t
                    hesitate to contact us and we will discuss any of the issues you are going through with our product.
                </p>
            </div>
        </div>
    </div>
</x-main-layout>