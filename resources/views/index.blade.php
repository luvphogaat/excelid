    {{-- @extends('layouts.guest')
    @section('title', 'Excel Id Card Solutions')
    @section('content') --}}
    <x-main-layout>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>
        <!-- Banner Start -->
        <div class="sigma_banner banner-2">

            <div class="sigma_banner-slider">

                <!-- Banner Item Start -->
                <div class="sigma_banner-slider-inner dot-pattern bg-center bg-top bg-norepeat"
                    style="background-image: url('img/banner/SLIDE 1.jpg');">
                    <div class="sigma_banner-text">
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <div class="mb-0 section-title">
                                        <h6 class="subtitle">About Us</h6>
                                        <h1 class="text-white title">Let Us Help You With All Of Your Printing Needs
                                        </h1>
                                        <p class="text-white">You design We Create, Customize Your Imagination</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Banner Item End -->

                <!-- Banner Item Start -->
                <div class="sigma_banner-slider-inner dot-pattern bg-center bg-top bg-norepeat"
                    style="background-image: url('img/banner/SLIDE 2.jpg');">
                    <div class="sigma_banner-text">
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <div class="mb-0 section-title">
                                        <h6 class="subtitle">About Us</h6>
                                        <h1 class="text-white title">Let Us Help You With All Of Your Printing Needs
                                        </h1>
                                        <p class="text-white">You design We Create, Customize Your Imagination</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Banner Item End -->

                <!-- Banner Item Start -->
                <div class="sigma_banner-slider-inner dot-pattern bg-center bg-top bg-norepeat"
                    style="background-image: url('img/banner/SLIDE 3.jpg');">
                    <div class="sigma_banner-text">
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <div class="mb-0 section-title">
                                        <h6 class="subtitle">About Us</h6>
                                        <h1 class="text-white title">Let Us Help You With All Of Your Printing Needs
                                        </h1>
                                        <p class="text-white">You design We Create, Customize Your Imagination</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Banner Item End -->

                <!-- Banner Item Start -->
                <div class="sigma_banner-slider-inner dot-pattern bg-center bg-top bg-norepeat"
                    style="background-image: url('img/banner/SLIDE 4.jpg');">
                    <div class="sigma_banner-text">
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <div class="mb-0 section-title">
                                        <h6 class="subtitle">About Us</h6>
                                        <h1 class="text-white title">Let Us Help You With All Of Your Printing Needs
                                        </h1>
                                        <p class="text-white">You design We Create, Customize Your Imagination</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Banner Item End -->

                <!-- Banner Item Start -->
                <div class="sigma_banner-slider-inner dot-pattern bg-center bg-top bg-norepeat"
                    style="background-image: url('img/banner/SLIDE 5.jpg');">
                    <div class="sigma_banner-text">
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <div class="mb-0 section-title">
                                        <h6 class="subtitle">About Us</h6>
                                        <h1 class="text-white title">Let Us Help You With All Of Your Printing Needs
                                        </h1>
                                        <p class="text-white">You design We Create, Customize Your Imagination</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Banner Item End -->

                <!-- Banner Item Start -->
                <div class="sigma_banner-slider-inner dot-pattern bg-center bg-top bg-norepeat"
                    style="background-image: url('img/banner/SLIDE 6.jpg');">
                    <div class="sigma_banner-text">
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <div class="mb-0 section-title">
                                        <h6 class="subtitle">About Us</h6>
                                        <h1 class="text-white title">Let Us Help You With All Of Your Printing Needs
                                        </h1>
                                        <p class="text-white">You design We Create, Customize Your Imagination</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Banner Item End -->

            </div>

        </div>
        <!-- Banner End -->
        {{-- {{ Request::ip() }} --}}
        <!-- About Start -->
        <section class="section">
            <div class="container">
                <div class="row align-items-center">

                    <div class="col-lg-1">
                        <div class="sigma_dots dots-2 primary d-none d-lg-block"></div>
                    </div>

                    <div class="col-lg-5 mb-lg-30">
                        <div class="sigma_about-content">
                            <div class="section-title text-left">
                                <h4 class="title">Be Your Own Boss </h4>
                                <span class="small-text custom-primary">Start with your chosen design</span>
                            </div>
                            <p>Excel Id Card Solutions- EIDS</p>
                            <p>EIDS is a company formed in 2017 by the efforts of Mrs Manpreet Kaur (MCA, Mphil). The
                                company
                                aimed at manufacturing of ID Cards with digital printed (Customised Lanyards) as per the
                                customer
                                requirement. We also provide solutions for customised Printed Mugs, Sashes, Tshirts, Cap
                                ets for the
                                Schools, Institutes , corporates as well.</p>
                            <p>The owner of the company has 12 years of Teaching experience in reputed Institutes of
                                Graduate (BBA,
                                BCA) and Post Graduation (MCA, MBA), so well known with the mistakes that occur in ID
                                Cards which
                                lead to misleading Information. The aim of the company is to provide best quality
                                products at
                                reasonable prices.</p>
                            <p>The slogan of company is &quot; YOU DESIGN, WE CREATE. CUSTOMIZE UR IMAGINATION&quot;.
                            </p>
                            <div>
                                <p>YOU CAN REACH US</p>
                                <p>@9899980987</p>
                                <p>@8383006931</p>
                                <p>Excelidcardsolutions@gmail.com</p>
                            </div>
                        </div>
                    </div>

                    <div class="offset-lg-1 col-lg-5 position-relative">
                        <div class="sigma_dots dots-1 secondary d-none d-lg-block"></div>
                        {{-- <img src="https://via.placeholder.com/470x583" alt="about"> --}}
                        <iframe width="470" height="500"
                            src="https://www.youtube.com/embed/Tbdq_7LSIKU?loop=1&autoplay=1&mute=1" frameborder="0"
                            allowfullscreen></iframe>
                    </div>



                </div>
            </div>
        </section>
        <!-- About End -->

        <!-- Icons Start -->
        <div class="section section-padding pt-0">
            <div class="container">
                <div class="row">

                    <div class="col-xl-3 col-lg-3 col-sm-6">
                        <div class="sigma_icon-block icon-block-5">
                            <div class="icon-wrapper">
                                <img src="{{ asset('img/icons/info/1.png') }}" alt="info">
                            </div>
                            <div class="sigma_icon-block-content">
                                <h5>Highly Quality Printing</h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-sm-6">
                        <div class="sigma_icon-block icon-block-5">
                            <div class="icon-wrapper">
                                <img src="{{ asset('img/icons/info/2.png') }}" alt="info">
                            </div>
                            <div class="sigma_icon-block-content">
                                <h5>Pixel Perfect Printing</h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-sm-6">
                        <div class="sigma_icon-block icon-block-5">
                            <div class="icon-wrapper">
                                <img src="{{ asset('img/icons/info/3.png') }}" alt="info">
                            </div>
                            <div class="sigma_icon-block-content">
                                <h5>Printing on Daily Demand</h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-sm-6">
                        <div class="sigma_icon-block icon-block-5">
                            <div class="icon-wrapper">
                                <img src="{{ asset('img/icons/info/4.png') }}" alt="info">
                            </div>
                            <div class="sigma_icon-block-content">
                                <h5>Competitive Printing Prices</h5>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Icons End -->

        <!-- Services Start -->
        <div class="section section-padding pt-0 service-section">

            <div class="container">
                <div class="section-title flex-title text-left">
                    <h4 class="title">Our Services</h4>
                    <div class="sigma_arrows">
                        <i class="fa fa-arrow-left slick-arrow slider-prev"></i>
                        <i class="fa fa-arrow-right slick-arrow slider-next"></i>
                    </div>
                </div>

                <div class="sigma_service-slider-2">

                    <div class="slide-item">
                        <div class="row">

                            <div class="col-lg-6">
                                <a href="#" class="sigma_service style-3">
                                    <div class="sigma_service-thumb">
                                        <img src="{{ asset('img/services/1.jpeg') }}" alt="service"
                                            style="width:500px; height:250px;">
                                    </div>
                                    <div class="sigma_service-body">
                                        <h5>Designing & Printing Solutions</h5>
                                    </div>
                                </a>

                                <a href="#" class="sigma_service style-3">
                                    <div class="sigma_service-thumb">
                                        <img src="{{ asset('img/services/2.jpeg') }}" alt="service"
                                            style="width:500px; height:250px;">
                                    </div>
                                    <div class="sigma_service-body">
                                        <h5>High Quality Products</h5>
                                    </div>
                                </a>
                            </div>

                            <div class="col-lg-6">
                                <a href="#" class="sigma_service style-3">
                                    <div class="sigma_service-thumb">
                                        <img src="{{ asset('img/services/8.jpg') }}" alt="service"
                                            style="width:500px; height:535px;">
                                    </div>
                                    <div class="sigma_service-body">
                                        <h5>High Quality Products for Schools and Corporates</h5>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="slide-item">
                        <div class="row">

                            <div class="col-lg-6">
                                <a href="#" class="sigma_service style-3">
                                    <div class="sigma_service-thumb">
                                        <img src="{{ asset('img/services/4.jpeg') }}" alt="service"
                                            style="width:500px; height:250px;">
                                    </div>
                                    <div class="sigma_service-body">
                                        <h5>Customized Face Mask</h5>
                                    </div>
                                </a>

                                <a href="#" class="sigma_service style-3">
                                    <div class="sigma_service-thumb">
                                        <img src="{{ asset('img/services/5.jpeg') }}" alt="service"
                                            style="width:500px; height:250px;">
                                    </div>
                                    <div class="sigma_service-body">
                                        <h5>Customized Face Mask / Customized Face Shield / Sanitizer Dispenser</h5>
                                    </div>
                                </a>
                            </div>

                            <div class="col-lg-6">
                                <a href="#" class="sigma_service style-3">
                                    <div class="sigma_service-thumb">
                                        <img src="{{ asset('img/services/7.jpg') }}" alt="service"
                                            style="width:500px; height:535px;">
                                    </div>
                                    <div class="sigma_service-body">
                                        <h5>Foot operated Saniter Dispenser</h5>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- Services End -->

        <!-- Services Start -->
        {{-- <div class="section section-padding pt-0">
            <div class="container">

                <div class="section-title text-center">
                    <h4 class="title">Our Facilities</h4>
                    <p>Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit.
                    </p>
                </div>

                <div class="row">

                    <div class="col-xl-3 col-lg-4 col-md-6">
                        <a href="#" class="sigma_service style-2">
                            <div class="sigma_service-thumb">
                                <img src="{{ asset('img/icons/service/3.png') }}" alt="service">
        </div>
        <div class="sigma_service-body">
            <h5>Printed in full color</h5>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod </p>
        </div>
        </a>
        </div>

        <div class="col-xl-3 col-lg-4 col-md-6">
            <a href="#" class="sigma_service style-2">
                <div class="sigma_service-thumb">
                    <img src="{{ asset('img/icons/service/1.png') }}" alt="service">
                </div>
                <div class="sigma_service-body">
                    <h5>Double Sided</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod </p>
                </div>
            </a>
        </div>

        <div class="col-xl-3 col-lg-4 col-md-6">
            <a href="#" class="sigma_service style-2">
                <div class="sigma_service-thumb">
                    <img src="{{ asset('img/icons/service/4.png') }}" alt="service">
                </div>
                <div class="sigma_service-body">
                    <h5>Variety of paper sizes</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod </p>
                </div>
            </a>
        </div>

        <div class="col-xl-3 col-lg-4 col-md-6">
            <a href="#" class="sigma_service style-2">
                <div class="sigma_service-thumb">
                    <img src="{{ asset('img/icons/service/2.png') }}" alt="service">
                </div>
                <div class="sigma_service-body">
                    <h5>Optional Finishing</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod </p>
                </div>
            </a>
        </div>

        </div>

        </div>
        </div> --}}
        <!-- Services End -->

        <!-- Contact form Start -->
        <div class="row section section-padding">

            <div class="col-lg-2 d-none d-lg-block">
                <div class="section">
                    <div class="section-title flip d-none d-lg-block">
                        <h4 class="title">Get in Touch!</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 position-relative">
                <div class="section pb-0">
                    <div class="container-fluid">
                        <div class="section-title d-block d-lg-none">
                            <h4 class="title">Get in Touch!</h4>
                        </div>
                        <form action="{{url('contactus/save')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" name="name" value="" required
                                    style="border: 1px solid lightgrey">
                            </div>
                            <div class="form-group">
                                <label>Email Address <em class="text-danger">*</em> </label>
                                <input type="email" class="form-control" name="email" value="" required
                                    style="border: 1px solid lightgrey">
                            </div>
                            <div class="form-group">
                                <label>Subject <em class="text-danger">*</em> </label>
                                <input type="text" class="form-control" name="subject" value="" required
                                    style="border: 1px solid lightgrey">
                            </div>
                            <div class="form-group">
                                <label>Message <em class="text-danger">*</em> </label>
                                <textarea name="message" cols="45" rows="5" class="form-control" value="" required
                                    style="border: 1px solid lightgrey"></textarea>
                            </div>
                            <button type="submit" class="sigma_btn-custom secondary" name="button">Submit
                                Now</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="offset-lg-1 col-lg-6 position-relative">
                <div class="bg-cover sigma_contact-bg"
                    style="background-image: url({{ asset('img/imgbannar.jpg') }});width: 685px; height: 564px;">
                </div>
            </div>

        </div>
        <!-- Contact form End -->


        <!-- Related Products start -->
        <div class="section section-padding pt-0">
            <div class="container">

                <div class="section-title text-center">
                    <h4 class="title">Our Products</h4>
                    {{-- <p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis
                        quis
                        ac lectus.</p> --}}
                </div>

                <div class="sigma_related-posts">

                    <!-- Products Post Start -->
                    <div class="sigma_related-slider">
                        @foreach ($products as $product)
                        <!-- Product Start -->
                        <div class="sigma_product">
                            <div class="sigma_product-thumb">
                                <a href="{{ url('product/'.$product->id.'/options') }}" style="text-align: center">
                                    {{-- <img src="https://via.placeholder.com/270x260" alt="product"> --}}
                                    @if (!empty($product->product_images))
                                    <img style="max-width:270px;max-height:260px;width:auto;height:200px;margin: 0px auto"
                                        src="{{ $product->product_images }}" alt="product" />
                                    @else
                                    <img style="max-width:270px;max-height:260px;width:auto;height:200px;margin: 0px auto"
                                        src="{{ asset('img/no-img.png') }}"  />
                                    @endif
                                </a>
                            </div>
                            {{-- <div class="sigma_product-body">
                                <h5 class="sigma_product-title"> <a href="product-single.html">T-shirt Design</a> </h5>
                                <div class="sigma_product-price">
                                    <span>29$</span>
                                </div>
                                <a href="product-single.html" class="sigma_btn-custom btn-sm dark btn-pill">Buy Now</a>

                            </div> --}}
                            <div class="sigma_product-body">
                                <h5 class="sigma_product-title"> <a
                                        href="{{ url('product/'.$product->id.'/options') }}">{{ $product->product_name }}</a>
                                </h5>
                                @if (count($product->priceQty) > 0)
                                <div class="sigma_product-price">
                                    <span>&#x20B9;&nbsp;{{$product->priceQty[0]->price}}
                                        ({{ $product->priceQty[0]->quantity }} Quantity)</span>
                                </div>
                                @endif
                                <a href="{{'product/'.$product->id.'/options'}}"
                                    class="sigma_btn-custom btn-sm dark btn-pill">Select Now</a>

                            </div>
                        </div>
                        <!-- Product End -->
                        @endforeach

                        <!-- Product Start -->
                        {{-- <div class="sigma_product">
                            <div class="sigma_product-thumb">
                                <a href="product-single.html"><img src="https://via.placeholder.com/270x260"
                                        alt="product"></a>
                            </div>
                            <div class="sigma_product-body">
                                <h5 class="sigma_product-title"> <a href="product-single.html">Book Cover</a> </h5>
                                <div class="sigma_product-price">
                                    <span>19$</span>
                                </div>
                                <a href="product-single.html" class="sigma_btn-custom btn-sm dark btn-pill">Buy Now</a>

                            </div>
                        </div> --}}
                        <!-- Product End -->

                        <!-- Product Start -->
                        {{-- <div class="sigma_product">
                            <div class="sigma_product-thumb">
                                <a href="product-single.html"><img src="https://via.placeholder.com/270x260"
                                        alt="product"></a>
                            </div>
                            <div class="sigma_product-body">
                                <h5 class="sigma_product-title"> <a href="product-single.html">Van Cover</a> </h5>
                                <div class="sigma_product-price">
                                    <span>19$</span>
                                    <span>29$</span>
                                </div>
                                <a href="product-single.html" class="sigma_btn-custom btn-sm dark btn-pill">Buy Now</a>

                            </div>
                        </div> --}}
                        <!-- Product End -->

                        <!-- Product Start -->
                        {{-- <div class="sigma_product">
                           
                            <div class="sigma_product-thumb">
                                <a href="product-single.html"><img src="https://via.placeholder.com/270x260"
                                        alt="product"></a>
                            </div>
                            <div class="sigma_product-body">
                                <h5 class="sigma_product-title"> <a href="product-single.html">Polaroid Enhancement</a>
                                </h5>
                                <div class="sigma_product-price">
                                    <span>29$</span>
                                </div>
                                <a href="product-single.html" class="sigma_btn-custom btn-sm dark btn-pill">Buy Now</a>

                            </div>
                        </div> --}}
                        <!-- Product End -->

                    </div>
                    <!-- Products End -->

                </div>

            </div>
        </div>
        <!-- Related Products End -->

        <!-- Map Start -->
        <div class="section p-0">

            <div class="sigma_map">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3501.2990990865214!2d77.19152031503862!3d28.65076169000669!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d029dee5ae8b3%3A0x623aa12e65047a7d!2sExcel%20Id%20Card%20Solutions.Com!5e0!3m2!1sen!2sin!4v1604908626858!5m2!1sen!2sin"
                    height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                    tabindex="0"></iframe>
                <div class="sigma_contact-info" style="padding: 0px; min-width: 42%">
                    <iframe width="500" height="600"
                    src="https://www.youtube.com/embed/0-kAPTMVOso?loop=1&autoplay=1&mute=1" frameborder="0"
                    allowfullscreen></iframe>
                </div>
            </div>

        </div>
        <!-- Map End -->

        <!-- Testimonials Start -->
        {{-- <section class="section section-padding">
            <div class="container">

                <div class="sigma_testimonial style-3">
                    <div class="sigma_testimonial-slider">

                        <div class="sigma_testimonial-inner">
                            <img src="https://via.placeholder.com/270x368" alt="testimonial">
                            <div class="sigma_testimonial-body">
                                <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                                    default model text.</p>
                                <cite>Bobby K. Parker</cite>
                            </div>
                        </div>
                        <div class="sigma_testimonial-inner">
                            <img src="https://via.placeholder.com/270x368" alt="testimonial">
                            <div class="sigma_testimonial-body">
                                <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                                    default model text.</p>
                                <cite>Bobby K. Parker</cite>
                            </div>
                        </div>
                        <div class="sigma_testimonial-inner">
                            <img src="https://via.placeholder.com/270x368" alt="testimonial">
                            <div class="sigma_testimonial-body">
                                <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                                    default model text.</p>
                                <cite>Bobby K. Parker</cite>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section> --}}
        <!-- Testimonials End -->


        <!-- Clients Start -->
        {{-- <div class="section pt-0">
            <div class="container">
                <div class="row no-gutters">

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-3 col-6 p-0">
                        <div class="sigma_client">
                            <img src="https://via.placeholder.com/150x49" alt="client">
                        </div>
                    </div>

                </div>
            </div>
        </div> --}}
        <!-- Clients End -->
        {{-- @stop
    @section('footer')
    @parent
    @endsection --}}
        {{-- @section('script')
        <script type="text/javascript">
            $('#reload').click(function () {
                $.ajax({
                    type: 'GET',
                    url: 'reload-captcha',
                    success: function (data) {
                        $(".captcha span").html(data.captcha);
                    }
                });
            });
        </script>
    @endsection --}}
    </x-main-layout>