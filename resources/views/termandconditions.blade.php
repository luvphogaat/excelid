<x-main-layout>

    <x-slot name="logo">
    </x-slot>
    <!-- Subheader Start -->
    <div class="sigma_subheader primary-bg">

        <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture" />

        <!-- Top Left Wave -->
        <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div>

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>

        <div class="container">
            <div class="sigma_subheader-inner">
                <h1>Terms & Conditions</h1>
            </div>
        </div>
    </div>
    <!-- Subheader End -->

    <!--Cart Start -->
    <div class="section">
        <div class="container">
            <p style="font-size:16px;">
                Please read the following terms and conditions very carefully as your use of service is subject to your
                acceptance of and compliance with the following terms and conditions ("Terms").
            </p>
            <h6>Introduction:</h6>
            <p>Excelidcradsolutions.com is an Internet based content and e-commerce portal. Excelidcardsolutions.com
                providing customers with a wide range of online printing options for customized designing by provision
                of designing tools and shipping of the completed order collectively referred to as "the online
                services".
            </p>
            <p>Use of the Website is offered to you conditioned on acceptance without modification of all the terms,
                conditions and notices contained in these Terms, as may be posted on the Website from time to time.
                Excelidcardsolutions.com at its sole discretion reserves the right not to accept a User from registering
                on the Website without assigning any reason thereof</p>
            <h6>User Account, Password, and Security:</h6>
            <p>You will receive a password and account designation upon completing the Website's registration process.
                You are responsible for maintaining the confidentiality of the password and account, and are fully
                responsible for all activities that occur under your password or account.
            </p>
            <h6>Services Offered:</h6>
            <p>Excelidcardsolutions.com provides a number of Internet-based services through the Web Site. One such
                service enables users to purchase original merchandise such as school id cards, multi-color lanyard and
                different type of name badges etc. (collectively, "Products"). Upon placing an order,
                excelidcardsolutions.com shall ship the product to you and be entitled to its payment for the Services.
            </p>
        </div>
    </div>
</x-main-layout>