<x-main-layout>
    <x-slot name="logo">
        {{-- <x-jet-authentication-card-logo /> --}}
    </x-slot>

    @if(session()->has('flash_message'))
    <div class="alert alert-{{ session('flash_message') }}">
        {!! session('flash_message') !!}
    </div>
    @endif
    <div class="sigma_subheader">

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>

        <div class="sigma_map">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3501.2990990865214!2d77.19152031503862!3d28.65076169000669!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d029dee5ae8b3%3A0x623aa12e65047a7d!2sExcel%20Id%20Card%20Solutions.Com!5e0!3m2!1sen!2sin!4v1604908626858!5m2!1sen!2sin"
                height="400" allowfullscreen=""></iframe>
        </div>

    </div>
    <!-- Subheader End -->

    <!-- Icons Start -->
    <div class="section section-padding">
        <div class="container">

            <div class="section-title text-center">
                <h6 class="subtitle">Information</h6>
                <h4 class="title">Get in Touch</h4>
            </div>

            <div class="row">

                <div class="col-lg-4 col-md-6">
                    <div class="sigma_icon-block icon-block-7">
                        <i class="flaticon-call"></i>
                        <div class="sigma_icon-block-content">
                            <h5>Call Us</h5>
                            <p>+91-8383006931</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="sigma_icon-block icon-block-7">
                        <i class="flaticon-message"></i>
                        <div class="sigma_icon-block-content">
                            <h5>Mail Us</h5>
                            <p>ExcelIDCardsSolutions@gmail.com</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="sigma_icon-block icon-block-7">
                        <i class="flaticon-paper-plane"></i>
                        <div class="sigma_icon-block-content">
                            <h5>Find Us</h5>
                            <p>2600/5, 1st Floor, Beadon Pura
                                Karol Bagh, Delhi, India - 110005</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- Icons End -->

    <!-- Contact form Start -->
    <div class="row">

        <div class="col-lg-2 d-none d-lg-block">
            <div class="section">
                <div class="section-title flip d-none d-lg-block">
                    <h4 class="title">Get in Touch!</h4>
                </div>
            </div>
        </div>
        <div class="col-lg-3 position-relative">
            <div class="section pb-0">
                <div class="container-fluid">
                    <div class="section-title d-block d-lg-none">
                        <h4 class="title">Get in Touch!</h4>
                    </div>
                    <form action="{{url('contactus/save')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" class="form-control" name="name" value="" required style="border: 1px solid lightgrey">
                        </div>
                        <div class="form-group">
                            <label>Email Address <em class="text-danger">*</em> </label>
                            <input type="email" class="form-control" name="email" value="" required style="border: 1px solid lightgrey">
                        </div>
                        <div class="form-group">
                            <label>Subject <em class="text-danger">*</em> </label>
                            <input type="text" class="form-control" name="subject" value="" required style="border: 1px solid lightgrey">
                        </div>
                        <div class="form-group">
                            <label>Message <em class="text-danger">*</em> </label>
                            <textarea name="message" cols="45" rows="5" class="form-control"
                                value="" required style="border: 1px solid lightgrey"></textarea>
                        </div>
                        <button type="submit" class="sigma_btn-custom secondary" name="button">Submit
                            Now</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="offset-lg-1 col-lg-6 position-relative">
            <div class="bg-cover sigma_contact-bg" style="background-image: url({{ asset('img/imgbannar.jpg') }});">
            </div>
            <div class="sigma_contact-info">
                <h3 class="text-white">Contact Details</h3>
                <div class="sigma_contact-info-item">
                    <h6>Location</h6>
                    <p>2600/5, 1st Floor, Beadon Pura</p>
                    <p>Karol Bagh, Delhi, India - 110005</p>
                </div>
                <div class="sigma_contact-info-item">
                    <h6>Mobile Number</h6>
                    <p>+91-8383006931</p>
                </div>
                <div class="sigma_contact-info-item">
                    <h6>Email</h6>
                    <p>ExcelIDCardsSolutions@gmail.com</p>
                </div>
                <div class="sigma_contact-info-item">
                    <ul class="sigma_sm">
                        {{-- <li> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
                        <li> <a href="#"> <i class="fab fa-instagram"></i> </a> </li>
                        <li> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
                        <li> <a href="#"> <i class="fab fa-linkedin-in"></i> </a> </li> --}}
                        <li> <a target="_blank" href="https://www.facebook.com/ExcelIdCardSolutions"> <i class="fab fa-facebook-f"></i> </a> </li>
                        <li> <a target="_blank" href="https://www.instagram.com/p/CLgPXD7nyXz/?igshid=spya82794cjk"> <i class="fab fa-instagram"></i> </a> </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <br/><br/>
    <!-- Contact form End -->

    <!-- Clients Start -->
    {{-- <div class="section">
        <div class="container">
            <div class="row no-gutters">

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

            </div>
        </div>
    </div> --}}
    <!-- Clients End -->
      <!--Start of Tawk.to Script-->
        <script defer type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/6034b5659c4f165d47c61bf5/1ev6u9755';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
</x-main-layout>