@component('mail::message')

# Order Details

{{-- Your order is almost confirmed, you are just one step to confirmation. Click on the below button to pay the order, payment link will be valid for 50 minutes. --}}

Your order is saved, you just need to make a payment for the amount mentioned to '' and share the paymentId

@component('mail::panel')
# Order : # {{ $orderNumber }} 
Order Date: {{  $orderDate }}<br>
Order Total: Rs. {{  $totalAmount }} <br>
<br>
# Shipping Address:
{{ $shipDetails['street_address_1'] }} <br>
{{ $shipDetails['street_address_2'] }}<br>
{{ $shipDetails['town_city'] }}<br>
{{ $shipDetails['country'] }}<br>
{{ $shipDetails['phone_number'] }}<br>
@endcomponent
<br>
# Your Order Details
@component('mail::table')
| Product                     | Quantity                      | Price                         |
| --------------------------- | :---------------------------: | -----------------------------:|
@foreach($mailDetails as $key => $mailDetail)
| <div style="font-size:14px;text-align:left"> Product Name: {{ $mailDetail['product_name'] }} </div><div style="font-size:14px;text-align:left"> Category :{{ $mailDetail['category'] }} </div><div style="font-size:14px;text-align:left">Job Size: {{ $mailDetail['job_size'] }}</div>@if (count($mailDetail['addons']) > 0) @foreach ($mailDetail['addons'] as $key => $addon)<div style="font-size:14px;text-align:left">{{ $key }}: {{ $addon }}</div> @endforeach @endif | {{ $mailDetail['quantity'] }} | Rs. {{ $mailDetail['price'] }} |
@endforeach
|  |  | Total Amount | {{ $totalAmount }} |
@endcomponent

@component('mail::button', ['url' => $url])
    View Order
@endcomponent


Thanks & Regards,<br><br>
MANPREET KAUR<br><br>
+919899980987, 9999620987, 8383006931<br><br>
EXCEL ID CARD SOLUTIONS<br>
2600/5, 1ST FLOOR, BEADON PURA<br>
KAROL BAGH<br>
NEW DELHI - 110005
@endcomponent
