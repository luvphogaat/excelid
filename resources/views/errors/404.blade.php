<x-main-layout>
  <!-- Subheader Start -->
  <div class="sigma_subheader primary-bg">

    <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture" />

    <!-- Top Left Wave -->
    <div class="sigma_subheader-shape circles">
      <div class="circle circle-lg circle-1 primary-dark-bg"></div>
      <div class="circle circle-sm circle-2 bg-white"></div>
      <div class="circle circle-md circle-3 secondary-bg"></div>
    </div>

    <!-- Bottom Wave -->
    <div class="sigma_subheader-shape waves">
      <div class="wave"></div>
      <div class="wave"></div>
    </div>

    <div class="container">
      <div class="sigma_subheader-inner">
        <h1>Error</h1>
      </div>
    </div>
  </div>
  <!-- Subheader End -->

  <!-- Error section Start -->
  <section class="section error bg-bottom bg-norepeat">
    <div class="container">
      <div class="row align-items-center">
        <div class="offset-lg-3 col-lg-6">
          <div class="error-texts text-center">
            <h1 class="custom-primary">404</h1>
            <h2>Ooops! That page doesn't exist!</h3>
            <a href="{{ route('index') }}" class="sigma_btn-custom secondary">Back to Home</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Error section End -->
</x-main-layout>