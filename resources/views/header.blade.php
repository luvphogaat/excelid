<aside class="sigma_aside sigma_aside-left">

    <a class="navbar-brand" href="index"> <img src="{{ asset('img/eids_logo.png') }}" alt="logo"> </a>

    <!-- Menu -->
    <ul>
        @foreach ($menuItems as $item)
        <li class="menu-item menu-item-has-children">
            <a href="{{ url('products/'.$item->link) }}">{{ $item->title }}</a>
            @if (count($item->submenus) > 0)
            <ul class="sub-menu">
                @foreach($item->submenus as $submenu)
                <li class="menu-item">
                <a href="{{ url('products/'.$item->link.'/'.$submenu->link)}}">{{ $submenu->title }}</a>
                </li>
                @endforeach
            </ul>
            @endif
        </li>
    @endforeach
    </ul>

</aside>
<div class="sigma_aside-overlay aside-trigger-left"></div>
<!-- Header Start -->
<header
    class="sigma_header header-1  custom-header-absolute" style="z-index:999;">
    <!-- Middle Header Start -->
    <div class="sigma_header-middle">
        <div class="container-fluid">
            <nav class="navbar">
                <!-- Logo Start -->
                <div class="sigma_logo-wrapper">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('img/eids_logo.png') }}" alt="logo" style="margin: 0px auto">
                    </a>
                </div>
                <form style="min-width:30%" action="{{ route('search.post') }}" method="post">
                    @csrf
                    <div class="row no-gutters align-items-center" style="border: 1px solid grey; border-radius: 20px">
                        <div class="col-auto" style="padding:10px">
                            <i class="fas fa-search h4 text-body"></i>
                        </div>
                        <!--end of col-->
                        <div class="col">
                            <input name="searchQuery" class="form-control form-control-lg form-control-borderless" type="search" placeholder="Search" style="border: none; width: 90%">
                        </div>
                        <!--end of col-->
                    </div>  
                </form>
                <!-- Logo End -->
                {{-- @if((Request::is('login') || (!Request::is('reset-password') || (!Request::is('register')) --}}
            <!-- Menu -->
                {{-- <ul class="navbar-nav">
                    @foreach ($menuItems as $item)
                    <li class="menu-item menu-item-has-children">
                        <a href="{{ url($item->link) }}">{{ $item->title }}</a>
                        @if (count($item->submenus) > 0)
                        <ul class="sub-menu">
                            @foreach($item->submenus as $submenu)
                            <li class="menu-item">
                            <a href="{{ url('product/'.$submenu->link)}}">{{ $submenu->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                @endforeach
                </ul> --}}

                <!-- Controls -->
                <div style="display:inline-flex; flex-wrap:wrap; gap:12px;">
                    <a href="{{ route('contact-us') }}"
                        class="sigma_btn-custom btn-sm btn-pill d-none d-lg-block ">
                        <i class="fas fa-user-headset" style="font-size:20px;margin-right:5px"></i><span>Support</span>
                    </a>
                    <a href="{{ route('cart') }}"
                        class="sigma_btn-custom btn-sm btn-pill d-none d-lg-block ">
                        <i class="fas fa-shopping-cart" style="font-size:20px;margin-right:5px"></i><span>Cart</span>
                        @if($cartCount > 0)
                            <span class="cart-badge-count">{{$cartCount}}</span>
                        @endif
                    </a>
                    <a href="{{ route('login') }}"
                        class="sigma_btn-custom btn-sm btn-pill d-none d-lg-block">
                        <i class="fas fa-user"  style="font-size:20px;margin-right:5px"></i><span>Account</span>
                    </a>
                </div>

                <!-- Mobile Toggler -->
                <div class="aside-toggler style-2 aside-trigger-left">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                {{-- @endif --}}
            </nav>
            <nav class="navbar">
                <!-- Logo Start -->
                {{-- <div class="sigma_logo-wrapper">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('img/eids_logo.png') }}" alt="logo">
                    </a>
                </div> --}}
                <!-- Logo End -->
                {{-- @if((Request::is('login') || (!Request::is('reset-password') || (!Request::is('register')) --}}
            <!-- Menu -->
                <ul class="navbar-nav" style="margin:0px auto">
                    @foreach ($menuItems as $item)
                    <li class="menu-item menu-item-has-children">
                        @if($item->link != 'products/more')
                            <a href="{{ url('products/'.$item->link) }}">{{ $item->title }}</a>
                        @else
                        <a href="#">{{ $item->title }}</a>
                        @endif
                        @if (count($item->submenus) > 0)
                        <ul class="sub-menu">
                            @foreach($item->submenus as $submenu)
                            <li class="menu-item">
                                <a href="{{ url('products/'.$item->link.'/'.$submenu->link)}}">{{ $submenu->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                @endforeach
                </ul>

                <!-- Controls -->
                {{-- <div style="display:inline-flex; flex-wrap:wrap; gap:12px;">
                    <a href="{{ route('cart') }}"
                        class="sigma_btn-custom btn-sm btn-pill d-none d-lg-block ">
                        <i class="fas fa-shopping-cart" style="font-size:20px;margin-right:5px"></i><span>Cart</span>
                        @if($cartCount > 0)
                            <span class="cart-badge-count">{{$cartCount}}</span>
                        @endif
                    </a>
                    <a href="{{ route('login') }}"
                        class="sigma_btn-custom btn-sm btn-pill d-none d-lg-block">
                        <i class="fas fa-user"  style="font-size:20px;margin-right:5px"></i><span>Account</span>
                    </a>
                </div> --}}

                <!-- Mobile Toggler -->
                {{-- <div class="aside-toggler style-2 aside-trigger-left">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div> --}}
                {{-- @endif --}}
            </nav>
        </div>
    </div>
    <!-- Middle Header End -->

</header>
<!-- Header End -->
