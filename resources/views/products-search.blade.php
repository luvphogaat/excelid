<x-main-layout>
    <x-slot name="logo">
        <x-jet-authentication-card-logo />
    </x-slot>
    <!-- Subheader Start -->
    <div class="sigma_subheader dark-overlay primary-overlay bg-cover bg-norepeat"
        style="background-image: url('{{ asset('img/banner/SLIDE '.rand(1,6).'.jpg') }}')">

        <!-- <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture"> -->

        prod
        <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div>

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>

        <div class="container">
            <div class="sigma_subheader-inner">
                <h1>Search Result</h1>
                {{-- <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $users->category_title }}</li>
                    </ol>
                </nav> --}}
            </div>
        </div>
    </div>
    <!-- Subheader End -->

    <!-- Products Start -->
    <div class="section">
        <div class="container">

            <!-- Product Count & Orderby Start -->
            <div class="sigma_shop-global">
            <p>Showing <b>{{ $productCount }}</b> of <b>{{ $productCount }}</b> products </p>
                <form method="post">
                    <select class="form-control" name="orderby">
                        <option value="default">Default sorting</option>
                        <option value="latest">Latest release</option>
                        <option value="price-down">Price: High - Low</option>
                        <option value="price-up">Price: Low - High</option>
                        <option value="popularity">Popularity Sorting</option>
                    </select>
                </form>
            </div>
            <!-- Product Count & Orderby End -->

            <div class="row masonry">
                <!-- Upload Design Starts -->
                {{-- <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 masonry-item">
                  <div class="sigma_product">
                      <div class="sigma_product-thumb" style="padding:25px;background:rgba(72, 172, 110, .8); height:250px">
                      <a href="product-single.html"><img src="{{ asset('img/icons/uploadIcon.png') }}"
                                  alt="product"></a>
                      </div>
                      <div class="sigma_product-body">
                          <h5 class="sigma_product-title"> <a
                                  href="product-single.html">Upload Design</a> </h5>
                          <div class="sigma_product-price">
                              <span>&nbsp;</span>
                          </div>
                          <a href="product-single.html" class="sigma_btn-custom btn-sm dark btn-pill">Select Now</a>

                      </div>
                  </div>
              </div> --}}
                <!-- Upload Design Ends -->
                <!-- Products STARTS -->
                @foreach ($products as $product)
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 masonry-item">
                    <div class="sigma_product">
                        <div class="sigma_product-thumb">
                        <a href="{{ route('product.single', $product->slug) }}">
                            @if (!empty($product->product_images))
                                <img style="max-width:270px;max-height:260px;width:270px;height:260px;" src="{{ $product->product_images }}" alt="product">
                             @else
                                <img style="max-width:270px;max-height:260px;width:270px;height:260px;" src="{{ asset('img/no-img.png') }}" >
                            @endif
                            
                        </a>
                        </div>
                        <div class="sigma_product-body">
                            <h5 class="sigma_product-title"> <a
                                    href="{{ route('product.single', $product->slug) }}">{{ $product->product_name }}</a> </h5>
                            <div class="sigma_product-price">
                                <span>&#x20B9;&nbsp;{{$product->price}} ({{ $product->quantity }} Quantity)</span>
                            </div>
                        <a href="{{ route('product.single', $product->slug) }}" class="sigma_btn-custom btn-sm dark btn-pill">Select Now</a>

                        </div>
                    </div>
                </div>
                @endforeach
                <!-- Products ENDS -->

            </div>

            <!-- Pagination Start -->
            {{-- <ul class="pagination">
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item ">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
            </ul> --}}
            <!-- Pagination End -->

        </div>
    </div>
    <!-- Products End -->

    <!-- Clients Start -->
    {{-- <div class="section pt-0">
        <div class="container">
            <div class="row no-gutters">

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

                <div class="col-lg-2 col-md-3 col-6 p-0">
                    <div class="sigma_client">
                        <img src="https://via.placeholder.com/150x49" alt="client">
                    </div>
                </div>

            </div>
        </div>
    </div> --}}
    <!-- Clients End -->
</x-main-layout>