    <!-- Subheader Start -->
    <div class="sigma_subheader dark-overlay primary-overlay bg-cover bg-norepeat"
        style="background-image: url('{{ asset('img/banner/SLIDE '.rand(1,6).'.jpg') }}')">

        <!-- <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture"> -->

        <!-- Top Left Wave -->
        <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div>

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>

        <div class="container">
            <div class="sigma_subheader-inner">
                <h1>Product Name</h1>
            </div>
        </div>
    </div>
    <!-- Subheader End -->

    <!-- Products Start -->
    <div class="section">
        <div class="container-fluid body1" id="viewbeg" style="//border:double">


            <div class="container-fluid stepping" style="margin-top:30px;padding:15px;width:90%">


                <ul style="padding:0px;list-style-type:none;">
                    <li class="arrow-whole col-md-4 " id="step-1" style="border:double;padding:0px">
                        <div style="width:90%;//border:double;float:left;height:60px;padding:10px 0px 0px 10px;font-weight:bolder"
                            class="text-center current broadstrip">
                            <h4>STEP 1</h4>
                        </div>
                        <div class="arrow-right current1 arr-right" style="max-width:10%"></div>
                    </li>
                    <li class="arrow-whole col-md-4 col-lg-4 col-sm-3 col-xs-3" id="step-2"
                        style="border:double;padding:0px">
                        <div class="arrow-left arr-left"></div>
                        <div style="width:80%;//border:double;float:left;height:60px;background-color:#530053;padding:10px 0px 0px 10px;font-weight:bolder"
                            class="text-center broadstrip1">
                            <h4>STEP 2</h4>
                        </div>
                        <div class="arrow-right arr-right"></div>
                    </li>

                    <li class="arrow-whole col-md-4 col-lg-4 col-sm-3 col-xs-3" id="step-4"
                        style="border:double;padding:0px">
                        <div class="arrow-left arr-left"></div>
                        <div style="width:90%;//border:double;float:left;height:60px;background-color:#530053;padding:10px 0px 0px 10px;font-weight:bolder"
                            class="text-center broadstrip">
                            <h4>STEP 3</h4>
                        </div>

                    </li>

                </ul>
            </div>
            <br><br>

            <form role="form" method="post" action="get_order.php" enctype="multipart/form-data">
                <div class="container-fluid s1" id="s1"
                    style="//border:double;padding:0px;margin:0px auto;overflow:hidden;width:90%">

                    <div class="jumbotron" style="padding-top:10px;">
                        <p class="pull-right">Cost : Rs.<span id="cost"><?php echo $viewStep ?></span></p>
                        <h3 class="text-center">Lanyard Printing</h3>
                        <br>
                        <div class="row">

                            <div class="col-md-5">
                                <select class="form-control" id="lanstyle" onchange="get(this.value)" name="lanstyle">
                                    <option value="">Select your Lanyard Style</option>
                                </select>
                                <div id="styleerror"></div>
                            </div>
                            <div class="col-md-5 col-md-offset-1">
                                <input type="text" class="form-control" value="100" id="quantity"
                                    onchange="priceget1(this.value)" name="quantity">
                                <div id="quantityerror"></div>
                            </div>
                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-5">
                                <select class="form-control" id="lanwidth" onchange="priceget(this.value)" name="width">
                                    <option value="">Select your lanyard width</option>
                                </select>
                                <div id="widtherror"></div>
                            </div>

                            <div class="col-md-5 col-md-offset-1">
                                <input type="text" class="form-control" placeholder="Enter Text " id="frontstrip"
                                    oninput="entertext()" name="fronttext">
                                <div id="texterror"></div>
                            </div>

                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-5">
                                <span id="lancolor">
                                    <select class="form-control" onchange="displaycolor()" name="lancolor">
                                        <option>Select color for your lanyard</option>
                                    </select>
                                </span>
                            </div>
                            {{-- <script>
                                function displaycolor() {
                                    var style = document.getElementById('lanstyle').value;
                                    var colorname = document.getElementById('colorname').value;
                                    //alert(colorname);
                                    document.getElementById('showoutput').style.backgroundColor = colorname;
                                    if (style == 'Digital') {
                                        document.getElementById('showoutput1').style.backgroundColor = colorname;
                                    }
                                }
                            </script> --}}
                            <div class="col-md-5 col-md-offset-1">
                                <input type="text" class="form-control" placeholder="Enter Back Strip Text "
                                    id="backstrip" style="display:none" oninput="entertext1()" name="backtext">
                            </div>

                        </div>
                        <script>
                            function entertext() {
                                var frontstrip = document.getElementById('frontstrip').value;
                                var main = frontstrip;
                                document.getElementById('showoutput').innerHTML = main;
                            }

                            function entertext1() {
                                //var colorname = document.getElementById('colorname').value;
                                var backstrip = document.getElementById('backstrip').value
                                document.getElementById('showoutput1').innerHTML = backstrip;

                            }
                        </script>
                        <br>
                        <div class="row" id="uploaddis" style="display:none">

                            <div class="col-md-5">
                                <input type="file" class="form-control" name="uploadpattern" />
                            </div>
                        </div>
                        <div class="container"
                            style="//border:double;min-height:30px;width:30%;margin-bottom:5px;margin-left:0px;margin-top:10px;padding:5px;padding-left:10px;color:white;"
                            id="showoutput">
                        </div>

                        <div class="container"
                            style="//border:double;min-height:30px;width:30%;margin-bottom:5px;margin-left:0px;margin-top:10px;padding:5px;padding-left:10px;display:none;color:white"
                            id="showoutput1">

                        </div>
                        <a class="step2 btn btn-success pull-right" onclick="step2()">Next</a>
                    </div>

                </div>
                <div class="container-fluid s2" id="s2"
                    style="//border:double;padding:0px;//position:absolute;display:none;overflow:hidden;width:90%">

                    <div class="jumbotron">
                        <p class="pull-right">Cost : <span id="cost1">Rs. 0</span></p>
                        <input type="hidden" name="cost2" value="0" id="cost2" />

                        <h3 class="text-center">LANYARD FITTING</h3>
                        <div class="row" style="//border:double">
                            <div class="col-md-6" style="//border:double">
                                <label>Select your attachment style</label>
                                <select class="form-control" style="max-width:400px;" id="getattachmentoption"
                                    onchange="showattachment()" name="attachment_type">
                                    <option selected value="fitting">Fitting</option>
                                    <option value="holder">Holder</option>
                                </select>
                                <br>
                                <div class="container-fluid" style="" id="attachmentid1">
                                    <label>Select Fitting Style</label>
                                    <select name="attachment_name" id="attachmentid" class="form-control"
                                        style="max-width:300px;" onchange="getfitting(this.value)">
                                    </select>
                                </div>

                                <div class="container-fluid" style="display:none" id="attachmentid2">
                                    <label>Select Holder Style</label>
                                    <select name="attachment_name" class="form-control" style="max-width:300px"
                                        onchange="getfitting(this.value)" id="attachment-2">
                                        <option selected>Single Side Horizontal Holder</option>
                                        <option>Single Side Vertical Holder</option>
                                        <option>Double Side Horizontal Holder</option>
                                        <option>Double Side Vertical Holder</option>
                                    </select>
                                </div>

                                <br>
                            </div>
                            <div class="col-md-4 col-md-offset-1 " id="image_attachment" style="//border:double">
                                <img src="resources/img/attachments/With_Etching_Hook.jpg" width="100" height="100">
                            </div>
                        </div>
                        <script>
                            function showattachment() {
                                var name = document.getElementById('getattachmentoption').value;
                                //alert(name);
                                if (name == 'fitting') {
                                    document.getElementById('attachmentid1').style.display = '';
                                    document.getElementById('attachmentid2').style.display = 'none';
                                    document.getElementById('cost1').innerHTML = parseInt(document.getElementById(
                                            'cost')
                                        .innerHTML) + 1.50;

                                    document.getElementById('cost2').value = parseInt(document.getElementById('cost')
                                        .innerHTML) + 1.50;

                                    document.getElementById('image_attachment').innerHTML =
                                        '<img src = "resources/img/attachments/With_Etching_Hook.jpg" />';
                                } else {
                                    document.getElementById('attachmentid1').style.display = 'none';
                                    document.getElementById('attachmentid2').style.display = '';
                                    document.getElementById('cost1').innerHTML = parseInt(document.getElementById(
                                            'cost')
                                        .innerHTML) + 3.50;

                                    document.getElementById('cost2').value = parseInt(document.getElementById('cost')
                                        .innerHTML) + 3.50;

                                    document.getElementById('image_attachment').innerHTML =
                                        '<img src = "resources/img/attachments/Single_Side_Horizontal_Holder.jpg" />';
                                }
                            }
                        </script>


                        <a class="step1 btn btn-success" onclick="step1()">Previous</a>
                        <a class="step3 btn btn-success pull-right" onclick="step3()">Next</a>
                    </div>

                    <script>
                        function getvalue() {
                            var value1 = document.getElementById('colorpicker').value;
                            document.getElementById('getvaluecolor').value = value1;
                            //alert(value1);
                        }
                    </script>
                </div>
                <div class="container-fluid s4" id="s4"
                    style="//border:double;padding:0px;//position:absolute;display:none;overflow:hidden;width:90%">

                    <div class="jumbotron" style="">
                        <h2 class="text-center">Summary</h2>
                        <div class="container" style="//border:double;max-width:500px;padding:10px">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>Lanyard Style</td>
                                            <td id="showstyle"></td>
                                        </tr>
                                        <tr>
                                            <td>Lanyard Width</td>
                                            <td id="showwidth"></td>
                                        </tr>
                                        <tr>
                                            <td>Lanyard Text</td>
                                            <td id="showtext"></td>
                                        </tr>
                                        <tr>
                                            <td>Lanyard Quantity</td>
                                            <td id="showquantity"></td>
                                        </tr>
                                        <tr>
                                            <td>Lanyard Attachment Style</td>
                                            <td id="showattachment"></td>
                                        </tr>
                                        <tr>
                                            <td>Total Price</td>
                                            <td id="showprice"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <a class="step3 btn btn-success" onclick="step3_a()">Previous</a>
                        <button class="submit btn btn-success pull-right" type="submit">Place order</button>
                    </div>

                </div>
            </form>

        </div>
    </div>
    <!-- Products End -->
    <script type="text/javascript">
        /////step button with next
        step1 = () => {
            alert('Hello Step 1');
        }
        step2 = () => {
            alert('Hello Step 2');
        };
        step3 = () => {
            alert('Hello Step 3');
        }
    </script>