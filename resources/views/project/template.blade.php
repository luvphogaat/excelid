@section('baseHref')
@endsection
@section('style')
<link rel="stylesheet" href="{{ asset('css/appVue.css') }}" />
<link rel="stylesheet" href="{{ asset('css/loaderVue.css') }}" />
@endsection
@section('script')
<script src="{{ asset('js/appVue.js') }}"></script>
@endsection
<x-main-layout>
    <div id="app">
        <!-- {{-- <div class="sigma_subheader dark-overlay primary-overlay bg-cover bg-norepeat"
            style="background-image: url('{{ asset('img/banner/SLIDE '.rand(1,6).'.jpg') }}')">

            <div class="sigma_subheader-shape circles">
                <div class="circle circle-lg circle-1 primary-dark-bg"></div>
                <div class="circle circle-sm circle-2 bg-white"></div>
                <div class="circle circle-md circle-3 secondary-bg"></div>
            </div>

            <div class="sigma_subheader-shape waves">
                <div class="wave"></div>
                <div class="wave"></div>
            </div>

            <div class="container">
                <div class="sigma_subheader-inner">
                    <h1>Product Name</h1>
                </div>
            </div>
        </div> --}} -->
        <div class="section" style="padding:50px 0">
            <div class="container-fluid body1" id="designapplication">
                <app-component 
                    :customization-data="{{json_encode($customizationData)}}" 
                    v-bind:product-data="{{ json_encode($productDetail) }}" 
                    v-bind:product-job-size="{{ json_encode($productJobSize) }}" 
                    :session-id="{{ json_encode(Session::getId()) }}" 
                    :user-id="{{ json_encode(Auth::user()) }}"
                >
                </app-component>
            </div>
        </div>
    </div>
</x-main-layout>
