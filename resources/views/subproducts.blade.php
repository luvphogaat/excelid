<x-main-layout>
    <x-slot name="logo">
        <x-jet-authentication-card-logo />
    </x-slot>
    <!-- Subheader Start -->
    <div class="sigma_subheader dark-overlay primary-overlay bg-cover bg-norepeat"
        style="background-image: url('{{ asset('img/banner/SLIDE '.rand(1,6).'.jpg') }}')">

        <!-- <img src="{{  asset('img/textures/3.png') }}" class="texture-3" alt="texture"> -->

        <!-- Top Left Wave -->
        <div class="sigma_subheader-shape circles">
            <div class="circle circle-lg circle-1 primary-dark-bg"></div>
            <div class="circle circle-sm circle-2 bg-white"></div>
            <div class="circle circle-md circle-3 secondary-bg"></div>
        </div>

        <!-- Bottom Wave -->
        <div class="sigma_subheader-shape waves">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>

        <div class="container">
            <div class="sigma_subheader-inner">
                <h1>{{ $product->product_name }}</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item"><a href="#">{{ $category->category_title }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $product->product_name }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- Subheader End -->

    <!-- Products Start -->
    <div class="section">
        <div class="container">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 masonry-item">
                        <img src="{{ $product->product_images }}" style="width:300px; height: auto" />
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 masonry-item">
                        <h3 class="text-center">GET STARTED</h3>
                        <div class="row masonry">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 masonry-item">
                            <a href="{{  route('designs.custom', [request()->route('parentslug'), $product->slug, $uid]) }}" class="btn_custom">
                                <div class="btn_custom_title-text upload_design ">
                                    <!-- <img src="https://www.printstop.co.in/themes/printstop/images/browse.png" /> -->
                                    <i class="fa fa-upload image-icon pull-left text-white float-left" aria-hidden="true"></i>
                                    <h4 class="text-white">Upload your own Design</h4>
                                    <i class="fa fa-angle-right pull-right text-white float-right"></i>
                                </div>
                            </a>
                        </div>
                            @if ($designCount > 0)
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 masonry-item" style="margin-top: 20px;">
                                <a href="{{  route('designs.list', $product->id) }}" class="btn_custom">
                                    <div class="btn_custom_title-text browse_design">
                                        <!-- <img src="https://www.printstop.co.in/themes/printstop/images/browse.png" /> -->
                                        <i class="fa fa-search image-icon pull-left text-white float-left" aria-hidden="true"></i>
                                        <h4 class="text-white">Browse our Designs</h4>
                                        <i class="fa fa-angle-right pull-right text-white float-right"></i>
                                    </div>
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Products End -->

    <!-- Clients Start -->
    
    <!-- Clients End -->
</x-main-layout>