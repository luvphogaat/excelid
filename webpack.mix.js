const mix = require('laravel-mix');
require('dotenv').config();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/appVue.js', 'public/js').vue()
    .sass('resources/sass/appVue.scss', 'public/css')
    .sass('resources/sass/loaderVue.scss', 'public/css')
    .webpackConfig(require('./webpack.config'));
